import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: [
    '../../../assets/custom/css/style.css',
    './input.component.css']
})
export class InputComponent implements OnInit {

  searchText
  statusDefined
  constructor() {
    this.statusDefined = [{
      label: 'Hoạt động', value: 'true'
    }, {
      label: 'Ngưng hoạt động', value: 'false'
    }]
   }

  ngOnInit() {
  }

}
