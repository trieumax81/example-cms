import { BaseEntity } from './base-entity';

export class ProductMaterials extends BaseEntity {
  ProductId: number
  MaterialId: number
  Amount: number
  UnitName: string
} 