import { BaseEntity } from './base-entity';

export class ColorPrint extends BaseEntity {
    Code: string
    Name: string
}