export class BaseEntity {
  Id: number
  Status : string
  CreatedBy: string
  CreatedDate: Date
  UpdatedBy: string
  UpdatedDate: Date
  Note: string
}