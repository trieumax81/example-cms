import { BaseEntity } from './base-entity';

export class Partner extends BaseEntity {
    Code
    Name
    TaxCode
    Website
    Address
}