import { BaseEntity } from './base-entity';

export class VendorContact extends BaseEntity {
    Name
    DepartmentId
    PositionId
    Phone
    Skype
    Email
    Content
    VendorId
}