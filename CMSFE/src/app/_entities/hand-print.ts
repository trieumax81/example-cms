import { BaseEntity } from './base-entity';

export class HandPrint extends BaseEntity {
    Code: string
    Name: string
}