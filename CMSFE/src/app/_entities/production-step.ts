import { BaseEntity } from './base-entity';

export class ProductionStep extends BaseEntity {
  NumberOrder: number
  Code: string
  Name: string
}