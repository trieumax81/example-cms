import { BaseEntity } from './base-entity';

export class MaterialPropertyValues extends BaseEntity {
    Value : number
    MaterialId
    MaterialPropertyId
}