import { BaseEntity } from './base-entity';

export class CustomerContact extends BaseEntity {
    Name
    DepartmentId
    PositionId
    Phone
    Skype
    Email
    Content
    CustomerId
}