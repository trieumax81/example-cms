import { BaseEntity } from './base-entity';

export class VendorContract extends BaseEntity {
    RoleId;
    VendorId;
    DateStart 
    DateEnd
    DeadlinePayment
}