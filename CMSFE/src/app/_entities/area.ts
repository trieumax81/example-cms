import { BaseEntity } from './base-entity'

export class Area extends BaseEntity {
  Code: string
  Name: string
  Address: string
  ManagerId: number
}