import { BaseEntity } from './base-entity';
import { ProductionStep } from './production-step';
import { Product } from './product';

export class ProductProduction extends BaseEntity {
  ProductId: number
  NumberOrder: number
  ProductionStepId: number

  constructor() {
    super()
  }
}