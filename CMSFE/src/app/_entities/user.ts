import { BaseEntity } from './base-entity';

export class User extends BaseEntity {
  UserName: string
  Password: string
  FullName: string       
  Gender: string       
  Email: string
  PhoneNumber: string
  Description: string 
  ManagerId: string
  Avatar: string
  Setting: string
  Address: string
  Status: string
  CreatedBy: string
  CreatedDate: Date
  UpdatedBy: string
  UpdatedDate: Date
  RoleIds: string[]
  
}