import { MachiningStep } from './machining-step';
import { BaseEntity } from './base-entity';
import { Product } from './product';

export class ProductMachining extends BaseEntity {
  ProductId: number
  MachiningStepId: number
  NumberOrder: number

  constructor() {
    super()
  }
}