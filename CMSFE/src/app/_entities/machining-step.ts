import { BaseEntity } from './base-entity';

export class MachiningStep extends BaseEntity {
  NumberOrder: number
  Code: string
  Name: string
}