import { BaseEntity } from './base-entity';

export class Customer extends BaseEntity {
    Code
    Name
    TaxCode
    Website
    Address
}