import { BaseEntity } from './base-entity';
import { ProductFinished } from './product-finished';
import { ProductPrint } from './product-print';
import { ProductProduction } from './product-production';
import { ProductMachining } from './product-machining';
import { ProductMaterials } from './product-material';

export class InfoProduct {
  ProductFinished: ProductFinished
  ProductPrint: ProductPrint
  ProductProductions: ProductProduction[]
  ProductMachinings: ProductMachining[]
  ProductMaterials: ProductMaterials[]
  
  constructor() {
    this.ProductFinished = new ProductFinished()
    this.ProductPrint = new ProductPrint()
    this.ProductProductions = []
    this.ProductMachinings = []
    this.ProductMaterials = []
  }
}
export class Product extends BaseEntity {
  Code: string
  Name: string
  Version: string
  UnitId: number
  AreaId: number
  MinimumInventory: number
  TypeProduct: string
  MOQ: number
  SalePrice: number
  StatusProduct: string
  CustomerId: number
  NumberProduct: number
  PlanProductionTime
  Status: string
  IsSample: boolean
  CustomerCode: string
  Infos: InfoProduct[]

  constructor() {
    super()
    this.Infos = []
  }
}