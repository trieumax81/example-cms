import { BaseEntity } from './base-entity';
import { Product } from './product';

export class ProductFinished extends BaseEntity {
  UrlImage: string
  Value: string
  PaperType: string
  ColorId: number
  PageNum: number
  Orther: string
  ProductId: number
  constructor() {
    super()
  }
}