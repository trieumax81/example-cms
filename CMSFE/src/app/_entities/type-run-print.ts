import { BaseEntity } from './base-entity';

export class TypeRunPrint extends BaseEntity {
    Code: string
    Name: string
}