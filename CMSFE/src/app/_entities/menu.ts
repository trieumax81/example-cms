import { BaseEntity } from './base-entity';

export class Menu extends BaseEntity {
  Code: string
  Name: string
  ParentId: number
  Level: number
  Icon: string
  NumberOrder: number
  Color: string
  BackgroundColor: string
  Link: string
  IsVisible: boolean
}