import { BaseEntity } from './base-entity';
import { Role } from './role';
import { Menu } from './menu';

export class RolePermission extends BaseEntity {
  RoleId
  MenuId: number
  CanRead: boolean
  CanCreate: boolean
  CanUpdate: boolean
  CanDelete: boolean
  CanImport: boolean
  CanExport: boolean
  Role: Role
  Menu: Menu

  constructor() {
    super() 
    this.Role = new Role()
    this.Menu = new Menu()
  }
}