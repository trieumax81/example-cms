import { BaseEntity } from './base-entity';

export class Screen extends BaseEntity {
  ScreenName: string
  ComponentName: string
}