import { BaseEntity } from './base-entity'

export class Employee extends BaseEntity {
  Email: string
  Birthday: Date
  Address: string
  DepartmentId: number
  PositionId: number
  Phone: string
  BirthPlace: string
  Code: string
  Gender: string
  IdCardDate: Date
  IdCardNumber: string
  IdCardPlace: string
  IsDelete: Boolean
  JobId: number
  Name: string
  National: string
  AreaId:number

}