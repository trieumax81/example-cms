import { BaseEntity } from './base-entity';
import { RolePermission , UserRole } from '../_entities';

export class Role extends BaseEntity {
  Name: string
  Description: string
  RolePermissions: RolePermission[]
  RoleUsers: UserRole[]

  constructor() {
    super()
    this.RolePermissions = []
  }
}