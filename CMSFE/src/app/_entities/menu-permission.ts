export class MenuPermission {
  Id: number
  Link: string
  Name: string
  Icon: string
  CanRead: boolean
  CanCreate: boolean
  CanUpdate: boolean
  CanDelete: boolean
  CanImport: boolean
  CanExport: boolean
  Childs: MenuPermission[]
}