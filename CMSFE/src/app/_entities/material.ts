import { BaseEntity } from "./base-entity";
import { MaterialPropertyValues } from "./material-properties-value";
export class Material extends BaseEntity {
  GroupId;
  TypeId;
  Code;
  Name;
  UnitId;
  AreaId;
  MinimumInventory;
  VendorId;
  PurchasePrice;
  Note;
  MaterialPropertyValues: MaterialPropertyValues[];

  constructor() {
    super()
    this.MaterialPropertyValues = []
  }
}
