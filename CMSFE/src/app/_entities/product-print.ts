import { BaseEntity } from './base-entity';
import { Product } from './product';

export class ProductPrint extends BaseEntity {
  HandNum: number
  HandId: number
  SizePaper: string
  PrintNum: number
  TypeRunId: number
  PaperType: string
  ColorId: number
  PaperNum: number
  ProductId: number

  constructor() {
    super()
  }
}