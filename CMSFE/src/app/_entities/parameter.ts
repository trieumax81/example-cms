import { BaseEntity } from './base-entity';

export class Parameter extends BaseEntity {
  ParameterCode: string
  ParameterType: string
  Value: string
  Note: string
  Status: string
}