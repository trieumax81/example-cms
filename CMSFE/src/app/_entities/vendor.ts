import { BaseEntity } from './base-entity';

export class Vendor extends BaseEntity {
    Code
    Name
    TaxCode
    Website
    Address
}