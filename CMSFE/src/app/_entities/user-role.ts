import { BaseEntity } from './base-entity';

export class UserRole extends BaseEntity {
  RoleId: string
  UserId: string
}