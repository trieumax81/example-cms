import { Injectable } from '@angular/core'
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http'
import { Observable, throwError } from 'rxjs'
import { catchError } from 'rxjs/operators'
import { AuthService } from '../_services'
import { Router, ActivatedRoute, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService, private router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req)
      .pipe(catchError(err => {
        if (err.status === 401 || err.status === 403) {
          // auto logout if 401 response returned from api
          this.authService.Logout();
          this.router.navigate(['admin/login'])
          // location.reload(true);
        }
        const error = err.error || err.statusText
        console.log(error)
        return throwError(error)
      }))
  }
}
