import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DashboardComponent } from "./components/admin/dashboard/dashboard.component";
import { AuthenticationComponent } from "./components/admin/authentication/authentication.component";
import { ParameterListComponent } from "./components/admin/parameter/parameter-list/parameter-list.component";
import { ButtonComponent } from "./templates/button/button.component";
import { InputComponent } from "./templates/input/input.component";
import { TabsComponent } from "./templates/tabs/tabs.component";
import { UserListComponent } from "./components/admin/users/user-list/user-list.component";
import { MenuListComponent } from "./components/admin/systems/menu-list/menu-list.component";
import { HomeComponent } from './components/user/home/home.component';
import { RoleListComponent } from "./components/admin/roles/role-list/role-list.component";
import { FileManagerComponent } from "./components/admin/file-manager/file-manager.component";
import { UploadPdfComponent } from './components/admin/upload-pdf/upload-pdf.component';
import { PdfListComponent} from './components/admin/pdf_file';


const routes: Routes = [
  { path: "", component: HomeComponent },
  { path: "admin", component: DashboardComponent },
  { path: "admin/login", component: AuthenticationComponent },
  { path: "admin/menu", component: MenuListComponent },
  { path: "admin/du-lieu-dung-chung", component: ParameterListComponent },
  { path: "admin/quan-ly-nguoi-dung", component: UserListComponent },
  { path: "admin/nhom-quyen", component: RoleListComponent },
  { path: "admin/quan-ly-tep", component: FileManagerComponent },
  { path: "templates/button", component: ButtonComponent },
  { path: "templates/button", component: ButtonComponent },
  { path: "templates/input", component: InputComponent },
  { path: "templates/tabs", component: TabsComponent },
  { path: "admin/tai-lieu", component: PdfListComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
