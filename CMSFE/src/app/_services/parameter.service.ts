import { Injectable, ErrorHandler } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { map } from "rxjs/operators";
import { Observable, BehaviorSubject } from "rxjs";
import { GridDataResult } from "@progress/kendo-angular-grid";
import { TextConstant } from '../_constants';
@Injectable({
  providedIn: "root"
})
export class ParameterService extends BehaviorSubject<GridDataResult> {
  domain;
  // loading;
  query
  constructor(private http: HttpClient) {
    super(null);
    this.domain = environment.domain
    this.query = TextConstant.EMPTY
  }

  deleteMany(ids): Observable<any> {
    const arrayIds = ids.split(',')
    arrayIds.forEach(id => {
      this.query += `ids=${id}`
      if (arrayIds.indexOf(id)!=arrayIds.length - 1) {
        this.query += '&'
      }
    });
    return this.http.delete(`${this.domain}/api/Parameter/DeleteMany?${this.query}`)
  }

  createParameter(parameterDto): Observable<any> {
    return this.http.post(`${this.domain}/api/Parameter/Create`, parameterDto, { observe: 'response' })
  }

  updateParameter(parameterDto): Observable<any> {
    return this.http.put(`${this.domain}/api/Parameter/Update`, parameterDto, { observe: 'response' })
  }

  getById(id): Observable<any> {
    return this.http.get(`${this.domain}/api/Parameter/GetById?Id=${id}`)
  }

  GetParametersByPage(keyword, status, pageIndex, pageSize): Observable<any> {
    // this.loading = true;
    return this.http
      .get(
        `${this.domain}/api/Parameter/GetAllByParams?Keyword=${keyword}&Status=${status}&PageIndex=${pageIndex}&PageSize=${pageSize}`
      )
      .pipe(
        map(
          response =>
            <GridDataResult>{
              data: response["Data"],
              total: parseInt(response["TotalItems"], 10)
            }
        )
      );
  }
}
