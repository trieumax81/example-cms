import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { JwtTokenConstant } from '../_constants';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private domain
  private jwtHelperService: JwtHelperService
  private headers = new HttpHeaders({ 'Content-Type': 'application/json' })

  constructor(private http: HttpClient) {
    this.jwtHelperService = new JwtHelperService
    this.domain = environment.domain
   }

  login(loginDto): Observable<any> {
    return this.http.post(`${this.domain}/Token`, loginDto, { headers: this.headers })
  }

  IsAuthenticated() {
    const token = localStorage.getItem(JwtTokenConstant.X_ACCESS_TOKEN);
    if (!token) {
      return false;
    }
    return !this.jwtHelperService.isTokenExpired(token);
  }

  Logout() {
    localStorage.clear();
  }

  retrieveLoggedUser(): Observable<any> | any {
    return this.http.get(`${this.domain}/api/v1.0/users/current`)
  }
}
