import { Injectable, ErrorHandler } from "@angular/core";
import { environment } from "../../environments/environment";
import { map } from "rxjs/operators";
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';  
import { Observable, BehaviorSubject } from "rxjs";
import { GridDataResult } from "@progress/kendo-angular-grid";
import { TextConstant } from '../_constants';
@Injectable({
  providedIn: "root"
})

@Injectable({
  providedIn: 'root'
})
export class DocumentService extends BehaviorSubject<GridDataResult>{

  domain;
  // loading;
  query
  constructor(private http: HttpClient) {
    super(null);
    this.domain = environment.domain
    this.query = TextConstant.EMPTY
  }

  deleteMany(ids): Observable<any> {
    const arrayIds = ids.split(',')
    arrayIds.forEach(id => {
      this.query += `ids=${id}`
      if (arrayIds.indexOf(id)!=arrayIds.length - 1) {
        this.query += '&'
      }
    });
    return this.http.delete(`${this.domain}/api/Document/DeleteMany?${this.query}`)
  }

  createDocument(DocumentDto): Observable<any> {
    return this.http.post(`${this.domain}/api/Document/Create`, DocumentDto, { observe: 'response' })
  }

  updateDocument(DocumentDto): Observable<any> {
    return this.http.put(`${this.domain}/api/Document/Update`, DocumentDto, { observe: 'response' })
  }

  getById(id): Observable<any> {
    const headers = new HttpHeaders({'Access-Control-Allow-Origin' : '*', 'Content-Type': 'text/plain'}); 

    return this.http.get(`${this.domain}/api/Document/GetById?Id=${id}`,{headers})
  }

  GetDocumentsByPage(keyword, status, pageIndex, pageSize): Observable<any> {
    // this.loading = true;
    return this.http
      .get(
        `${this.domain}/api/Document/GetDocuments?Keyword=${keyword}&Status=${status}&PageIndex=${pageIndex}&PageSize=${pageSize}`
        
      )
      .pipe(
        map(
          response =>
            <GridDataResult>{
              data: response["Data"],
              total: parseInt(response["TotalItems"], 10)
            }
        )
      );
  }
}
