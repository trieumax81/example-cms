import { TestBed } from '@angular/core/testing';

import { MatDialogService } from './dialog.service';

describe('DialogService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MatDialogService = TestBed.get(MatDialogService);
    expect(service).toBeTruthy();
  });
});
