import { Injectable } from '@angular/core';
import { MatConfirmDialogComponent } from '../../components/admin/common';
import { MatDialog } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class MatDialogService {

  constructor(private dialog: MatDialog) { }

  openConfirmDialog(data:any) {
    return this.dialog.open(MatConfirmDialogComponent, {
      width: '456px',
      height: '190px',
      panelClass: 'custom-modalbox',
      data:{
        title: data.title,
        message: data.message,
        disableClose: true
      }
    })
  }
}
