import { Injectable, ErrorHandler } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { catchError, map, tap } from "rxjs/operators";
import { throwError, Observable, BehaviorSubject } from "rxjs";
import { GridComponent, GridDataResult } from "@progress/kendo-angular-grid";
import { TextConstant } from '../_constants';
@Injectable({
  providedIn: "root"
})
export class UserService extends BehaviorSubject<GridDataResult> {
  domain;
  loading;
  query
  constructor(private http: HttpClient) {
    super(null);
    this.domain = environment.domain;
    this.query = TextConstant.EMPTY;
  }

  GetByPage(keyword, status, pageIndex, pageSize): Observable<any> {
    this.loading = true;
    return this.http
      .get(`${this.domain}/api/User/GetUsers?Keyword=${keyword}&Status=${status}&PageIndex=${pageIndex}&PageSize=${pageSize}`)
      .pipe(
        map(
          response =>
            <GridDataResult>{
              data: response["Data"],
              total: parseInt(response["TotalItems"], 10)
            }
        )
      );
  }

  create(data): Observable<any> {
    return this.http.post(`${this.domain}/api/User/Create`, data, { observe: 'response' })
  }

  update(data): Observable<any> {
    return this.http.put(`${this.domain}/api/User/Update`, data, { observe: 'response' })
  }

  deleteMany(ids): Observable<any> {
    return this.http.post(`${this.domain}/api/User/DeleteMany`, ids );
  } 
  //others 
  getRoles(UserId): Observable<any> {
    this.loading = true;
    return this.http
      .get(`${this.domain}/api/User/GetRoles?UserId=${UserId}`)
      .pipe(
        map(
          response =>
            <GridDataResult>{
              data: response["Data"],
              total: parseInt(response["TotalItems"], 10)
            }
        )
      );
  }
}
