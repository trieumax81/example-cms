import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../environments/environment";
import { map } from "rxjs/operators";
import { Observable, BehaviorSubject } from "rxjs";
import { GridDataResult } from "@progress/kendo-angular-grid";
import { TextConstant } from '../../_constants';

@Injectable({
  providedIn: 'root'
})
export class IconService {

  domain
  constructor(private http: HttpClient) {
    this.domain = environment.domain
   }

  getIconsByPage(keyword, page, size): Observable<any> {
    return this.http.get(`${this.domain}/api/Common/GetIcons?Keyword=${keyword}&Page=${page}&PageSize=${size}`)
  }
}
