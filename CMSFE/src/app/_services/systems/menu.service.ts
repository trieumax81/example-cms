import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../environments/environment";
import { map } from "rxjs/operators";
import { Observable, BehaviorSubject } from "rxjs";
import { GridDataResult } from "@progress/kendo-angular-grid";
import { TextConstant } from "../../_constants";

@Injectable({
  providedIn: "root"
})
export class MenuService {
  domain;
  constructor(private http: HttpClient) {
    this.domain = environment.domain;
  }

  getByPage(keyword, status, pageIndex, pageSize): Observable<any> {
    return this.http.get(`${this.domain}/api/Menu/GetMenus?Keyword=${keyword}&Status=${status}&PageIndex=${pageIndex}&PageSize=${pageSize}`)
  }
  create(menuDto): Observable<any> {
    return this.http.post(`${this.domain}/api/Menu/Create`, menuDto, {
      observe: "response"
    });
  }

  update(menuDto): Observable<any> {
    return this.http.put(`${this.domain}/api/Menu/Update`, menuDto, {
      observe: "response"
    });
  }

  getById(id): Observable<any> {
    return this.http.get(`${this.domain}/api/Menu/GetById?Id=${id}`);
  }

  deleteMany(ids): Observable<any> {
    return this.http.post(`${this.domain}/api/Menu/DeleteMany`, ids);
  }

  ///////////////////////////////FUNCTIONS ORTHERS//////////////////////////////////
  getMenuParents(): Observable<any> {
    return this.http.get(`${this.domain}/api/Menu/GetMenuParents`);
  }
  getMenusByUser(): Observable<any> {
    return this.http.get(`${this.domain}/api/Menu/GetMenusByUser`);
  }

  getRoles(MenuId): Observable<any> {
    return this.http.get(`${this.domain}/api/Menu/GetRoles?MenuId=${MenuId}`)
  }
}
