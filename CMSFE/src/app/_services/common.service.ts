import { Injectable, ErrorHandler } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { catchError, map, tap } from "rxjs/operators";
import { throwError, Observable, BehaviorSubject } from "rxjs";
import { GridComponent, GridDataResult } from "@progress/kendo-angular-grid";
import { TextConstant } from '../_constants';
@Injectable({
  providedIn: "root"
})
export class CommonService extends BehaviorSubject<GridDataResult> {
  domain;
  loading;
  query
  constructor(private http: HttpClient) {
    super(null);
    this.domain = environment.domain;
    this.query = TextConstant.EMPTY;
  }
 
  getGenders(): Observable<any> {
    this.loading = true;
    return this.http
      .get(`${this.domain}/api/Common/GetGenders`)
      .pipe(
        map(
          response =>
            <GridDataResult>{
              data: response["Data"],
              total: parseInt(response["TotalItems"], 10)
            }
        )
      );
  }

  getAllUnits(): Observable<any> {
    return this.http.get(`${this.domain}/api/Common/GetUnits`)
  }
}
