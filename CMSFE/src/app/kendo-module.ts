import { GridModule } from '@progress/kendo-angular-grid';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { DateInputsModule, DateInputModule, DatePickerModule } from '@progress/kendo-angular-dateinputs';
import { DropDownListModule, DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { ExcelExportModule } from '@progress/kendo-angular-excel-export';
import { InputsModule } from '@progress/kendo-angular-inputs'
import { IntlModule } from '@progress/kendo-angular-intl';

import { NgModule } from "@angular/core";

@NgModule({
  exports: [
    GridModule,
    ButtonsModule,
    DateInputModule,
    DateInputsModule,
    DatePickerModule,
    DropDownListModule,
    DropDownsModule,
    ExcelExportModule,
    InputsModule,
    IntlModule
  ]
})

export class KendoModule {}