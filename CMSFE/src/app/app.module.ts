import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { MaterialModule } from "./material-module";
import { BlockUIModule } from "ng-block-ui";
import { NgbDropdownModule } from "@ng-bootstrap/ng-bootstrap";
import { ToastrModule } from "ngx-toastr";
import * as angularJwt from "@auth0/angular-jwt";
import { MatConfirmDialogComponent } from "./components/admin/common";
import { ErrorInterceptor, HttpAuthInterceptor } from "./_interceptor";

import { AuthenticationComponent, DashboardComponent } from "./components/admin";

import {
  ParameterListComponent,
  ParameterDetailComponent
} from "./components/admin/parameter";


import {
  HeaderComponent,
  FooterComponent,
  PopupChatComponent,
  AlertErrorComponent,
  AlertInfoComponent,
  ScrollToTopComponent,
  PopupSocialComponent,
  PageBreadcrumbComponent,
  ButtonActionFilterComponent
} from "./components/admin/common";

import { TableToolbarComponent } from "./components/admin/common/table-toolbar/table-toolbar.component";

import { KendoModule } from "./kendo-module";
import { PrimeNGModule } from "./prime-ng-module";
import { DialogService } from "primeng/api";
import { UserListComponent } from "./components/admin/users/user-list/user-list.component";
import { UserDetailComponent } from "./components/admin/users/user-detail/user-detail.component";
import { ButtonComponent, InputComponent, TabsComponent } from "./templates";


import { MenuListComponent, MenuDetailComponent } from "./components/admin/systems";

import { GridModule } from '@progress/kendo-angular-grid';

import { RoleListComponent } from './components/admin/roles/role-list/role-list.component';
import { RoleDetailComponent } from './components/admin/roles/role-detail/role-detail.component';
import { FileManagerComponent } from './components/admin/file-manager/file-manager.component';
import { HomeComponent } from './components/user/home/home.component';
import { UploadPdfComponent } from './components/admin/upload-pdf/upload-pdf.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { PdfListComponent, PdfDetailComponent} from './components/admin/pdf_file';
@NgModule({
  declarations: [
    AppComponent,
    MatConfirmDialogComponent,
    AuthenticationComponent,
    HeaderComponent,
    FooterComponent,
    PopupChatComponent,
    AlertErrorComponent,
    ScrollToTopComponent,
    PopupSocialComponent,
    DashboardComponent,
    PageBreadcrumbComponent,
    TableToolbarComponent,
    ButtonActionFilterComponent,
    AlertInfoComponent,
    UserListComponent,
    UserDetailComponent,
    ButtonComponent,
    InputComponent,
    TabsComponent,
    RoleListComponent,
    RoleDetailComponent,
    MenuListComponent,
    MenuDetailComponent,
    ParameterListComponent,
    ParameterDetailComponent,
    FileManagerComponent,
    HomeComponent,
    UploadPdfComponent,
    PdfListComponent,
    PdfDetailComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    MaterialModule,
    KendoModule,
    angularJwt.JwtModule,
    NgbDropdownModule,
    PrimeNGModule,
    BlockUIModule.forRoot(),
    ToastrModule.forRoot(),
    GridModule,
    PdfViewerModule
  ],
  entryComponents: [
    MatConfirmDialogComponent,
    ParameterDetailComponent,
    MenuDetailComponent,
    UserDetailComponent,
    RoleDetailComponent,
    PdfDetailComponent
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: HttpAuthInterceptor, multi: true },
    DialogService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
