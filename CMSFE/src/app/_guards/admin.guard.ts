import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { AuthService } from '../_services';
import { JwtTokenConstant } from '../_constants';

@Injectable({ providedIn: 'root' })
export class AdminGuard implements CanActivate {
  private jwtHelperService: JwtHelperService

  constructor(public authService: AuthService, public router: Router) {
    this.jwtHelperService = new JwtHelperService;
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | import("@angular/router").UrlTree | import("rxjs").Observable<boolean | import("@angular/router").UrlTree> | Promise<boolean | import("@angular/router").UrlTree> {
    // this will be passed from the route config
    // on the data property
    const expectedRole = route.data.expectedRole
    // console.log(expectedRole);
    const token = localStorage.getItem(JwtTokenConstant.X_ACCESS_TOKEN)
    // decode the token to get its payload
    const tokenPayload = this.jwtHelperService.decodeToken(token)

    // if (this.authService.IsAuthenticated() && tokenPayload.authorities.some(item => item.authority === expectedRole)) {
    //   // console.log("AdminGuard");
    //   return true;
    // }

    this.router.navigate(['/403'], { queryParams: { returnUrl: state.url } });
    return false
  }

}