import { Component } from '@angular/core';
import { MatDialogService } from './_services/common';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [
    './app.component.css']
})
export class AppComponent {
  constructor(private dialogService: MatDialogService, private toastrService: ToastrService) {
  }

  ngOnInit() {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    // setTimeout(() => this.toastrService.success('sup'))
  }

  // testCommon() {
  //   const dataDialog = {
  //     title: 'Thông báo',
  //     message: 'Test?'
  //   }

  //   this.dialogService.openConfirmDialog(dataDialog)
  //   .afterClosed() 
  //   .subscribe(res => {
  //     if (res) {
  //       this.toastrService.success('success', 'successfully', {timeOut: 3000})
  //     } 
  //   })
  // }
}
