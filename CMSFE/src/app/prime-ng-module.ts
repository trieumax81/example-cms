import { NgModule } from "@angular/core";
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { ToastModule } from 'primeng/toast';
import { MultiSelectModule } from 'primeng/multiselect';
import { ColorPickerModule } from 'primeng/colorpicker';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { PickListModule } from 'primeng/picklist';
import {InputSwitchModule} from 'primeng/inputswitch';
import {CalendarModule} from 'primeng/calendar';
@NgModule({
  exports: [
    DropdownModule,
    InputTextModule,
    DynamicDialogModule,
    ToastModule,
    MultiSelectModule,
    ColorPickerModule,
    ScrollPanelModule,
    PickListModule,
    InputSwitchModule,
    CalendarModule
  ]
})

export class PrimeNGModule {}