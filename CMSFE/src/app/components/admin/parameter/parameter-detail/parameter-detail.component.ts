import { Component, OnInit } from '@angular/core';
import { Parameter } from '../../../../_entities';
import { ParameterService } from '../../../../_services';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/api';
import { ToastrService } from 'ngx-toastr';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { TextConstant } from '../../../../_constants';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-parameter-detail',
  templateUrl: './parameter-detail.component.html',
  styleUrls: ['./parameter-detail.component.css']
})
export class ParameterDetailComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI
  id
  parameter: Parameter
  statusDefined
  
  constructor(private parameterService: ParameterService, public ref: DynamicDialogRef, public config: DynamicDialogConfig, private toastrService: ToastrService) {
    this.statusDefined = [{
      label: 'Hoạt động', value: 'Active'
    }, {
      label: 'Ngưng hoạt động', value: 'Inactive'
    }]
    
    this.parameter = new Parameter()
   }

  async ngOnInit() {
    // get data from parameters component (data: id)
    if (this.config.data && this.config.data.id) {
      this.id = this.config.data.id
      await this.getParameterById(this.id)
    }
  }

  async onSubmit() {
    if (!this.id) {
      await this.createParameter(this.parameter)
    } else {
      await this.updateParameter(this.parameter)
    }
  }

  async getParameterById(parameterId) {
    this.blockUI.start(TextConstant.LOADING)
    await this.parameterService.getById(parameterId)
      .pipe(first())
      .subscribe(res => {
        if (res.Issucess === false) {
          this.toastrService.error('Không tìm thấy dữ liệu!', 'Thông báo')
        } else {
          this.parameter = res.Data
        }

        this.blockUI.stop()
      }, err => {
        this.toastrService.error('Không tìm thấy dữ liệu!', 'Thông báo')
        this.blockUI.stop()
      })
  }

  async createParameter(parameterDto) {
    this.blockUI.start(TextConstant.LOADING)
    await this.parameterService.createParameter(parameterDto)
      .pipe(first())
      .subscribe(res => {
        if (res.body.IsSuccess === false) {
          this.toastrService.error('Thêm dữ liệu không thành công!','Thông báo')
        } else {
          this.toastrService.success('Thêm dữ liệu thành công!', 'Thông báo')
          this.ref.close(res.body)
        }
        this.blockUI.stop()
      }, err => {
        this.toastrService.error('Thêm dữ liệu không thành công!','Thông báo')
        this.blockUI.stop()
      })
  }

  async updateParameter(parameterDto) {
    this.blockUI.start(TextConstant.LOADING)
    await this.parameterService.updateParameter(parameterDto)
      .pipe(first())
      .subscribe(res => {
        console.log(res)
        if (res.body.IsSuccess === false) {
          this.toastrService.error('Sửa dữ liệu không thành công!', 'Thông báo')
        } else {
          this.toastrService.success('Sửa dữ liệu thành công!', 'Thông báo')
          this.ref.close(res.body)
        }
        this.blockUI.stop()
      }, err => {
        this.toastrService.error('Sửa dữ liệu không thành công!', 'Thông báo')
        this.blockUI.stop()
      })    
  }

  onClosePopup() {
    this.ref.close(undefined)
  }
}
