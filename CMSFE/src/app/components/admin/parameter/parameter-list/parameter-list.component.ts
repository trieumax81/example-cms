import { Component, OnInit } from "@angular/core";
import { Parameter } from "../../../../_entities";
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ParameterDetailComponent } from '../parameter-detail/parameter-detail.component';
import { DialogService } from 'primeng/api';
import { ParameterService } from '../../../../_services';
import { TextConstant, NumberConstant, StatusConstant } from '../../../../_constants';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { ToastrService } from 'ngx-toastr';
declare var $: any;
@Component({
  selector: "app-parameters",
  templateUrl: "./parameter-list.component.html",
  styleUrls: [
    "../../../../../assets/custom/css/style.css",
    "./parameter-list.component.css"
  ]
})
export class ParameterListComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI

  parameters: Parameter[];
  gridData: GridDataResult
  pageSize: number
  pageIndex: number
  skip
  arrIds = [];
  textFilter: string
  statusFilter
  statusDefined
  kendoLoading = false
  constructor(private paramSrv: ParameterService, private primeDialogService: DialogService, private toastrService: ToastrService) {
    this.statusDefined = [{
      label: 'Hoạt động', value: 'Active'
    }, {
      label: 'Ngưng hoạt động', value: 'Inactive'
    }]

    this.pageSize = NumberConstant.PAGE_SIZE_DEFAULT
    this.pageIndex = NumberConstant.PAGE_INDEX_DEFAULT
    this.skip = NumberConstant.PAGE_SKIP_DEFAULT
    this.textFilter = TextConstant.EMPTY
    this.statusFilter = StatusConstant.ACTIVE
  }

  ngOnInit() {
    this.getAllByParams()
  }

  async getDataAfterCreated(e) {
    if(e.IsSuccess === true) {
      this.getAllByParams()
    }
  }
  public ngAfterViewInit() {
    this.resizeGrid();
  }

  private resizeGrid() {
    var grid = $("#grid");
    var offsetbottom =
      parseInt($(window).height()) - parseInt(grid.offset().top);
    var gridElement = grid,
      dataArea = gridElement.find(".k-grid-content"),
      heightHeader = $(".k-grid-header", grid).outerHeight() || 0,
      heightToolbar = $(".k-grid-toolbar", grid).outerHeight() || 0,
      heightFooter = $(".k-pager-wrap", grid).outerHeight() || 0;
    dataArea.height(
      offsetbottom - heightHeader - heightToolbar - heightFooter - 25
    );
  }
  async deleteMany(e) {
    return this.paramSrv.deleteMany(e)
      .subscribe(async res => {
        if (res.IsSuccess === true) {
          this.toastrService.success('Xoá dữ liệu thành công!', 'Thông báo')
          await this.getAllByParams()
        } else {
          this.toastrService.error('Xóa dữ liệu thất bại!', 'Thông báo')
        }
      })
  }

  async getAllByParams(keyword = TextConstant.EMPTY, status = StatusConstant.ACTIVE, pageIndex = this.pageIndex, pageSize = this.pageSize) {
    this.kendoLoading = true
    
    await this.paramSrv.GetParametersByPage(keyword, status, pageIndex, pageSize)
      .subscribe(res => {
        this.kendoLoading = false
        this.gridData = res
        this.blockUI.stop()
      })
  }

  pageChange({ skip, take }: PageChangeEvent): void {
    this.pageIndex = (skip/take) + 1
    this.pageSize = take
    this.skip = skip
    this.getAllByParams(TextConstant.EMPTY, StatusConstant.ACTIVE, this.pageIndex, this.pageSize)
  }

  onToggleCheckbox(e) {
    if (e.checked) {
      this.arrIds.push(e.source.value);
    } else if (!e.checked) {
      this.arrIds.splice(this.arrIds.indexOf(e.source.value), 1);
    }
  }

  async showPopupEdit(parameterId) {
    const ref = this.primeDialogService.open(ParameterDetailComponent, {
      data: {
        id: parameterId
      },
      header: 'Thông tin Dữ liệu dùng chung',
      width: '50%',
      autoZIndex: false
    })

    ref.onClose.subscribe(res => {
      res === undefined ? undefined : this.getAllByParams()
    })
  }

  async filter() {
    await this.getAllByParams(this.textFilter.trim(), this.statusFilter, this.pageIndex, this.pageSize)
  }

  async cancelFilter() {
    this.textFilter = TextConstant.EMPTY
    await this.getAllByParams(this.textFilter.trim(), this.statusFilter, this.pageIndex, this.pageSize)
  }
}
