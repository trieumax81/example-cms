import {
  Component,
  OnInit,
  Input,
  Output,
  ElementRef,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  HostListener,
  OnDestroy
} from "@angular/core";
import { DynamicDialogRef, DynamicDialogConfig } from "primeng/api";
import { ToastrService } from "ngx-toastr";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { GridDataResult } from "@progress/kendo-angular-grid";
import { first } from "rxjs/operators";
import {
  PDFDocumentProxy,
  PDFViewerParams,
  PDFPageProxy,
  PDFSource,
  PDFProgressData,
  PDFPromise
} from "pdfjs-dist";
let PDFJS: any;
let PDFJSViewer: any;

function isSSR() {
  return typeof window === 'undefined';
}


export enum RenderTextMode {
  DISABLED,
  ENABLED,
  ENHANCED
}
@Component({
  selector: "app-upload-pdf",
  templateUrl: "./upload-pdf.component.html",
  styleUrls: [
    "./upload-pdf.component.scss",
    "../../../../assets/custom/css/style.css"
  ]
})
export class UploadPdfComponent implements OnInit {
  pdfSrc: string = "";
  constructor() {}

  ngOnInit() {}
  onFileSelected() {
    let $img: any = document.querySelector("#file");

    if (typeof FileReader !== "undefined") {
      let reader = new FileReader();

      reader.onload = (e: any) => {
        this.pdfSrc = e.target.result;
        console.log("pdfSrc", $img.files[0]);
      };

      reader.readAsArrayBuffer($img.files[0]);
    }
  }
}
