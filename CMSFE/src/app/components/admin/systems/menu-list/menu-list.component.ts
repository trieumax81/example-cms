import { Component, OnInit } from "@angular/core";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { Menu } from "../../../../_entities";
import {
  GridDataResult,
  PageChangeEvent,
  GridComponent
} from "@progress/kendo-angular-grid";
import { MenuService } from "../../../../_services/systems";
import { DialogService } from "primeng/api";
import { ToastrService } from "ngx-toastr";
import {
  TextConstant,
  StatusConstant,
  NumberConstant
} from "../../../../_constants";
import { MenuDetailComponent } from "../menu-detail/menu-detail.component";
import { isNullOrUndefined } from "util";
declare var $: any;
@Component({
  selector: "app-menu-list",
  templateUrl: "./menu-list.component.html",
  styleUrls: [
    "../../../../../assets/custom/css/style.css",
    "./menu-list.component.css"
  ]
})
export class MenuListComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;

  menus: Menu[];
  gridData: GridDataResult;
  arrIds = [];
  statusFilter;
  statusDefined;

  textFilter = TextConstant.EMPTY;

  public pageSize = 100;
  public pageIndex = NumberConstant.PAGE_INDEX_DEFAULT;
  public skip = NumberConstant.PAGE_SKIP_DEFAULT;
  public kendoLoading = false;
  public checkboxAction = 0;

  constructor(
    private menuService: MenuService,
    private dialogService: DialogService,
    private toastrService: ToastrService
  ) {
    this.statusDefined = [
      {
        label: "Hoạt động",
        value: StatusConstant.ACTIVE
      },
      {
        label: "Ngưng hoạt động",
        value: StatusConstant.INACTIVE
      }
    ];
  }

  async ngOnInit() {
    await this.getMenus();
  }

  async getMenus(
    keyword = TextConstant.EMPTY,
    status = StatusConstant.ACTIVE,
    pageIndex = this.pageIndex,
    pageSize = this.pageSize
  ) {
    this.kendoLoading = true;
    this.menuService
      .getByPage(keyword, status, pageIndex, pageSize)
      .subscribe(res => {
        this.kendoLoading = false;
        this.gridData = <GridDataResult>{
          data: res.Data,
          total: res.TotalItems
        }
      });
  }

  public ngAfterViewInit() {
    this.resizeGrid();
  }

  private resizeGrid() {
    var grid = $("#grid");
    var offsetbottom =
      parseInt($(window).height()) - parseInt(grid.offset().top);
    var gridElement = grid,
      dataArea = gridElement.find(".k-grid-content"),
      heightHeader = $(".k-grid-header", grid).outerHeight() || 0,
      heightToolbar = $(".k-grid-toolbar", grid).outerHeight() || 0,
      heightFooter = $(".k-pager-wrap", grid).outerHeight() || 0;
    dataArea.height(
      offsetbottom - heightHeader - heightToolbar - heightFooter - 25
    );
  }

  async pageChange({ skip, take }: PageChangeEvent) {
    this.pageIndex = skip / take + 1;
    this.pageSize = take;
    this.skip = skip;
    this.getMenus(
      this.textFilter.trim(),
      this.statusFilter,
      this.pageIndex,
      this.pageSize
    );
  }

  async filter() {
    this.getMenus(
      this.textFilter.trim(),
      this.statusFilter,
      this.pageIndex,
      this.pageSize
    );
  }

  async cancelFilter() {
    this.textFilter = TextConstant.EMPTY;
    await this.getMenus(
      this.textFilter.trim(),
      this.statusFilter,
      this.pageIndex,
      this.pageSize
    );
  }

  ///////////////////////////////Checkbox///////////////////////////////
  private checkedAllRow() {
    var length = this.gridData.data.length;
    var checked = 0;
    this.gridData.data.map(m => {
      if (m.checked) checked++;
    });
    if (checked == 0) {
      this.checkboxAction = 0;
    } else {
      this.checkboxAction = checked == length ? 1 : 2;
    }
  }
  checkAllRow(e) {
    var x = e.checked;
    this.checkboxAction = x ? 1 : 0;
    this.gridData.data.map(m => {
      m.checked = x;
    });
  }

  checkRow(e, Id) {
    var x = e.checked;
    var dataItem = this.gridData.data.find(x => x.Id === Id);
    dataItem.checked = x;
    this.checkedAllRow();

    if (e.checked) {
      this.arrIds.push(Id);
    } else if (!e.checked) {
      this.arrIds.splice(this.arrIds.indexOf(Id), 1);
    }
  }

  ///////////////////////////////Thêm/Sửa/Xóa///////////////////////////////
  async onCreate(e) {
    let dialog = this.dialogService.open(MenuDetailComponent, {
      header: "Thêm menu",
      width: "calc(100% - 30px)",
      style: {
        'max-width': '600px',
      },
      autoZIndex: false
    });

    dialog.onClose.subscribe(res => {
      if (res) {
        this.getDataAfterCreatedorUpdate();
      }
    })
  }
  async editRecord(Id) {
    // let dataItem = this.gridData.data.find(x => x.Id === Id);
    let dialog = this.dialogService.open(MenuDetailComponent, {
      data: {
        id: Id,
        component: this
      },
      header: "Sửa menu",
      width: "calc(100% - 30px)",
      style: {
        'max-width': '600px',
      },
      autoZIndex: false
    });
    dialog.onClose.subscribe(res => {
      if (res) {
        this.getDataAfterCreatedorUpdate();
      }
    })
  }

  async deleteMany(e) {
    return this.menuService.deleteMany(this.arrIds).subscribe(async res => {
      if (res.IsSuccess === true) {
        this.toastrService.success("Xoá dữ liệu thành công!", "Thông báo");
        await this.getMenus();
      } else {
        this.toastrService.error("Xóa dữ liệu thất bại!", "Thông báo");
      }
    });

  }

  async getDataAfterCreatedorUpdate() {
    await this.getMenus(
      this.textFilter ? this.textFilter.trim() : '',
      this.statusFilter,
      this.pageIndex,
      this.pageSize
    );
  }
}
