import { Component, OnInit, Output } from '@angular/core';
import { MenuService, IconService } from '../../../../_services/systems';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/api';
import { ToastrService } from 'ngx-toastr';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Menu } from '../../../../_entities';
import { TextConstant, NumberConstant } from '../../../../_constants';
import { first } from 'rxjs/operators';
import { Icon } from '../../../../_entities/icon';
import { GridDataResult } from '@progress/kendo-angular-grid';
import { resolve } from 'url';

@Component({
  selector: 'app-menu-detail',
  templateUrl: './menu-detail.component.html',
  styleUrls: ['./menu-detail.component.css']
})
export class MenuDetailComponent implements OnInit {

  @Output() outputAfterCreated
  @BlockUI() blockUI: NgBlockUI
  id
  statusDefined
  visible
  menu: Menu
  icons: Icon[]
  menuParents: Menu[]
  iconSelected

  public checkboxAction = 0;


  constructor(private menuService: MenuService, private iconService: IconService, public ref: DynamicDialogRef, public config: DynamicDialogConfig, private toastrService: ToastrService) {
    this.statusDefined = [{
      label: 'Hoạt động', value: 'Active'
    }, {
      label: 'Ngưng hoạt động', value: 'Inactive'
    }]
    this.menu = new Menu()
  }

  async ngOnInit() {
    await this.getIcons()
    await this.getMenuParents()
    if (this.config.data && this.config.data.id) {
      this.id = this.config.data.id
      await this.getMenuById(this.id)
    }
  }

  async getMenuParents() {
    return new Promise((resolve, reject) => {
      this.menuService.getMenuParents()
        .subscribe(res => {
          if (res.IsSuccess === true) {
            res.Data.map(item => {
              item.label = item.Name
              item.value = item.Id
            })
            this.menuParents = res.Data
            resolve()
          } else {
            reject()
          }
        })
    })
  }

  async getIcons(keyword = TextConstant.EMPTY, pageIndex = NumberConstant.PAGE_INDEX_DEFAULT, pageSize = 10000) {
    return new Promise((resolve, reject) => {
      this.iconService.getIconsByPage(keyword, pageIndex, pageSize)
        .subscribe(res => {
          if (res.IsSuccess === true) {
            this.icons = res.Data
            resolve()
          } else {
            reject()
          }
        })
    })
  }

  async getMenuById(menuId) {
    this.blockUI.start(TextConstant.LOADING)
    await this.menuService.getById(menuId)
      .pipe(first())
      .subscribe(res => {
        if (res.IsSuccess === false) {
          this.toastrService.error('Không tìm thấy dữ liệu!', ' Thông báo')
        } else {
          this.menu = res.Data
        }
        this.blockUI.stop()
      })
  }
  
  ////////////////////////////HANDEL SAVES////////////////////////////////
  async onSubmit() {
    console.log(this.id, 'sumit')
    if (!this.id) {
      await this.create(this.menu)
    } else {
      await this.update(this.menu)
    }
  }

  async create(menuDto) {
    this.blockUI.start(TextConstant.LOADING)
    await this.menuService.create(menuDto)
      .pipe(first())
      .subscribe(res => {
        if (res.body.IsSuccess === false) {
          this.toastrService.error('Thêm dữ liệu không thành công!', 'Thông báo')
        } else {
          this.toastrService.success('Thêm dữ liệu thành công!', 'Thông báo')
          this.ref.close(res.body)
        }
        this.blockUI.stop()
      }, err => {
        this.toastrService.error('Thêm dữ liệu không thành công!', 'Thông báo')
        this.blockUI.stop()
      })
  }

  async update(menuDto) {
    this.blockUI.start(TextConstant.LOADING)
    await this.menuService.update(menuDto)
      .pipe(first())
      .subscribe(res => {
        if (res.body.IsSuccess === false) {
          this.toastrService.error('Sửa dữ liệu không thành công!', 'Thông báo')
        } else {
          this.toastrService.success('Sửa dữ liệu thành công!', 'Thông báo')
          this.ref.close(res.body)
        }
        this.blockUI.stop()
      }, err => {
        this.toastrService.error('Sửa dữ liệu không thành công!', 'Thông báo')
        this.blockUI.stop()
      })
  }

  closePopup() {
    this.ref.close(undefined)
  }

}
