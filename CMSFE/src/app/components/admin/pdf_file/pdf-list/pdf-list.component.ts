import { Component, OnInit } from "@angular/core";
import { Document } from "../../../../_entities";
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { PdfDetailComponent } from '../pdf-detail/pdf-detail.component';
import { DialogService } from 'primeng/api';
import { DocumentService } from '../../../../_services';
import { TextConstant, NumberConstant, StatusConstant } from '../../../../_constants';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { ToastrService } from 'ngx-toastr';
import { first } from 'rxjs/operators';

declare var $: any;
@Component({
  selector: 'app-pdf-list',
  templateUrl: './pdf-list.component.html',
  styleUrls: ['./pdf-list.component.css',
  "../../../../../assets/custom/css/style.css"]
})
export class PdfListComponent implements OnInit {

  @BlockUI() blockUI: NgBlockUI
  pdfSrc: string = "";
  gridData: GridDataResult
  pageSize: number
  pageIndex: number
  skip
  arrIds = [];
  textFilter: string
  statusFilter
  statusDefined
  kendoLoading = false
  document: Document
  constructor( private documentService: DocumentService,private primeDialogService: DialogService, private toastrService: ToastrService) {
    this.statusDefined = [{
      label: 'Hoạt động', value: 'Active'
    }, {
      label: 'Ngưng hoạt động', value: 'Inactive'
    }]

    this.pageSize = NumberConstant.PAGE_SIZE_DEFAULT
    this.pageIndex = NumberConstant.PAGE_INDEX_DEFAULT
    this.skip = NumberConstant.PAGE_SKIP_DEFAULT
    this.textFilter = TextConstant.EMPTY
    this.statusFilter = StatusConstant.ACTIVE
    this.document = new Document();
  }


  ngOnInit() {
    this.getDocumentAllBy();
  }
  async getDocumentAllBy(keyword = TextConstant.EMPTY, status = StatusConstant.ACTIVE, pageIndex = this.pageIndex, pageSize = this.pageSize) {
    this.kendoLoading = true
    
    await this.documentService.GetDocumentsByPage(keyword, status, pageIndex, pageSize)
      .subscribe(res => {
        this.kendoLoading = false
        this.gridData = res
        console.log("res",res);
        this.blockUI.stop()
      })
  }
  public ngAfterViewInit() {
    this.resizeGrid();
  }

  private resizeGrid() {
    var grid = $("#grid");
    var offsetbottom =
      parseInt($(window).height()) - parseInt(grid.offset().top);
    var gridElement = grid,
      dataArea = gridElement.find(".k-grid-content"),
      heightHeader = $(".k-grid-header", grid).outerHeight() || 0,
      heightToolbar = $(".k-grid-toolbar", grid).outerHeight() || 0,
      heightFooter = $(".k-pager-wrap", grid).outerHeight() || 0;
    dataArea.height(
      offsetbottom - heightHeader - heightToolbar - heightFooter - 25
    );
  }
  pageChange({ skip, take }: PageChangeEvent): void {
    this.pageIndex = (skip/take) + 1
    this.pageSize = take
    this.skip = skip
    this.getDocumentAllBy(TextConstant.EMPTY, StatusConstant.ACTIVE, this.pageIndex, this.pageSize)
  }

  onToggleCheckbox(e) {
    if (e.checked) {
      this.arrIds.push(e.source.value);
    } else if (!e.checked) {
      this.arrIds.splice(this.arrIds.indexOf(e.source.value), 1);
    }
  }

  async showPopupEdit(Id) {
    const ref = this.primeDialogService.open(PdfDetailComponent, {
      data: {
        id: Id
      },
      header: 'Thông tin Tài liệu',
      width: '80%'
    })

    ref.onClose.subscribe(res => {
      res === undefined ? undefined : this.getDocumentAllBy()
    })
  }
  async showPopupView(Id) {
    this.blockUI.start(TextConstant.LOADING)
    await this.documentService.getById(Id)
      .pipe(first())
      .subscribe(res => {
        if (res.Issucess === false) {
          this.toastrService.error('Không tìm thấy dữ liệu!', 'Thông báo')
        } else {
          this.document = res.Data
          this.pdfSrc = this.document.Url;
          console.log("this.pdfSrc",this.pdfSrc)
        }

        this.blockUI.stop()
      }, err => {
        this.toastrService.error('Không tìm thấy dữ liệu!', 'Thông báo')
        this.blockUI.stop()
      })
  }

  async filter() {
    await this.getDocumentAllBy(this.textFilter.trim(), this.statusFilter, this.pageIndex, this.pageSize)
  }

  async cancelFilter() {
    this.textFilter = TextConstant.EMPTY
    await this.getDocumentAllBy(this.textFilter.trim(), this.statusFilter, this.pageIndex, this.pageSize)
  }
}
