import { Component, OnInit } from '@angular/core';
import { Document } from '../../../../_entities';
import { DocumentService } from '../../../../_services';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/api';
import { ToastrService } from 'ngx-toastr';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { TextConstant ,StatusConstant} from '../../../../_constants';
import { first } from 'rxjs/operators';
@Component({
  selector: 'app-pdf-detail',
  templateUrl: './pdf-detail.component.html',
  styleUrls: ['./pdf-detail.component.scss']
})
export class PdfDetailComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI
  id
  document: Document
  statusDefined
  pdfSrc: string = "";
  constructor(private documentService: DocumentService, public ref: DynamicDialogRef, public config: DynamicDialogConfig, private toastrService: ToastrService) {
    this.statusDefined = [{
      label: 'Hoạt động', value: StatusConstant.ACTIVE
    }, {
      label: 'Ngưng hoạt động', value: StatusConstant.INACTIVE
    }]
    
    this.document = new Document()
    this.document.Status = StatusConstant.ACTIVE
   }

  async ngOnInit() {
    // get data from documents component (data: id)
    if (this.config.data && this.config.data.id) {
      this.id = this.config.data.id
      await this.getDocumentById(this.id);
      
    }
  }

  async onSubmit() {
    if (!this.id) {
      await this.createDocument(this.document)
    } else {
      await this.updateDocument(this.document)
    }
  }

  async getDocumentById(documentId) {
    this.blockUI.start(TextConstant.LOADING)
    await this.documentService.getById(documentId)
      .pipe(first())
      .subscribe(res => {
        if (res.Issucess === false) {
          this.toastrService.error('Không tìm thấy dữ liệu!', 'Thông báo')
        } else {
          this.document = res.Data
          this.pdfSrc = this.document.Url;
          console.log("this.pdfSrc",this.pdfSrc)
        }

        this.blockUI.stop()
      }, err => {
        this.toastrService.error('Không tìm thấy dữ liệu!', 'Thông báo')
        this.blockUI.stop()
      })
  }

  async createDocument(documentDto) {
    this.blockUI.start(TextConstant.LOADING)
    await this.documentService.createDocument(documentDto)
      .pipe(first())
      .subscribe(res => {
        if (res.body.IsSuccess === false) {
          this.toastrService.error('Thêm dữ liệu không thành công!','Thông báo')
        } else {
          this.toastrService.success('Thêm dữ liệu thành công!', 'Thông báo')
          this.ref.close(res.body)
        }
        this.blockUI.stop()
      }, err => {
        this.toastrService.error('Thêm dữ liệu không thành công!','Thông báo')
        this.blockUI.stop()
      })
  }

  async updateDocument(documentDto) {
    this.blockUI.start(TextConstant.LOADING)
    await this.documentService.updateDocument(documentDto)
      .pipe(first())
      .subscribe(res => {
        console.log(res)
        if (res.body.IsSuccess === false) {
          this.toastrService.error('Sửa dữ liệu không thành công!', 'Thông báo')
        } else {
          this.toastrService.success('Sửa dữ liệu thành công!', 'Thông báo')
          this.ref.close(res.body)
        }
        this.blockUI.stop()
      }, err => {
        this.toastrService.error('Sửa dữ liệu không thành công!', 'Thông báo')
        this.blockUI.stop()
      })    
  }

  onClosePopup() {
    this.ref.close(undefined)
  }

}
