import { Component, OnInit } from '@angular/core';
import { User } from '../../../../_entities';
import { UserService, CommonService } from '../../../../_services';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/api';
import { ToastrService } from 'ngx-toastr';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { TextConstant } from '../../../../_constants';
import { GridDataResult } from '@progress/kendo-angular-grid';
import { first } from 'rxjs/operators';
@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI
  user: User
  gridData: GridDataResult
  statusDefined
  listGender

  public checkboxAction = 0;

  constructor(private userService: UserService
    , private commonService: CommonService
    , public ref: DynamicDialogRef
    , public config: DynamicDialogConfig
    , private toastrService: ToastrService) {
    this.statusDefined = [{
      label: 'Hoạt động', value: 'Active'
    }, {
      label: 'Ngưng hoạt động', value: 'Inactive'
    }]
    this.user = new User()
  }

  ngOnInit() {
    // get data from users component (data: id)
    if (this.config.data) {
      this.user = this.config.data;
      console.log("user",this.user)
    }
    this.getRoles(this.user.Id);
    this.getGenders();
  }
  private async getGenders() {
    await this.commonService.getGenders().subscribe(res => {
      this.listGender = res.data;
    });

  }
  private async getRoles(UserId) {
    await this.userService.getRoles(UserId)
      .subscribe(res => {
        res.data.map(m => {
          m.checked = m.UserId ? true : false;
        });
        this.gridData = res
        this.checkedAllRow();
      })
  }

  private checkedAllRow() {
    var length = this.gridData.data.length;
    var checked = 0;
    this.gridData.data.map(m => {
      if (m.checked) checked++;
    });
    if (checked == 0) {
      this.checkboxAction = 0;
    } else {
      this.checkboxAction = checked == length ? 1 : 2;
    }
  }
  checkAllRow(e) {
    var x = e.checked;
    this.checkboxAction = x ? 1 : 0;
    this.gridData.data.map(m => {
      m.checked = x;
    });
  }

  checkRow(e, RoleId) {
    var x = e.checked;
    var dataItem = this.gridData.data.find(x => x.RoleId === RoleId);
    dataItem.checked = x;
    this.checkedAllRow();
  }

  async onSubmit() {
    this.user.RoleIds = [];
    this.gridData.data.map(item => {
      if (item.checked) this.user.RoleIds.push(item.RoleId);
    });
    if (!this.user.Id) {
      await this.createUser(this.user)
    } else {
      await this.updateUser(this.user)
    }
  }
  //thêm user
  async createUser(data) {
    this.blockUI.start(TextConstant.LOADING)
    await this.userService.create(data)
      .pipe(first())
      .subscribe(res => {
        if (res.body.IsSuccess === false) {
          this.toastrService.error('Thêm dữ liệu không thành công!', 'Thông báo')
        } else {
          this.toastrService.success('Thêm dữ liệu thành công!', 'Thông báo')
          this.ref.close(res.body)
        }
        this.blockUI.stop()
      }, err => {
        this.toastrService.error('Thêm dữ liệu không thành công!', 'Thông báo')
        this.blockUI.stop()
      })
  }
  //cập nhật user
  async updateUser(data) {
    this.blockUI.start(TextConstant.LOADING)
    await this.userService.update(data)
      .pipe(first())
      .subscribe(res => {
        console.log(res)
        if (res.body.IsSuccess === false) {
          this.toastrService.error('Sửa dữ liệu không thành công!', 'Thông báo')
        } else {
          this.toastrService.success('Sửa dữ liệu thành công!', 'Thông báo')
          this.ref.close(res.body)
        }
        this.blockUI.stop()
      }, err => {
        this.toastrService.error('Sửa dữ liệu không thành công!', 'Thông báo')
        this.blockUI.stop()
      })
  }

  onClosePopup() {
    this.ref.close()
  }
}
