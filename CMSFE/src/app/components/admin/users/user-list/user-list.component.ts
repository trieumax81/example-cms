import { Component, OnInit, AfterViewInit } from "@angular/core";
import { ToastrService } from 'ngx-toastr';
import * as $ from "jquery";
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { UserDetailComponent } from '../user-detail/user-detail.component';
import { DialogService } from 'primeng/api';
import { UserService } from '../../../../_services'
import { TextConstant, NumberConstant } from '../../../../_constants';
import { GridDataResult, PageChangeEvent, DataStateChangeEvent } from '@progress/kendo-angular-grid';
import { State } from '@progress/kendo-data-query';

@Component({
  selector: "app-users",
  templateUrl: "./user-list.component.html",
  styleUrls: [
    "../../../../../assets/custom/css/style.css",
    "./user-list.component.css"
  ]
})
export class UserListComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI
  arrIds = [];
  searchText: string
  statusDefined
  gridData: GridDataResult
  textFilter: string
  statusFilter: string

  public buttonCount = 5;
  public info = true;
  public type: 'numeric' | 'input' = 'numeric';
  public pageSize = NumberConstant.PAGE_SIZE_DEFAULT;
  public pageIndex = NumberConstant.PAGE_INDEX_DEFAULT;
  public skip = NumberConstant.PAGE_SKIP_DEFAULT;
  public kendoLoading = false;
  public checkboxAction = 0;

  constructor(private dialogService: DialogService, private userService: UserService, private toastrService: ToastrService) {
    this.statusDefined = [{
      label: 'Hoạt động', value: 'true'
    }, {
      label: 'Ngưng hoạt động', value: 'false'
    }]
  }

  ngOnInit() {
    this.getUsers()
  }
  ///////////////////////////////Lưới///////////////////////////////
  private async getUsers(keyword = TextConstant.EMPTY, status = TextConstant.EMPTY, pageIndex = NumberConstant.PAGE_INDEX_DEFAULT, pageSize = NumberConstant.PAGE_SIZE_DEFAULT) {
    this.arrIds = [];
    this.kendoLoading = true;
    this.checkboxAction = 0;
    await this.userService.GetByPage((keyword || '').trim(), status, pageIndex, pageSize)
      .subscribe(res => {
        this.kendoLoading = false;
        res.data.map(item => {
          item.CreatedDate = new Date(item.CreatedDate)
          item.UpdatedDate = new Date(item.UpdatedDate)
          return item
        })
        this.gridData = res
      })
  }

  public pageChange({ skip, take }: PageChangeEvent): void {
    this.pageIndex = skip / take + 1;
    this.pageSize = take;
    this.skip = skip;
    this.getUsers(this.textFilter, this.statusFilter, this.pageIndex, this.pageSize)
  }

  public ngAfterViewInit() {
    this.resizeGrid();
  }

  private resizeGrid() {
    var grid = $("#grid")
    var offsetbottom = parseInt($(window).height()) - parseInt(grid.offset().top);
    var gridElement = grid,
      dataArea = gridElement.find(".k-grid-content"),
      heightHeader = $(".k-grid-header", grid).outerHeight() || 0,
      heightToolbar = $(".k-grid-toolbar", grid).outerHeight() || 0,
      heightFooter = $(".k-pager-wrap", grid).outerHeight() || 0;
    dataArea.height(offsetbottom - heightHeader - heightToolbar - heightFooter - 25);
  }

  async filter() {
    await this.getUsers(this.textFilter, this.statusFilter, this.pageIndex, this.pageSize)
  }

  async cancelFilter() {
    this.textFilter = TextConstant.EMPTY
    await this.getUsers(this.textFilter, this.statusFilter, this.pageIndex, this.pageSize)
  }
  ///////////////////////////////Checkbox///////////////////////////////
  private checkedAllRow() {
    var length = this.gridData.data.length;
    var checked = 0;
    this.gridData.data.map(m => {
      if (m.checked) checked++;
    });
    if (checked == 0) {
      this.checkboxAction = 0;
    } else {
      this.checkboxAction = checked == length ? 1 : 2;
    }
  }
  checkAllRow(e) {
    var x = e.checked;
    this.checkboxAction = x ? 1 : 0;
    this.gridData.data.map(m => {
      m.checked = x;
    });
  }

  checkRow(e, Id) {
    var x = e.checked;
    var dataItem = this.gridData.data.find(x => x.Id === Id);
    dataItem.checked = x;
    this.checkedAllRow();

    if (e.checked) {
      this.arrIds.push(Id);
    } else if (!e.checked) {
      this.arrIds.splice(this.arrIds.indexOf(Id), 1);
    }
  }

  ///////////////////////////////Thêm/Sửa/Xóa///////////////////////////////
  async onCreate(e) {
    this.dialogService.open(UserDetailComponent, {
      header: 'Thông tin người dùng',
      width: 'calc(100% - 30px)',
      style: {
        'margin': '0 auto',
        'max-width': '1200px',
        'max-height': '90vh',
      },
      autoZIndex: false,
    })
  }
  async editRecord(Id) {
    var dataItem = this.gridData.data.find(x => x.Id === Id);
    this.dialogService.open(UserDetailComponent, {
      data: dataItem,
      header: 'Thông tin người dùng',
      width: 'calc(100% - 30px)',
      style: {
        'margin': '0 auto',
        'max-width': '1200px',
        'max-height': '90vh',
      },
      autoZIndex: false,
    })
  }

  async deleteMany(e) {
    return this.userService.deleteMany(this.arrIds)
      .subscribe(async res => {
        if (res.IsSuccess === true) {
          this.toastrService.success('Xoá dữ liệu thành công!', 'Thông báo')
          await this.getUsers()
        } else {
          this.toastrService.error('Xóa dữ liệu thất bại!', 'Thông báo')
        }
      })
  }

  async getDataAfterCreated(e) {
    console.log('aftercreate', e)
    if (e.IsSuccess === true) {
      this.getUsers();
    }
  }
  
}