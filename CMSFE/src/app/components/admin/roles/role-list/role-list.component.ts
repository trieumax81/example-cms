import { Component, OnInit, AfterViewInit } from "@angular/core";
import { ToastrService } from 'ngx-toastr';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { RoleDetailComponent } from '../role-detail/role-detail.component';
import { DialogService } from 'primeng/api';
import { RoleService } from '../../../../_services'
import { TextConstant, NumberConstant } from '../../../../_constants';
import { GridDataResult, PageChangeEvent, DataStateChangeEvent } from '@progress/kendo-angular-grid';
import { State } from '@progress/kendo-data-query';
declare var $: any
@Component({
  selector: "app-roles",
  templateUrl: "./role-list.component.html",
  styleUrls: [
    "../../../../../assets/custom/css/style.css",
    "./role-list.component.css"
  ]
})
export class RoleListComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI
  arrIds: string[] = []
  searchText: string
  statusDefined
  gridData: GridDataResult
  textFilter: string
  statusFilter: string = ''

  public buttonCount = 5;
  public info = true;
  public type: 'numeric' | 'input' = 'numeric';
  public pageSize = NumberConstant.PAGE_SIZE_DEFAULT;
  public pageIndex = NumberConstant.PAGE_INDEX_DEFAULT;
  public skip = NumberConstant.PAGE_SKIP_DEFAULT;
  public kendoLoading = false;
  public checkboxAction = 0;

  constructor(private dialogService: DialogService, private roleService: RoleService, private toastrService: ToastrService) {
    this.statusDefined = [{
      label: 'Hoạt động', value: 'Active'
    }, {
      label: 'Ngưng hoạt động', value: 'Inactive'
    }]
  }

  ngOnInit() {
    this.getRoles()
  }
  ///////////////////////////////Lưới///////////////////////////////
  private async getRoles(keyword = this.textFilter, status = this.statusFilter, pageIndex = this.pageIndex, pageSize = this.pageSize) {
    this.arrIds = [];
    this.kendoLoading = true;
    this.checkboxAction = 0;
    await this.roleService.GetByPage((keyword || '').trim(), status, pageIndex, pageSize)
      .subscribe(res => {
        this.kendoLoading = false;
        res.data.map(item => {
          item.CreatedDate = new Date(item.CreatedDate)
          item.UpdatedDate = new Date(item.UpdatedDate)
          return item
        })
        this.gridData = res
      })
  }

  public pageChange({ skip, take }: PageChangeEvent): void {
    this.pageIndex = skip / take + 1;
    this.pageSize = take;
    this.skip = skip;
    this.getRoles(this.textFilter, this.statusFilter, this.pageIndex, this.pageSize)
  }

  public ngAfterViewInit() {
    this.resizeGrid();
  }

  private resizeGrid() {
    var grid = $("#grid")
    var offsetbottom = parseInt($(window).height()) - parseInt(grid.offset().top);
    var gridElement = grid,
      dataArea = gridElement.find(".k-grid-content"),
      heightHeader = $(".k-grid-header", grid).outerHeight() || 0,
      heightToolbar = $(".k-grid-toolbar", grid).outerHeight() || 0,
      heightFooter = $(".k-pager-wrap", grid).outerHeight() || 0;
    dataArea.height(offsetbottom - heightHeader - heightToolbar - heightFooter - 25);
  }

  async filter() {
    await this.getRoles(this.textFilter, this.statusFilter, this.pageIndex, this.pageSize)
  }

  async cancelFilter() {
    this.textFilter = TextConstant.EMPTY
    await this.getRoles(this.textFilter, this.statusFilter, this.pageIndex, this.pageSize)
  }
  ///////////////////////////////Checkbox///////////////////////////////
  private checkedAllRow() {
    var length = this.gridData.data.length;
    var checked = 0;
    this.gridData.data.map(m => {
      if (m.checked) checked++;
    });
    if (checked == 0) {
      this.checkboxAction = 0;
    } else {
      this.checkboxAction = checked == length ? 1 : 2;
    }
  }
  checkAllRow(e) {
    var x = e.checked;
    this.checkboxAction = x ? 1 : 0;
    this.gridData.data.map(m => {
      m.checked = x;
    });
  }

  checkRow(e, Id) {
    var x = e.checked;
    var dataItem = this.gridData.data.find(x => x.Id === Id);
    dataItem.checked = x;
    this.checkedAllRow();

    if (e.checked) {
      this.arrIds.push(Id);
    } else if (!e.checked) {
      this.arrIds.splice(this.arrIds.indexOf(Id), 1);
    }
  }

  ///////////////////////////////Thêm/Sửa/Xóa///////////////////////////////
  async onCreate(e) {
    this.dialogService.open(RoleDetailComponent, {
      header: 'Thêm quyền',
      autoZIndex: false,
    })
  }
  async editRecord(Id) {
    var dataItem = this.gridData.data.find(x => x.Id === Id);
    this.dialogService.open(RoleDetailComponent, {
      data: dataItem,
      header: 'Chỉnh sửa quyền',
      autoZIndex: false,
    })
  }

  async deleteMany(e) {
    return this.roleService.deleteMany(this.arrIds)
      .subscribe(async res => {
        if (res.IsSuccess === true) {
          this.toastrService.success('Xoá dữ liệu thành công!', 'Thông báo')
          await this.getRoles()
        } else {
          this.toastrService.error('Xóa dữ liệu thất bại!', 'Thông báo')
        }
      })
  }

  async getDataAfterCreated(e) {
    console.log('aftercreate', e)
    if (e.IsSuccess === true) {
      this.getRoles();
    }
  }
}