import { Component, OnInit } from '@angular/core';
import { Role, RolePermission } from '../../../../_entities';
import { RoleService, CommonService } from '..//../../../_services';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/api';
import { ToastrService } from 'ngx-toastr';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { TextConstant, NumberConstant } from '../../../../_constants';
import { GridDataResult, PageChangeEvent, DataStateChangeEvent } from '@progress/kendo-angular-grid';
import { first } from 'rxjs/operators';

declare var $: any

@Component({
  selector: 'app-role-detail',
  templateUrl: './role-detail.component.html',
  styleUrls: ['./role-detail.component.css']
})

export class RoleDetailComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI
  role: Role
  gridDataUser: GridDataResult
  gridDataMenu: GridDataResult
  statusDefined
  dataMenus: any[]
  dataUsers: any[]

  public textFilter = TextConstant.EMPTY;
  public textFilter2 = TextConstant.EMPTY;
  public pageSize = NumberConstant.PAGE_SIZE_DEFAULT;
  public pageIndex = NumberConstant.PAGE_INDEX_DEFAULT;
  public skip = NumberConstant.PAGE_SKIP_DEFAULT;
  public kendoLoading = false;
  public checkboxAction: boolean = false;
  public CanReadAll: boolean = false;
  public CanCreateAll: boolean = false;
  public CanUpdateAll: boolean = false;
  public CanDeleteAll: boolean = false;
  public CanImportAll: boolean = false;
  public CanExportAll: boolean = false;

  constructor(private roleService: RoleService
    , private commonService: CommonService
    , public ref: DynamicDialogRef
    , public config: DynamicDialogConfig
    , private toastrService: ToastrService) {
    this.statusDefined = [{
      label: 'Hoạt động', value: 'Active'
    }, {
      label: 'Ngưng hoạt động', value: 'Inactive'
    }]
    this.role = new Role()
  }

  ngOnInit() {
    // get data from users component (data: id)
    if (this.config.data) {
      this.role = this.config.data;
    }
    this.getMenus(this.role.Id);
    this.getUsers(this.role.Id);
  }

  public ngAfterViewInit() {
    //prevent on filter
    $('form .filter').on('keypress keydown keyup', function (event) {
      if (event.keyCode == 13) {
        $(this).trigger("change");
        event.preventDefault();
        return false;
      }
    })
  }
  async getUsers(RoleId) {
    return new Promise((resolve, reject) => {
      this.roleService.getUsers(RoleId)
        .subscribe(res => {
          res.data.map(m => {
            m.checked = m.RoleId ? true : false;
          });
          this.dataUsers = res.data;
          this.loadGridDataUser();
          this.checkedAllRow();
          resolve();
        })
    });
  }

  async getMenus(RoleId) {
    return new Promise((resolve, reject) => {
      this.roleService.getMenus(RoleId)
        .subscribe(res => {
          this.gridDataMenu = res
          this.dataMenus = res.data;
          this.checkedAllRow2(true);
          resolve();
        }) 
    });
  }

  public pageChange({ skip, take }: PageChangeEvent): void {
    this.pageIndex = skip / take + 1;
    this.pageSize = take;
    this.skip = skip;
    this.loadGridDataUser();
  }
  public loadGridDataUser(){
    this.gridDataUser = {
      data: this.dataUsers.slice(this.skip, this.skip + this.pageSize),
      total: this.dataUsers.length
    };
  }


  //////////////////////////////CHECKBOX ACTION///////////////////////
  private checkedAllRow() {
    var length = this.gridDataUser.data.length;
    var checked = 0;
    this.gridDataUser.data.map(m => {
      if (m.checked) checked++;
    });
    if (checked == 0) {
      this.checkboxAction = false;
    } else {
      this.checkboxAction = checked == length ? false : null;
    }
    console.log(this.checkboxAction, 'checkboxaction')
  }
  checkAllRow(e) {
    let x = e.checked;
    this.checkboxAction = x ? true : false;
    this.gridDataUser.data.map(m => {
      m.checked = x;
      m.IsDelete = x;
      m.dirty = true;
    });
  }

  checkRow(e, UserId) {
    let x = e.checked;
    let dataItem = this.gridDataUser.data.find(x => x.UserId === UserId);
    dataItem.checked = x;
    dataItem.IsDelete = x;
    dataItem.dirty = true;
    this.checkedAllRow();
  }

  private checkedAllRow2(All, Action = null) {
    let length = this.gridDataMenu.data.length;
    if (All) {
      let CanReadAll = 0;
      let CanCreateAll = 0;
      let CanUpdateAll = 0;
      let CanDeleteAll = 0;
      let CanImportAll = 0;
      let CanExportAll = 0;
      this.gridDataMenu.data.map(m => {
        if (m.CanRead) CanReadAll++;
        if (m.CanCreate) CanCreateAll++;
        if (m.CanUpdate) CanUpdateAll++;
        if (m.CanDelete) CanDeleteAll++;
        if (m.CanImport) CanImportAll++;
        if (m.CanExport) CanExportAll++;
      });
      if (CanReadAll == 0) {
        this.CanReadAll = false;
      } else {
        this.CanReadAll = CanReadAll == length ? true : null;
      }
      if (CanCreateAll == 0) {
        this.CanCreateAll = false;
      } else {
        this.CanCreateAll = CanCreateAll == length ? true : null;
      }
      if (CanUpdateAll == 0) {
        this.CanUpdateAll = false;
      } else {
        this.CanUpdateAll = CanUpdateAll == length ? true : null;
      }
      if (CanDeleteAll == 0) {
        this.CanDeleteAll = false;
      } else {
        this.CanDeleteAll = CanDeleteAll == length ? true : null;
      }
      if (CanImportAll == 0) {
        this.CanImportAll = false;
      } else {
        this.CanImportAll = CanImportAll == length ? true : null;
      }
      if (CanExportAll == 0) {
        this.CanExportAll = false;
      } else {
        this.CanExportAll = CanExportAll == length ? true : null;
      }
    } else {
      let checked = 0;
      this.gridDataMenu.data.map(m => {
        if (m[Action]) checked++;
      });
      if (checked == 0) {
        this[Action] = false;
      } else {
        this[Action] = checked == length ? true : null;
      }
    }
  }
  checkAllRow2(Action, e) {
    var x = e.checked;
    this.gridDataMenu.data.map(m => {
      m.dirty = true;
      m[Action] = x;
    });
  }

  checkRow2(e, Action, MenuId) {
    var x = e.checked;
    var dataItem = this.gridDataMenu.data.find(x => x.MenuId === MenuId);
    dataItem.dirty = true;
    dataItem[Action] = x;
    this.checkedAllRow2(false, Action);
  }

  /////////////////////////////////FILTER ////////////////////////////
  timeout: any
  async filter1(clear = false) {
    let self = this;
    if (clear) {
      clearTimeout(this.timeout);
      setTimeout(function () {
        self.gridDataUser.data = self.dataUsers.filter(s => s.Name.includes(self.textFilter));
      }, 500)
    } else {
      self.gridDataUser.data = self.dataUsers.filter(s => s.Name.includes(self.textFilter));
    }
  }
  filter2() {
    let self = this;
    clearTimeout(this.timeout);
    setTimeout(function () {
      self.gridDataMenu.data = self.dataMenus.filter(s => s.Name.includes(self.textFilter2));
    }, 500)
  }

  //////////////////////////////////////SUBMIT HANDLE///////////////////////////
  async onSubmit() {
    this.role.RolePermissions = [];
    this.gridDataMenu.data.map(item => {
      if (item.dirty) this.role.RolePermissions.push(item);
    });
    this.role.RoleUsers = [];
    this.gridDataUser.data.map(item => {
      if (item.dirty) this.role.RoleUsers.push(item);
    });
    if (!this.role.Id) {
      await this.create(this.role)
    } else {
      await this.update(this.role)
    }
  }
  //thêm role
  async create(data) {
    this.blockUI.start(TextConstant.LOADING)
    await this.roleService.create(data)
      .pipe(first())
      .subscribe(res => {
        if (res.body.IsSuccess === false) {
          this.toastrService.error('Thêm dữ liệu không thành công!', 'Thông báo')
        } else {
          this.toastrService.success('Thêm dữ liệu thành công!', 'Thông báo')
          this.ref.close(res.body)
        }
        this.blockUI.stop()
      }, err => {
        this.toastrService.error('Thêm dữ liệu không thành công!', 'Thông báo')
        this.blockUI.stop()
      })
  }
  //cập nhật role
  async update(data) {
    this.blockUI.start(TextConstant.LOADING)
    await this.roleService.update(data)
      .pipe(first())
      .subscribe(res => {
        console.log(res)
        if (res.body.IsSuccess === false) {
          this.toastrService.error('Sửa dữ liệu không thành công!', 'Thông báo')
        } else {
          this.toastrService.success('Sửa dữ liệu thành công!', 'Thông báo')
          this.ref.close(res.body)
        }
        this.blockUI.stop()
      }, err => {
        this.toastrService.error('Sửa dữ liệu không thành công!', 'Thông báo')
        this.blockUI.stop()
      })
  }

  onClosePopup() {
    this.ref.close()
  }
}
