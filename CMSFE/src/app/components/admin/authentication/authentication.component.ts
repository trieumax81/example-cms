import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/_entities';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { AuthService } from 'src/app/_services';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { JwtTokenConstant, TextConstant } from 'src/app/_constants';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: [
    '../../../../assets/custom/css/Login-Form-Dark.css',
    './authentication.component.css']
})
export class AuthenticationComponent implements OnInit {

  @BlockUI() blockUI: NgBlockUI
  user: User
  jwtHelperService: JwtHelperService
  isLoginSuccess: boolean

  constructor(private authService: AuthService, private router: Router) {
    this.onLogout();
    this.user = new User()
    this.jwtHelperService = new JwtHelperService()
   }

  async ngOnInit() {
  }

  async onLogin() {
    this.blockUI.start(TextConstant.LOADING)
    await this.authService.login(this.user)
      .pipe(first())
      .subscribe(res => {
        this.blockUI.stop()
        this.isLoginSuccess = true
        const jwtToken = res.accessToken
        if (jwtToken) {
          localStorage.setItem(JwtTokenConstant.X_ACCESS_TOKEN, jwtToken)
          this.router.navigate(['admin'])
        }
      }, err => {
        this.isLoginSuccess = false
        this.blockUI.stop()
      })
    // this.router.navigate(['dashboard'])
  }
 
  async onLogout() {
    this.authService.Logout();
    this.router.navigate(['admin/login'])
  }
}
