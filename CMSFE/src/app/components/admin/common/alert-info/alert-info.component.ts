import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-alert-info',
  templateUrl: './alert-info.component.html',
  styleUrls: ['./alert-info.component.css']
})
export class AlertInfoComponent implements OnInit {

  @Input() statusCode
  @Input() normalMessage
  @Input() strongMessage
  constructor() { }

  ngOnInit() {
  }

}
