import { Component, OnInit } from "@angular/core";
import { Router, NavigationEnd } from "@angular/router";
import { MenuService } from '../../../../_services/systems/menu.service'
import { MenuPermission } from '../../../../_entities/menu-permission'
import { AuthService } from '../../../../_services/auth.service'
import { JwtTokenConstant } from 'src/app/_constants';

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss", "./header.component.css"]
})
export class HeaderComponent implements OnInit {
  public pushRightClass: string;
  public menus: MenuPermission[]
  constructor(public router: Router, private menuService: MenuService, private authService: AuthService) {
    this.checkToken();
    this.router.events.subscribe(val => {
      if (
        val instanceof NavigationEnd &&
        window.innerWidth <= 992 &&
        this.isToggled()
      ) {
        this.toggleSidebar();
      }
    });
  }

  checkToken() {
    if (!localStorage.getItem(JwtTokenConstant.X_ACCESS_TOKEN)) {
      this.onLogout()
    }
  }
  async ngOnInit() {
    this.pushRightClass = "push-right";
    await this.getMenus();
  }

  async getMenus() {
    return new Promise((resolve, reject) => {
      this.menuService.getMenusByUser()
        .subscribe(res => {
          if (res.IsSuccess === true) {
            let menuTemp: MenuPermission[];
            menuTemp = res.Data.filter(s => s.ParentId == 0);
            this.menus = menuTemp; //menu cap 1
            this.menus.map(item => {
              this.recursiveMenu(res.Data, item);
            });
            resolve()
          } else {
            reject()
          }
        })
    })
  }
  recursiveMenu(menus, item) {
    let menuTemp: MenuPermission[];
    menuTemp = menus.filter(s => s.ParentId == item.Id);
    menuTemp.map(item => {
      this.recursiveMenu(menus, item);
    });
    item.Childs = menuTemp;
  }

  isToggled(): boolean {
    const dom: Element = document.querySelector("body");
    return dom.classList.contains(this.pushRightClass);
  }

  toggleSidebar() {
    const dom: any = document.querySelector("body");
    dom.classList.toggle(this.pushRightClass);
  }

  onLogout() {
    this.authService.Logout();
    this.router.navigate(['admin/login'])
  }
}
