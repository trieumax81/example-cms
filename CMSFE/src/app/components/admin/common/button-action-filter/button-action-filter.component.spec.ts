import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonActionFilterComponent } from './button-action-filter.component';

describe('ButtonActionFilterComponent', () => {
  let component: ButtonActionFilterComponent;
  let fixture: ComponentFixture<ButtonActionFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ButtonActionFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonActionFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
