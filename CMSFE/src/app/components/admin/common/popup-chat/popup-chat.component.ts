import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery'

@Component({
  selector: 'app-popup-chat',
  templateUrl: './popup-chat.component.html',
  styleUrls: ['./popup-chat.component.css']
})
export class PopupChatComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  openChatBox() {
    $('#myForm').css('display','block')
  }

  closeChatBox() {
    $('#myForm').css('display','none')
  }

}
