import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatDialogService } from '../../../../_services/common';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { TextConstant } from '../../../../_constants';
import { DialogService } from 'primeng/api';
import { ParameterDetailComponent } from '../../parameter';
import { Router } from '@angular/router';
import { isNullOrUndefined } from 'util';
import { MenuDetailComponent } from '../../systems';


@Component({
  selector: 'app-table-toolbar',
  templateUrl: './table-toolbar.component.html',
  styleUrls: [
    './table-toolbar.component.css']
})

export class TableToolbarComponent implements OnInit {

  @BlockUI() blockUI: NgBlockUI
  selectedExcel
  @Input() locationCreatedPage
  @Input() arrayIds
  @Input() parameterInput
  @Input() departmentInput
  @Input() positionInput
  @Input() jobInput
  @Input() unitInput
  @Input() materialGroupInput
  @Input() materialTypeInput
  @Input() rolePermissionInput
  @Input() menuInput
  @Input() userInput
  @Input() productSampleInput
  @Output() outputAfterCreated
  @Output() outputArrayIdToDelete
  @Output() outputCreate
  @Input() handPrintInput
  @Input() colorPrintInput
  @Input() TypeRunPrintInput
  // config display button actions
  @Input() showButtonAdd = true
  @Input() showButtonSave = true
  @Input() showButtonUpload = true
  @Input() showButtonDowload = true
  @Input() showButtonCancel = true
  @Input() showButtonDelete = true

  constructor(private dialogService: MatDialogService, private router: Router, private primeDiglogService: DialogService) {
    this.outputAfterCreated = new EventEmitter<any>()
    this.outputArrayIdToDelete = new EventEmitter<any>()
    this.outputCreate = new EventEmitter<any>()
  }

  ngOnInit() {
    console.log(this.arrayIds)
  }

  onShowCreatedRecord() {
    if (!this.locationCreatedPage) {
      if (this.parameterInput) {
        const ref = this.primeDiglogService.open(ParameterDetailComponent, {
          header: 'Thông tin quyền người dùng',
          width: '50%',
          autoZIndex: false
        })

        ref.onClose.subscribe(res => {
          isNullOrUndefined(res) ? undefined : this.outputAfterCreated.emit(res)
        })
      }

      


     
     
      // if (this.menuInput) {
      //   this.outputCreate.emit(); 
      // }

      // if (this.userInput) {
      //   this.outputCreate.emit();
      // }

      this.outputCreate.emit(); 
    } else {
      this.router.navigate([`/${this.locationCreatedPage}`])
    }
  }

  onCreateData() {
    window.location.href = this.locationCreatedPage
  }

  async onChangeExcel(e) {
    this.selectedExcel = e.target.files[0];
    await this.onImportExcel();
  }

  async onImportExcel() {
    // console.log(this.selectedExcel)
    // await this.excelIoService.importExcelFile(this.selectedExcel)
    const formData = new FormData();
    formData.append('excel', this.selectedExcel, this.selectedExcel.name);
    console.log('excel' + formData.get('excel'));
    // this.userService.importExcel(formData)
    //   .subscribe(res => {
    //     console.log(res);
    //   }, err => {
    //     console.log(err);
    //   });
  }

  onDeleteRecords() {
    console.log(this.arrayIds)
    const dialogData = {
      title: 'Xóa dữ liệu',
      message: 'Bạn có chắc chắn muốn xóa dữ liệu không?'
    }
    this.dialogService.openConfirmDialog(dialogData)
      .afterClosed()
      .subscribe(res => {
        if (res) {
          this.outputArrayIdToDelete.emit(this.arrayIds)
        }
      })
  }
}
