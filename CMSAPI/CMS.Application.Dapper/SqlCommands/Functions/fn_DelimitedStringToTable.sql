USE [CXCMDev]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_DelimitedStringToTable]    Script Date: 9/4/2019 11:49:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[fn_DelimitedStringToTable]
( 
	@String nvarchar(max),
	@Delimiter nchar(1)
)
RETURNS @returnTable TABLE(id int, val NVARCHAR(100)) 
AS
BEGIN
	IF @String IS NULL RETURN;
    
	DECLARE @currentStartIndex INT = 1
		,@currentEndIndex INT
		,@length INT = LEN(@String)
		,@idx INT = 0

    SET @currentEndIndex=CHARINDEX(@Delimiter,@String,@currentStartIndex);
    WHILE (@currentEndIndex<>0)
    BEGIN 
        INSERT INTO @returnTable VALUES (@idx,LTRIM(SUBSTRING(@String, @currentStartIndex, @currentEndIndex-@currentStartIndex)))
        SET @currentStartIndex=@currentEndIndex+1;
        SET @currentEndIndex=CHARINDEX(@Delimiter,@String,@currentStartIndex);
		SET @idx = @idx + 1;
    END

	IF (@currentStartIndex <= @length) INSERT INTO @returnTable  VALUES (@idx,LTRIM(SUBSTRING(@String, @currentStartIndex, @length-@currentStartIndex+1)));
   
   RETURN;
END 