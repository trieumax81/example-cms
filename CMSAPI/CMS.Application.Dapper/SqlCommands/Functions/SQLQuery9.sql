USE [NganHaDev]
GO
/****** Object:  StoredProcedure [dbo].[p_Products_Select_By_Page]    Script Date: 10/4/2019 5:22:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
	p_Products_Select_By_Page 
		@UserId = '778A9FAD-C3B8-4A68-9865-7EE0862654EF'
		--@UserId = 'FA05082F-8E51-4726-B730-7C540CE280FF'
		 select * from Products
*/
ALTER PROCEDURE [dbo].[p_Products_Select_By_Page]
	 @PageIndex int = 1
	,@PageSize int = 100000  
	,@keyword nvarchar(max) = ''
	,@IsSample VARCHAR(50) = 1
	,@Sort nvarchar(max) = ''
	,@Status nvarchar(max) = 'Active'
    ,@UserId uniqueidentifier = '615C521B-2F73-4221-B1AA-8A82D6A73508'
AS
BEGIN
	IF OBJECT_ID('tempdb..#Result') IS NOT NULL
		DROP TABLE #Result
	
	SELECT @keyword = '' WHERE @keyword IS NULL

	SELECT 
	   p.[Id]
      ,p.[IsSample]
      ,p.[Code]
      ,p.[Name]
      ,p.[Version]
      ,p.[UnitId]
      ,p.[MinimumInventory]
      ,p.[TypeProduct]
      ,p.[MOQ]
      ,p.[SalePrice]
      ,p.[IsDelete]
	  ,p.Status
	  ,p.CustomerId
	  ,p.NumberProduct
	  ,p.Specificationss
	  ,p.PlanProductionTime
      
	  ,un.[Name] UnitName
	  ,a.Name as AreaName
	  ,pa.Value AS TypeProductName
	  ,pa2.Value AS StatusProductName
	  ,cu.Code AS CustomerCode
	  ,cu.Name AS CustomerName

	  --thông tin thành phẩm
      ,pf.[Index]
	  ,pf.Value as PFValue
	  ,pf.PaperType PFPaperType
	  ,cp.Name as ColorName
	  ,pf.PageNum
	  ,pf.UrlImage
	  ,pf.Orther
	  --thông tin in
	  ,pp.HandNum
	  ,hp.Name as HandName
	  ,pp.SizePaper
	  ,pp.PrintNum
	  ,tr.Name as TypeRunName


	into #Result
	from Products p
	LEFT JOIN ProductFinisheds pf on pf.ProductId = p.Id
	LEFT JOIN ProductPrints pp on pp.ProductId = p.Id and pp.[Index] = pf.[Index]	
	LEFT JOIN ColorPrints cp on cp.Id = pf.ColorId
	LEFT JOIN HandPrints hp on hp.Id = pp.HandId
	LEFT JOIN TypeRuns tr on tr.Id = pp.TypeRunId
	LEFT JOIN Units un on un.Id = p.UnitId
	LEFT JOIN Areas a on a.Id = p.AreaId
	LEFT JOIN dbo.Parameters pa ON pa.ParameterCode = p.TypeProduct AND pa.ParameterType = 'TYPEPRODUCT'
	LEFT JOIN dbo.Parameters pa2 ON pa2.ParameterCode = p.Status AND pa2.ParameterType = 'STATUSPRODUCT'
	LEFT JOIN dbo.Customers cu ON cu.Id = p.CustomerId

	WHERE (p.Name like CONCAT('%', @keyword, '%')
		or p.Code = @keyword) 
		AND p.IsSample=@IsSample
		--and p.Status = @Status


	
	DECLARE @cols1 NVARCHAR(MAX)
		, @cols2 NVARCHAR(MAX)
		, @cols3 NVARCHAR(MAX)
		, @cols1_ NVARCHAR(MAX)
		, @cols2_ NVARCHAR(MAX)
		, @cols3_ NVARCHAR(MAX)
		, @query NVARCHAR(MAX);
	--column get
	SET @cols1 = STUFF(
					 (
						 SELECT DISTINCT
								','+QUOTENAME(c.Code)
						 FROM ProductionSteps c FOR XML PATH(''), TYPE
					 ).value('.', 'nvarchar(max)'), 1, 1, '');
	SET @cols2 = STUFF(
					 (
						 SELECT DISTINCT
								','+QUOTENAME(c.Code)
						 FROM MachiningSteps c FOR XML PATH(''), TYPE
					 ).value('.', 'nvarchar(max)'), 1, 1, '');
	SET @cols3 = STUFF(
					 (
						 SELECT DISTINCT
								','+QUOTENAME(c.Code)
						 FROM Materials c FOR XML PATH(''), TYPE
					 ).value('.', 'nvarchar(max)'), 1, 1, '');
	--column select
	select @cols1 = '[null]' where @cols1 is null
	select @cols2 = '[null]' where @cols2 is null
	select @cols3 = '[null]' where @cols3 is null 

	select @cols1_ = concat('p.',REPLACE(@cols1, ',', ',p.')) 
	select @cols2_ = concat('m.',REPLACE(@cols2, ',', ',m.')) 
	select @cols3_ = concat('mt.',REPLACE(@cols3, ',', ',mt.'))
    
         
    DECLARE
		 @Sql NVARCHAR(MAX) 

	SET @Sql = 
    N'
	;with p as (
		select * 
		from (
			select m.Id
				,m.[Index]
				,mp.Code
				,pp.NumberOrder
			from ProductionSteps mp 
			left join #Result m on 1 = 1
			left join ProductProductions pp on pp.ProductId = m.Id and pp.ProductionStepId = mp.Id 
		) x pivot (max(NumberOrder) for Code in ('+@cols1+')) p
	), m as (
		select * 
		from (
			select m.Id
				,m.[Index]
				,mp.Code
				,pp.NumberOrder
			from MachiningSteps mp 
			left join #Result m on 1 = 1
			left join ProductMachinings pp on pp.ProductId = m.Id and pp.MachiningStepId = mp.Id 
		) x pivot (max(NumberOrder) for Code in ('+@cols2+')) p 
	), mt as (
		select * 
		from (
			select m.Id
				,m.[Index]
				,mp.Code
				,pp.Amount
			from Materials mp 
			left join #Result m on 1 = 1
			left join ProductMaterials pp on pp.ProductId = m.Id and pp.MaterialId = mp.Id 
		) x pivot (max(Amount) for Code in ('+@cols3+')) p
	), FullSet AS (
		select r.* 
			,'+@cols1_+'
			,'+@cols2_+'
			,'+@cols3_+'
		from #Result r
		left join p on p.Id  = r.Id and p.[Index] = r.[Index]
		left join m on m.Id = r.Id and m.[Index] = r.[Index]
		left join mt on mt.Id = r.Id and mt.[Index] = r.[Index]
    ),CountSet AS(
	   SELECT COUNT(*) AS TotalItems
	   FROM FullSet
    )
    SELECT b.TotalItems, a.*
    FROM FullSet a, CountSet b 
	ORDER BY Id
    OFFSET (({Page} - 1) * {PageSize}) ROWS
    FETCH NEXT '+ CAST(@PageSize AS varchar(10)) +' ROWS ONLY
    '

    SELECT @Sql = REPLACE(@Sql, '{Page}', @PageIndex)
    SELECT @Sql = REPLACE(@Sql, '{PageSize}', @PageSize)
   
    PRINT (@Sql)

    EXEC (@Sql)

END
