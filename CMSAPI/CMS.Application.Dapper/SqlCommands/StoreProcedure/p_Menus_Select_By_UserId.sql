﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO 

IF OBJECT_ID('dbo.p_Menus_select_By_UserId') IS NOT NULL
	DROP PROC dbo.p_Menus_select_By_UserId
GO

/*
	p_Menus_select_By_UserId 
		--@UserId = 'CAA91831-8650-4D68-B00C-A2E4874042B3w'
		@UserId = 'FA05082F-8E51-4726-B730-7C540CE280FF' 

		select * from users
		 
		 
*/
CREATE PROCEDURE [dbo].p_Menus_select_By_UserId
		@UserId uniqueidentifier = 'CAA91831-8650-4D68-B00C-A2E4874042B3'
	as
	BEGIN
		IF OBJECT_ID('tempdb..#Result') IS NOT NULL
			DROP TABLE #Result
	
		;with p as (
			select distinct MenuId
				,ur.UserId
				,CanRead
				,CanCreate
				,CanUpdate
				,CanDelete
				,CanImport
				,CanExport
			from UserRoles ur
			inner join RolePermissions rp on rp.RoleId = ur.RoleId 
			where ur.UserId = @UserId and rp.CanRead = 1
		),Menu as(
			select
				char(64+m.NumberOrder) as RowOrder
				,m.Id
				,m.ParentId
				,m.Link
				,m.Name
				,m.Code
				,m.Icon
				,m.IsVisible
				,p.CanRead
				,p.CanCreate
				,p.CanUpdate
				,p.CanDelete
				,p.CanImport
				,p.CanExport
			from Menus m 
			inner join p on p.MenuId = m.Id
			where m.Level = 0
		),Menu1 as(
			select
				concat(m2.RowOrder,char(64+m.NumberOrder)) as RowOrder
				,m.Id
				,m.ParentId
				,m.Link
				,m.Name
				,m.Code
				,m.Icon
				,m.IsVisible
				,p.CanRead
				,p.CanCreate
				,p.CanUpdate
				,p.CanDelete
				,p.CanImport
				,p.CanExport
			from Menus m 
			inner join p on p.MenuId = m.Id
			inner join Menu m2 on m2.Id = m.ParentId
			where m.Level = 1
		),Menu2 as(
			select
				concat(m2.RowOrder,char(64+m.NumberOrder)) as RowOrder
				,m.Id
				,m.ParentId
				,m.Link
				,m.Name
				,m.Code
				,m.Icon
				,m.IsVisible
				,p.CanRead
				,p.CanCreate
				,p.CanUpdate
				,p.CanDelete
				,p.CanImport
				,p.CanExport
			from Menus m 
			inner join p on p.MenuId = m.Id
			inner join Menu1 m2 on m2.Id = m.ParentId
			where m.Level = 2
		),Menu3 as(
			select
				concat(m2.RowOrder,char(64+m.NumberOrder)) as RowOrder
				,m.Id
				,m.ParentId
				,m.Link
				,m.Name
				,m.Code
				,m.Icon
				,m.IsVisible
				,p.CanRead
				,p.CanCreate
				,p.CanUpdate
				,p.CanDelete
				,p.CanImport
				,p.CanExport
			from Menus m 
			inner join p on p.MenuId = m.Id
			inner join Menu2 m2 on m2.Id = m.ParentId
			where m.Level = 3
		),dt as(
			select * from Menu  
			union   
			select * from Menu1  
			union 
			select * from Menu2  
			union 
			select * from Menu3  
		),r as (
			select *
			from dt
			where dt.CanRead = 1
				or nullif(dt.Link,'') is not null 
				or dt.Id in (select distinct ParentId from dt)
		) 
		select 
			 RowOrder
			,Id
			,ParentId
			,Link
			,Name
			,Icon
			,IsVisible
			,case when sum(cast(CanRead	  as int)) > 0 then 1 else 0 end CanRead	
			,case when sum(cast(CanCreate as int)) > 0 then 1 else 0 end CanCreate
			,case when sum(cast(CanUpdate as int)) > 0 then 1 else 0 end CanUpdate
			,case when sum(cast(CanDelete as int)) > 0 then 1 else 0 end CanDelete
			,case when sum(cast(CanImport as int)) > 0 then 1 else 0 end CanImport
			,case when sum(cast(CanExport as int)) > 0 then 1 else 0 end CanExport
		from r
		where Name is not null
		group by RowOrder
			,Id
			,ParentId
			,Link
			,Name
			,Icon
			,IsVisible
		order by IsVisible desc, RowOrder asc

		

END
