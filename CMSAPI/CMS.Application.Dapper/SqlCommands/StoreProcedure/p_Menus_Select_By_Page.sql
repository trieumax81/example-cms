﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO 

IF OBJECT_ID('dbo.p_Menus_select_By_Page') IS NOT NULL
	DROP PROC dbo.p_Menus_select_By_Page
GO
 
/*
	p_Menus_select_By_Page 
		@UserId = '778A9FAD-C3B8-4A68-9865-7EE0862654EF'
		--@UserId = 'FA05082F-8E51-4726-B730-7C540CE280FF'
		,@Type = 'Multiple'
		  
		 select * from menus where parentid = 43
		 delete menus where id = 53
*/
CREATE PROCEDURE [dbo].p_Menus_select_By_Page
	 @PageIndex int = 1
	,@PageSize int = 100000  
	,@keyword nvarchar(max) = ''
	,@Sort nvarchar(max) = ''
	,@Status nvarchar(max) = ''
    ,@UserId uniqueidentifier = '615C521B-2F73-4221-B1AA-8A82D6A73508'
as
BEGIN
	IF OBJECT_ID('tempdb..#Result') IS NOT NULL
		DROP TABLE #Result
	
	select @keyword = '' where @keyword IS NULL
	 
	 ;with Menu as(
		select
			cast(concat(char(64+m.NumberOrder),m.Id) as varchar(max)) as RowOrder
			,cast(ROW_NUMBER() over (order by m.NumberOrder) as varchar(max)) RowIndex
			,m.NumberOrder
			,m.[Id]
			,m.[ParentId]
			,m.Code
			,cast('' as varchar(max)) Space
			,m.Name
			,m.[Level]
			,m.[Icon]
			,m.[Color]
			,m.[BackgroundColor]
			,m.IsVisible
			,m.Link
			,m.[Status]
			,m.[CreatedBy]
			,m.[CreatedDate]
			,m.[UpdatedBy] 
			,m.[UpdatedDate] 
		from Menus m 
		where m.Level = 0 OR m.ParentId NOT IN (select Id from Menus)

		union all 

		select
			cast(concat(m2.RowOrder,concat(char(64+m.NumberOrder),m.Id)) as varchar(max)) as RowOrder
			,ISNULL(m2.RowIndex,'') + '.' + cast(ROW_NUMBER() over (order by m.NumberOrder) as varchar(max)) RowIndex
			,m.NumberOrder
			,m.[Id]
			,m.[ParentId]
			,m.Code
			,cast(concat(m2.Space,' &nbsp; &nbsp; &nbsp; ') as varchar(max)) Space
			,m.Name
			,m.[Level]
			,m.[Icon]
			,m.[Color]
			,m.[BackgroundColor]
			,m.IsVisible
			,m.Link
			,m.[Status]
			,m.[CreatedBy]
			,m.[CreatedDate]
			,m.[UpdatedBy] 
			,m.[UpdatedDate] 
		from Menus m 
		INNER JOIN Menu m2 on m2.Id = m.ParentId
	)
    select m.RowOrder
		,m.NumberOrder 
		,m.[Id]
		,m.[ParentId]
		,m.Code
		,concat(m.[Space],m.RowIndex,'. ') Space
		,m.[Name]
		,m.[Level]
		,m.[Icon]
		,m.[Color]
		,m.[BackgroundColor]
		,m.IsVisible
		,m.Link
		,m.[Status]
		,u.UserName as CreatedBy
		,m.[CreatedDate]
		,u.UserName as UpdatedBy
		,m.[UpdatedDate] 
	into #Result
    from Menu m
	left join Users u ON u.Id = m.CreatedBy
	where (m.Name like concat('%', @keyword, '%')
		or m.Code like concat('%', @keyword, '%'))
		and m.Status = @Status

	 


    DECLARE @Sql NVARchar(MAX) = 
    N'
    ;with FullSet as
    (
	   select	a.*
	   from	#Result a
    ) 
    ,CountSet as
    (
	   select COUNT(*) as TotalItems
	   from FullSet
    )
    select b.TotalItems, a.*
    from FullSet a, CountSet b 
	ORDER BY  RowOrder asC
    OFFSET (({Page} - 1) * {PageSize}) ROWS
    FETCH NEXT '+ CasT(@PageSize as varchar(10)) +' ROWS ONLY
    '

    select @Sql = REPLACE(@Sql, '{Page}', @PageIndex)
    select @Sql = REPLACE(@Sql, '{PageSize}', @PageSize)

    EXEC (@Sql)

END
