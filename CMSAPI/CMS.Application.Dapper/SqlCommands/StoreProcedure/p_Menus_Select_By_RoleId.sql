﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO 

IF OBJECT_ID('dbo.p_Menus_Select_By_RoleId') IS NOT NULL
	DROP PROC dbo.p_Menus_Select_By_RoleId
GO

/* 
	select * from roles		 
*/
CREATE PROCEDURE [dbo].p_Menus_Select_By_RoleId
	@RoleId uniqueidentifier = '08D6148A-359D-C447-3557-DACE3F3C53E2'
AS
BEGIN
	 
	;WITH Menu AS(
		SELECT
			CONCAT(Char(64+m.NumberOrder),m.Id) AS RowOrder
			,m.NumberOrder
			,m.[Id]
			,m.[ParentId]
			,'' Space
			,m.Name
			,m.[Icon] 
			,m.[Level]
		FROM Menus m 
		WHERE m.Level = 0 OR m.ParentId NOT IN (SELECT Id FROM Menus)
	),Menu1 AS(
		SELECT 
			CONCAT(m2.RowOrder,CONCAT(CHAR(64+m.NumberOrder),m.Id)) AS RowOrder
			,m.NumberOrder
			,m.[Id]
			,m.[ParentId]
			,' &nbsp; &nbsp; &nbsp; ' Space
			,m.Name
			,m.[Icon] 
			,m.[Level]
		FROM Menus m 
		INNER JOIN Menu m2 on m2.Id = m.ParentId
		WHERE m.Level = 1
	),Menu2 AS(
		SELECT
			CONCAT(m2.RowOrder,CONCAT(CHAR(64+m.NumberOrder),m.Id)) AS RowOrder
			,m.NumberOrder
			,m.[Id]
			,m.[ParentId]
			,' &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ' Space
			,m.Name
			,m.[Icon]
			,m.[Level]
		FROM Menus m 
		INNER JOIN Menu1 m2 on m2.Id = m.ParentId
		WHERE m.Level = 2
	),Menu3 AS(
		SELECT 
			CONCAT(m2.RowOrder,CONCAT(CHAR(64+m.NumberOrder),m.Id)) AS RowOrder
			,m.NumberOrder
			,m.[Id]
			,m.[ParentId]
			,' &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ' Space
			,m.Name
			,m.[Icon]
			,m.[Level]
		FROM Menus m 
		INNER JOIN Menu2 m2 on m2.Id = m.ParentId
		WHERE m.Level = 3
	),Data AS(
		SELECT * FROM Menu  
		UNION   
		SELECT * FROM Menu1  
		UNION 
		SELECT * FROM Menu2  
		UNION 
		SELECT * FROM Menu3  
	)
    SELECT m.RowOrder
		,m.NumberOrder
		,m.[Id] MenuId
		,m.[ParentId]
		,m.[Space]
		,m.[Name]
		,m.[Icon] 
		,m.[Level]
		,rp.RoleId
		,isnull(rp.CanRead	,0) CanRead	
		,isnull(rp.CanCreate,0) CanCreate
		,isnull(rp.CanUpdate,0) CanUpdate
		,isnull(rp.CanDelete,0) CanDelete
		,isnull(rp.CanImport,0) CanImport
		,isnull(rp.CanExport,0) CanExport
    FROM	Data m
	LEFT JOIN RolePermissions rp ON rp.MenuId = m.Id AND rp.RoleId = @RoleId
	 

END

