﻿SET ANSI_NULLS ON	
GO
SET QUOTED_IDENTIFIER ON
GO 

IF OBJECT_ID('dbo.p_ActivityLogs_Select_By_Page') IS NOT NULL
	DROP PROC dbo.p_ActivityLogs_Select_By_Page
GO

/*
	p_ActivityLogs_Select_By_Page 
		@UserId = '778A9FAD-C3B8-4A68-9865-7EE0862654EF'
		--@UserId = 'FA05082F-8E51-4726-B730-7C540CE280FF'
		,@Type = 'Multiple'
		 
		 
		 
*/
CREATE PROCEDURE [dbo].p_ActivityLogs_Select_By_Page
	 @PageIndex int = 1
	,@PageSize int = 100000  
	,@keyword nvarchar(max) = ''
	,@Sort nvarchar(max) = ''
	,@Status nvarchar(max) = ''
    ,@UserId uniqueidentifier = '615C521B-2F73-4221-B1AA-8A82D6A73508'
AS
BEGIN
	IF OBJECT_ID('tempdb..#Result') IS NOT NULL
		DROP TABLE #Result
	 
	select al.*
		,u.UserName
		,u.FullName
		,isnull(m.Name,al.MenuCode) MenuName
	into #Result
	from ActivityLogs al
	inner join Users u on u.Id = al.UserId
	left join Menus m on m.Code = al.MenuCode
	where (nullif(@keyword,'') is null or 
		(u.FullName like concat('%', @keyword, '%')
		or u.UserName like concat('%', @keyword, '%')
		or m.Name like concat('%', @keyword, '%')
		or al.Action like concat('%', @keyword, '%')))
	
    DECLARE @Sql NVARCHAR(MAX) = 
    N'
    ;WITH FullSet AS
    (
	   SELECT	a.*
	   FROM	#Result a
    ) 
    ,CountSet AS
    (
	   SELECT COUNT(*) AS TotalItems
	   FROM FullSet
    )
    SELECT b.TotalItems, a.*
    FROM FullSet a, CountSet b 
	ORDER BY RequestAt DESC, Id DESC
    OFFSET (({Page} - 1) * {PageSize}) ROWS
    FETCH NEXT '+ CAST(@PageSize AS varchar(10)) +' ROWS ONLY
    '

    SELECT @Sql = REPLACE(@Sql, '{Page}', @PageIndex)
    SELECT @Sql = REPLACE(@Sql, '{PageSize}', @PageSize)

    EXEC (@Sql)

END
