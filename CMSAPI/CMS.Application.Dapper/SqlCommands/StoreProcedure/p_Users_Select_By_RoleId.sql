﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO 

IF OBJECT_ID('dbo.p_Users_Select_By_RoleId') IS NOT NULL
	DROP PROC dbo.p_Users_Select_By_RoleId
GO

/*
	p_Users_Select_By_RoleId 
		@UserId = '778A9FAD-C3B8-4A68-9865-7EE0862654EF'
		--@UserId = 'FA05082F-8E51-4726-B730-7C540CE280FF'
		,@Type = 'Multiple'


select * from roles		  
select * from userroles	
 
  


select * from rolepermissions
		 
		 
*/
CREATE PROCEDURE [dbo].p_Users_Select_By_RoleId
	@Status nvarchar(max) = 'Active'
	,@RoleId uniqueidentifier = null
    ,@UserId uniqueidentifier = '615C521B-2F73-4221-B1AA-8A82D6A73508'
AS
BEGIN 
	
	select u.Id UserId
		,u.FullName 
		,ur.RoleId
		,cast(0 as bit) IsDelete 
	into #Result
	from Users u
	left join UserRoles ur on ur.RoleId = @RoleId and ur.USerId = u.Id
	where u.Status = @Status 

	select *
		,(select count(*) from #Result)  TotalItems
	from #Result

END
