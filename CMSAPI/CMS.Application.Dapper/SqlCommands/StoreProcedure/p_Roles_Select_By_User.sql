﻿SET ANSI_NULLS ON	
GO
SET QUOTED_IDENTIFIER ON
GO 

IF OBJECT_ID('dbo.p_Roles_Select_By_User') IS NOT NULL
	DROP PROC dbo.p_Roles_Select_By_User
GO

/* 
	p_Roles_Select_By_User '06ec5401-885e-4281-a54b-5917e452be1d'
*/
CREATE PROCEDURE [dbo].p_Roles_Select_By_User
    @UserId uniqueidentifier = '615C521B-2F73-4221-B1AA-8A82D6A73508'
AS
BEGIN
	IF OBJECT_ID('tempdb..#Result') IS NOT NULL
		DROP TABLE #Result
	
	 
	SELECT 
		 r.[Id] RoleId
		,r.[Name] 
		,ur.UserId
	into #Result
	from Roles r
	left join UserRoles ur on ur.RoleId = r.Id AND ur.UserId = @UserId
	 
	select *
		,(select count(*) from #Result) TotalItems
	from #Result
END
