﻿SET ANSI_NULLS ON	
GO
SET QUOTED_IDENTIFIER ON
GO 

IF OBJECT_ID('dbo.p_Roles_Select_By_MenuId') IS NOT NULL
	DROP PROC dbo.p_Roles_Select_By_MenuId
GO

/* 
	p_Roles_Select_By_User '06ec5401-885e-4281-a54b-5917e452be1d'
*/
CREATE PROCEDURE [dbo].p_Roles_Select_By_MenuId
    @MenuId int = 0
AS
BEGIN
	IF OBJECT_ID('tempdb..#Result') IS NOT NULL
		DROP TABLE #Result
	
	 
	SELECT 
		 r.[Id] RoleId
		,r.[Name] 
		,rp.MenuId
	into #Result
	from Roles r
	left join RolePermissions rp on rp.RoleId = r.Id and rp.MenuId = @MenuId
	 
	select *
		,(select count(*) from #Result) TotalItems
	from #Result
END
