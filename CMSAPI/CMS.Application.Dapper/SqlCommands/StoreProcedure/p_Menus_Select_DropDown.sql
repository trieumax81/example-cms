﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO 

IF OBJECT_ID('dbo.p_Menus_Parent_Select_DropDown') IS NOT NULL
	DROP PROC dbo.p_Menus_Parent_Select_DropDown
GO

/*
	p_Menus_Parent_Select_DropDown 
		@UserId = '778A9FAD-C3B8-4A68-9865-7EE0862654EF'
		--@UserId = 'FA05082F-8E51-4726-B730-7C540CE280FF'
		,@Type = 'Multiple'
		 
		 
		select * from Menus
*/
CREATE PROCEDURE [dbo].p_Menus_Parent_Select_DropDown
	@Id int = 0
    ,@UserId uniqueidentifier = '615C521B-2F73-4221-B1AA-8A82D6A73508'
AS
BEGIN
	IF OBJECT_ID('tempdb..#Result') IS NOT NULL
		DROP TABLE #Result
	
	
	;WITH Menu AS(
		SELECT m.Level
			,Char(64+m.NumberOrder) AS RowOrder
			,m.Id 
			,m.ParentId
			,'' Space
			,m.Name
			,m.Icon
			,m.NumberOrder
		FROM Menus m 
		WHERE m.Level = 0
	),Menu1 AS(
		SELECT m.Level
			,CONCAT(m2.RowOrder,CHAR(64+m.NumberOrder)) AS RowOrder
			,m.Id 
			,m.ParentId
			,' &nbsp; ' Space
			,m.Name
			,m.Icon
			,m.NumberOrder
		FROM Menus m 
		INNER JOIN Menu m2 on m2.Id = m.ParentId
		WHERE m.Level = 1
	),Menu2 AS(
		SELECT m.Level
			,CONCAT(m2.RowOrder,CHAR(64+m.NumberOrder)) AS RowOrder
			,m.Id 
			,m.ParentId
			,' &nbsp; &nbsp; ' Space
			,m.Name
			,m.Icon
			,m.NumberOrder
		FROM Menus m 
		INNER JOIN Menu1 m2 on m2.Id = m.ParentId
		WHERE m.Level = 2
	),Data AS(  
		SELECT * FROM Menu  
		UNION 
		SELECT * FROM Menu1  
		UNION 
		SELECT * FROM Menu2    
	)
	
    SELECT  
		RowOrder
		,Level
		,Id 
		,Space 
		,Name 
		,ParentId
		,Icon
		,NumberOrder

    FROM	Data a 
	WHERE Id <> @Id
	ORDER BY RowOrder

END
