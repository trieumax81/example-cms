SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
	p_Users_Select_By_Page 
		@UserId = '778A9FAD-C3B8-4A68-9865-7EE0862654EF'
		--@UserId = 'FA05082F-8E51-4726-B730-7C540CE280FF'
		,@Type = 'Multiple'
*/
CREATE PROCEDURE [dbo].[p_Icons_Select_By_page]
	 @PageIndex int = 1
	,@PageSize int = 1000
	,@keyword nvarchar(max) = ''
	,@Status nvarchar(max) = ''
	,@Sort nvarchar(max) = ''
    ,@UserId uniqueidentifier = '615C521B-2F73-4221-B1AA-8A82D6A73508'
AS
BEGIN
	IF OBJECT_ID('tempdb..#Result') IS NOT NULL
		DROP TABLE #Result
	
	SELECT @keyword = '' WHERE @keyword IS NULL
	SELECT @Status = 'Active' WHERE @Status IS NULL

	select  
		p.Name as label,
		p.Name as value
	into #Result
	from Icons p
	where p.Name like CONCAT('%', @keyword, '%')

    DECLARE @Sql NVARCHAR(MAX) = 
    N'
    ;WITH FullSet AS
    (
	   SELECT	a.*
	   FROM	#Result a
    ) 
    ,CountSet AS
    (
	   SELECT COUNT(*) AS TotalItems
	   FROM FullSet
    )
    SELECT b.TotalItems, a.*
    FROM FullSet a, CountSet b 
	ORDER BY 1
    OFFSET (({Page} - 1) * {PageSize}) ROWS
    FETCH NEXT '+ CAST(@PageSize AS varchar(10)) +' ROWS ONLY
    '

    SELECT @Sql = REPLACE(@Sql, '{Page}', @PageIndex)
    SELECT @Sql = REPLACE(@Sql, '{PageSize}', @PageSize)

    EXEC (@Sql)

END
