﻿SET ANSI_NULLS ON	
GO
SET QUOTED_IDENTIFIER ON
GO 

IF OBJECT_ID('dbo.p_Roles_Select_By_Page') IS NOT NULL
	DROP PROC dbo.p_Roles_Select_By_Page
GO

/*
	p_Roles_Select_By_Page 
		@UserId = '778A9FAD-C3B8-4A68-9865-7EE0862654EF'
		--@UserId = 'FA05082F-8E51-4726-B730-7C540CE280FF'
		,@Type = 'Multiple'
		 
		 
		 
*/
CREATE PROCEDURE [dbo].p_Roles_Select_By_Page
	 @PageIndex int = 1
	,@PageSize int = 100000  
	,@keyword nvarchar(max) = ''
	,@Sort nvarchar(max) = ''
	,@Status nvarchar(max) = ''
    ,@UserId uniqueidentifier = '615C521B-2F73-4221-B1AA-8A82D6A73508'
AS
BEGIN
	IF OBJECT_ID('tempdb..#Result') IS NOT NULL
		DROP TABLE #Result
	
	SELECT @keyword = '' WHERE @keyword IS NULL
	 
	SELECT 
		 r.[Id] 
		,r.[Name]
		,r.[NormalizedName]
		,r.[ConcurrencyStamp]
		,r.[Description]
		,r.[Status]
		,r.[CreatedDate]
		,r.[UpdatedDate]
		,u1.UserName CreatedBy
		,u2.UserName UpdatedBy
	into #Result
	from Roles r
	left join Users u1 on u1.id = r.CreatedBy
	left join Users u2 on u2.id = r.UpdatedBy
	where (nullif(@keyword,'') is null or r.Name like concat('%', @keyword, '%'))
		and (@Status = '' or r.Status = @Status)

    DECLARE @Sql NVARCHAR(MAX) = 
    N'
    ;WITH FullSet AS
    (
	   SELECT	a.*
	   FROM	#Result a
    ) 
    ,CountSet AS
    (
	   SELECT COUNT(*) AS TotalItems
	   FROM FullSet
    )
    SELECT b.TotalItems, a.*
    FROM FullSet a, CountSet b 
	ORDER BY CreatedDate DESC, Id DESC
    OFFSET (({Page} - 1) * {PageSize}) ROWS
    FETCH NEXT '+ CAST(@PageSize AS varchar(10)) +' ROWS ONLY
    '

    SELECT @Sql = REPLACE(@Sql, '{Page}', @PageIndex)
    SELECT @Sql = REPLACE(@Sql, '{PageSize}', @PageSize)

    EXEC (@Sql)

END
