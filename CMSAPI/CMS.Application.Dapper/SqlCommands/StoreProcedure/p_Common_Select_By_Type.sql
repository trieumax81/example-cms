﻿SET ANSI_NULLS ON	
GO
SET QUOTED_IDENTIFIER ON
GO 

IF OBJECT_ID('dbo.p_Commons_Select_By_Type') IS NOT NULL
	DROP PROC dbo.p_Commons_Select_By_Type
GO

/* 
	p_Commons_Select_By_Type 'Unit','Active','06ec5401-885e-4281-a54b-5917e452be1d'
*/
CREATE PROCEDURE [dbo].p_Commons_Select_By_Type
	@Type nvarchar(max) = ''
	,@Status nvarchar(max) = 'Active'
	,@IsParameter bit = 1
AS
BEGIN
	
	select ParameterCode 'value', Value 'label'
	from Parameters 
	where @IsParameter = 1 and ParameterType = @Type and Status = @Status
	
	union all

	select cast(id as nvarchar(20)) 'value', Name as 'label'
	from Units 
	where @IsParameter = 0 and @Type = 'Unit' and @Status = Status
	 
	union all 

	select cast(id as nvarchar(20)) 'value', Name as 'label'
	from ColorPrints
	where @IsParameter = 0 and @Type = 'ColorPrint' and @Status = Status

	union all 

	select cast(id as nvarchar(20)) 'value', Name as 'label'
	from HandPrints
	where @IsParameter = 0 and @Type = 'HandPrint' and @Status = Status
	 
	union all 

	select cast(id as nvarchar(20)) 'value', Name as 'label'
	from TypeRuns
	where @IsParameter = 0 and @Type = 'TypeRun' and @Status = Status

	union all 

	select cast(id as nvarchar(20)) 'value', Name as 'label'
	from Areas
	where @IsParameter = 0 and @Type = 'Area' and @Status = Status
	 

END
