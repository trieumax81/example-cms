﻿using Microsoft.Extensions.Configuration;
using CMS.Application.Dapper.Helpers;
using CMS.Application.Dapper.Interfaces;
using CMS.Utilities.SearchRequest;
using CMS.Utilities.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace CMS.Application.Dapper.Implementation
{
    public class UserDapper : IUserDapper
    {
        private readonly IConfiguration _configuration;
        public UserDapper(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public Result<DataTable> GetUsers(ParamSearch param, string UserId)
        {
            using (var dbConn = new SqlHelper(_configuration.GetConnectionString("DefaultConnection")))
            {
                try
                {
                    var p = new List<SqlParameter>();
                    p.Add(new SqlParameter("@PageIndex", param.PageIndex));
                    p.Add(new SqlParameter("@PageSize", param.PageSize));
                    p.Add(new SqlParameter("@Keyword", param.Keyword));
                    p.Add(new SqlParameter("@Sort", param.Sort));
                    p.Add(new SqlParameter("@Status", param.Status));
                    p.Add(new SqlParameter("@UserId", UserId));

                    var data = dbConn.ExecuteQuery("p_Users_Select_By_Page", p);
                    var total = (data.Rows.Count > 0 ? Convert.ToInt32(data.Rows[0]["TotalItems"]) : 0);
                    data.Columns.Remove("TotalItems");
                    return new Result<DataTable>(true, data, total);

                }
                catch (Exception e)
                {
                    return new Result<DataTable>(false, e.Message);
                }
            }
        }

        public Result<DataTable> GetRolesByUser(string UserId)
        {
            using (var dbConn = new SqlHelper(_configuration.GetConnectionString("DefaultConnection")))
            {
                try
                {
                    var p = new List<SqlParameter>();
                    p.Add(new SqlParameter("@UserId", UserId));

                    var data = dbConn.ExecuteQuery("p_Roles_Select_By_User", p);
                    var total = (data.Rows.Count > 0 ? Convert.ToInt32(data.Rows[0]["TotalItems"]) : 0);
                    data.Columns.Remove("TotalItems");
                    return new Result<DataTable>(true, data, total);

                }
                catch (Exception e)
                {
                    return new Result<DataTable>(false, e.Message);
                }
            }
        }

    }
}
