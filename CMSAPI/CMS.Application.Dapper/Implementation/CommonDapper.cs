﻿using Microsoft.Extensions.Configuration;
using CMS.Application.Dapper.Helpers;
using CMS.Application.Dapper.Interfaces;
using CMS.Utilities.Constants;
using CMS.Utilities.SearchRequest;
using CMS.Utilities.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace CMS.Application.Dapper.Implementation
{
    public class CommonDapper : ICommonDapper
    {
        private readonly IConfiguration _configuration;
        public CommonDapper(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public Result<DataTable> GetByType(string Type, bool IsParameter)
        {
            using (var dbConn = new SqlHelper(_configuration.GetConnectionString("DefaultConnection")))
            {
                try
                {
                    var p = new List<SqlParameter>();
                    p.Add(new SqlParameter("@Type", Type));
                    p.Add(new SqlParameter("@Status", CommonConstants.StatusActive));
                    p.Add(new SqlParameter("@IsParameter", IsParameter));

                    var data = dbConn.ExecuteQuery("p_Commons_Select_By_Type", p);
                    return new Result<DataTable>(true, data);

                }
                catch (Exception e)
                {
                    return new Result<DataTable>(false, e.Message);
                }
            }
        }

        public Result<DataTable> GetIcons(ParamSearch param, string userId) {
            using (var dbConn = new SqlHelper(_configuration.GetConnectionString("DefaultConnection"))) {
                try {
                    var p = new List<SqlParameter>();
                    p.Add(new SqlParameter("@PageIndex", param.PageIndex));
                    p.Add(new SqlParameter("@PageSize", param.PageSize));
                    p.Add(new SqlParameter("@Keyword", param.Keyword));
                    p.Add(new SqlParameter("@Status", param.Status));
                    p.Add(new SqlParameter("@Sort", param.Sort));
                    p.Add(new SqlParameter("@UserId", userId));

                    var data = dbConn.ExecuteQuery("p_Icons_Select_By_page", p);
                    var total = (data.Rows.Count > 0 ? Convert.ToInt32(data.Rows[0]["TotalItems"]) : 0);
                    data.Columns.Remove("TotalItems");
                    return new Result<DataTable>(true, data, total);

                } catch (Exception e) {
                    return new Result<DataTable>(false, e.Message);
                }
            }
        }
    }
}
