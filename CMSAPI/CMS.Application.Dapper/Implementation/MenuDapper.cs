﻿using Microsoft.Extensions.Configuration;
using CMS.Application.Dapper.Helpers;
using CMS.Application.Dapper.Interfaces;
using CMS.Utilities.SearchRequest;
using CMS.Utilities.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace CMS.Application.Dapper.Implementation
{
    public class MenuDapper : IMenuDapper
    {
        private readonly IConfiguration _configuration;
        public MenuDapper(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public Result<DataTable> GetMenusByUser(string UserId)
        {
            using (var dbConn = new SqlHelper(_configuration.GetConnectionString("DefaultConnection")))
            {
                try
                {
                    var p = new List<SqlParameter>();
                    p.Add(new SqlParameter("@UserId", UserId));
                    var data = dbConn.ExecuteQuery("p_Menus_Select_By_UserId", p);
                    return new Result<DataTable>(true, data);
                }
                catch (Exception e)
                {
                    return new Result<DataTable>(false, e.Message);
                }
            }
        }
        public Result<DataTable> GetMenus(ParamSearch param, string UserId)
        {
            using (var dbConn = new SqlHelper(_configuration.GetConnectionString("DefaultConnection")))
            {
                try
                {
                    var p = new List<SqlParameter>();
                    p.Add(new SqlParameter("@PageIndex", param.PageIndex));
                    p.Add(new SqlParameter("@PageSize", param.PageSize));
                    p.Add(new SqlParameter("@Keyword", param.Keyword));
                    p.Add(new SqlParameter("@Sort", param.Sort));
                    p.Add(new SqlParameter("@Status", param.Status));
                    p.Add(new SqlParameter("@UserId", UserId));

                    var data = dbConn.ExecuteQuery("p_Menus_Select_By_Page", p);
                    var total = (data.Rows.Count > 0 ? Convert.ToInt32(data.Rows[0]["TotalItems"]) : 0);
                    data.Columns.Remove("TotalItems");
                    return new Result<DataTable>(true, data, total);

                }
                catch (Exception e)
                {
                    return new Result<DataTable>(false, e.Message);
                }
            }
        }
        public Result<DataTable> GetMenuParents(string UserId)
        {
            using (var dbConn = new SqlHelper(_configuration.GetConnectionString("DefaultConnection")))
            {
                try
                {
                    var p = new List<SqlParameter>();
                    p.Add(new SqlParameter("@UserId", UserId));
                    var data = dbConn.ExecuteQuery("p_Menus_Parent_Select_DropDown", p);
                    data.Columns.Remove("RowOrder");
                    return new Result<DataTable>(true, data);

                }
                catch (Exception e)
                {
                    return new Result<DataTable>(false, e.Message);
                }
            }
        }
        public Result<DataTable> GetRolesByMenuId(int MenuId)
        {
            using (var dbConn = new SqlHelper(_configuration.GetConnectionString("DefaultConnection")))
            {
                try
                {
                    var p = new List<SqlParameter>();
                    var str = @"
                        SELECT
                             r.[Id] RoleId
                            ,r.[Name]
                            ,rp.MenuId
	                    from Roles r
                        left join RolePermissions rp on rp.RoleId = r.Id and rp.MenuId = " + MenuId;
                    var data = dbConn.ExecuteString(str);
                    return new Result<DataTable>(true, data, data.Rows.Count);

                }
                catch (Exception e)
                {
                    return new Result<DataTable>(false, e.Message);
                }
            }
        }


    }
}
