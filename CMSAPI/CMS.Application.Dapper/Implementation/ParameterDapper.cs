﻿using Microsoft.Extensions.Configuration;
using CMS.Application.Dapper.Helpers;
using CMS.Application.Dapper.Interfaces;
using CMS.Utilities.SearchRequest;
using CMS.Utilities.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace CMS.Application.Dapper.Implementation {
    public class ParameterDapper : IParameterDapper {
        private readonly IConfiguration _configuration;
        public ParameterDapper(IConfiguration configuration) {
            this._configuration = configuration;
        }
        public Result<DataTable> GetParameters(ParamSearch param, string userId) {
            using (var dbConn = new SqlHelper(_configuration.GetConnectionString("DefaultConnection"))) {
                try {
                    var p = new List<SqlParameter>();
                    p.Add(new SqlParameter("@PageIndex", param.PageIndex));
                    p.Add(new SqlParameter("@PageSize", param.PageSize));
                    p.Add(new SqlParameter("@Keyword", param.Keyword));
                    p.Add(new SqlParameter("@Status", param.Status));
                    p.Add(new SqlParameter("@Sort", param.Sort));
                    p.Add(new SqlParameter("@UserId", userId));

                    var data = dbConn.ExecuteQuery("p_Parameters_Select_By_Page", p);

                    return new Result<DataTable>(true, data, data.Rows.Count > 0 ? Convert.ToInt32(data.Rows[0]["TotalItems"]) : 0);

                } catch (Exception ex) {
                    return new Result<DataTable>(false, ex.Message);
                }
            }
        }
    }
}
