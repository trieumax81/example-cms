﻿using System.Data;
using CMS.Utilities.SearchRequest;
using CMS.Utilities.ViewModels;

namespace CMS.Application.Dapper.Interfaces
{
    public interface IMenuDapper
    {
        Result<DataTable> GetMenus(ParamSearch param, string UserId);
        Result<DataTable> GetMenusByUser(string UserId);
        Result<DataTable> GetMenuParents(string UserId);
        Result<DataTable> GetRolesByMenuId(int MenuId);
    }
}
