﻿using System.Data;
using CMS.Utilities.SearchRequest;
using CMS.Utilities.ViewModels;

namespace CMS.Application.Dapper.Interfaces
{
    public interface IUserDapper
    {
        Result<DataTable> GetUsers(ParamSearch param, string UserId);
        Result<DataTable> GetRolesByUser(string UserId);
    }
}
