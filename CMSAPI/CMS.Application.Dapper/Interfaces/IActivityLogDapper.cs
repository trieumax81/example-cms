﻿using CMS.Utilities.SearchRequest;
using CMS.Utilities.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace CMS.Application.Dapper.Interfaces
{
    public interface IActivityLogDapper
    {
        Result<DataTable> GetActivityLogs(ParamSearch paramSearch, string userId);
    }
}
