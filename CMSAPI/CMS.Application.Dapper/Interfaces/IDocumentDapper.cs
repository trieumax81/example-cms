﻿using System;
using System.Data;
using CMS.Utilities.SearchRequest;
using CMS.Utilities.ViewModels;

namespace CMS.Application.Dapper.Interfaces
{
    public interface IDocumentDapper
    {
        Result<DataTable> GetDocuments(ParamSearch paramSearch, String userId);
    }
}
