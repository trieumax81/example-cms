﻿using System.Data;
using CMS.Utilities.SearchRequest;
using CMS.Utilities.ViewModels;

namespace CMS.Application.Dapper.Interfaces
{
    public interface IRoleDapper
    {
        Result<DataTable> GetRoles(ParamSearch param, string UserId);
        Result<DataTable> GetUsersByRoleId(string RoleId, string UserId);
        Result<DataTable> GetMenusByRoleId(string RoleId, string UserId);
    }
}
