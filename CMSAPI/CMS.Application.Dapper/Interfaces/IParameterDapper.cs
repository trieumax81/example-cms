﻿using System;
using System.Data;
using CMS.Utilities.SearchRequest;
using CMS.Utilities.ViewModels;

namespace CMS.Application.Dapper.Interfaces
{
    public interface IParameterDapper
    {
        Result<DataTable> GetParameters(ParamSearch paramSearch, String userId);
    }
}
