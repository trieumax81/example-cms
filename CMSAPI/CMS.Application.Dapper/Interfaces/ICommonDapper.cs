﻿using System.Data;
using CMS.Utilities.SearchRequest;
using CMS.Utilities.ViewModels;

namespace CMS.Application.Dapper.Interfaces
{
    public interface ICommonDapper
    {
        Result<DataTable> GetByType(string Type, bool IsParameter);
        Result<DataTable> GetIcons(ParamSearch paramSearch, string userId);
    }
}
