﻿using System;
using System.Collections.Generic;
using System.Text;
using CMS.Data.Enums;

namespace CMS.Data.Interfaces
{
    public interface ISwitchable
    {
        Status Status { set; get; }
    }
}
