﻿using CMS.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CMS.Data.Entities
{
    [Table("ActivityLogs")]
    public class ActivityLog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public Guid? UserId { get; set; }

        [StringLength(50)]
        public string MenuCode { get; set; }

        [StringLength(50)]
        public string Action { get; set; }

        public string Request { get; set; }

        [StringLength(512)]
        public string Description { get; set; }

        public DateTime RequestAt { get; set; }
    }
}

