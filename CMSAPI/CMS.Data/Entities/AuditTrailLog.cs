﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMS.Data.Entities
{
    [Table("AuditTrailLogs")]
    public class AuditTrailLog
    {
        [Key]
        public Guid Id { get; set; }

        [StringLength(255)]
        public string Action { get; set; }

        [StringLength(255)]
        public string TableName { get; set; }

        [StringLength(100)]
        public string RecordId { get; set; }

        [StringLength(3000)]
        public string OldValue { get; set; }

        [StringLength(3000)]
        public string NewValue { set; get; }

        public string UserId { set; get; }

        public DateTime ChangeTime{ get; set; }
    }
}
