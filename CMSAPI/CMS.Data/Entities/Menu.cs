﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CMS.Data.Interfaces;

namespace CMS.Data.Entities
{
    [Table("Menus")]
    public class Menu : IDateTracking
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int ParentId { get; set; }

        [StringLength(50)]
        public string Code { get; set; }

        [StringLength(128)]
        public string Name { get; set; }

        [StringLength(128)]
        public string Link { get; set; }

        public int Level { get; set; }

        [StringLength(50)]
        public string Icon { get; set; }

        [StringLength(50)]
        public string Color { get; set; }

        [StringLength(50)]
        public string BackgroundColor { get; set; } 

        public bool IsVisible { get; set; }

        [StringLength(20)]
        public string Status { set; get; }

        public int NumberOrder { get; set; }

        [StringLength(100)]
        public string CreatedBy { set; get; }

        public DateTime CreatedDate { set; get; }

        [StringLength(100)]
        public string UpdatedBy { set; get; }

        public DateTime UpdatedDate { set; get; }   

    }
}
