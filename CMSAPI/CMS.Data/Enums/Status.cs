﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Data.Enums
{
    public enum Status
    {
        InActive = 0,
        Active = 1
    }
}
