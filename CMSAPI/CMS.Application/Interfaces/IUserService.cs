﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS.Utilities.ViewModels.System;
using CMS.Data.Entities;
using CMS.Utilities.Dtos;
using CMS.Utilities.Enums;
using CMS.Utilities.ViewModels;

namespace CMS.Application.Interfaces
{
    public interface IUserService
    {

        Result<UserViewModel> GetById(string id);

        Result<User> Create(RegisterUserViewModel registerUserDto);

        Result<User> Update(RegisterUserViewModel updatedUserDto);

        Result<string> Delete(string userId);

        Result<string> DeleteMany(List<Guid> ids);

        bool IsExistedEmail(string id, string email);

        bool IsUserNameExisted(string userName);

        bool IsExisted(string id, string userName);

        bool UpdateProfile(UserProfileViewModel userDto);

        UserViewModel UpdateStatus(string userId, int status, byte[] version);

        User GetByEmail(string email);
        User GetByUserName(string userName);

        string ResetPassword(string userId);

        string ResetPasswordAndStatus(string userId, DatabaseEnum.UserStatus status);
         

    }
}
