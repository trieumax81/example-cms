﻿using System.Collections.Generic;
using CMS.Utilities.ViewModels.System;
using CMS.Data.Entities;

namespace CMS.Application.Interfaces
{
    public interface INotificationService
    {
        bool Add(NotificationBaseModel entityvm);

        bool Delete(int id);

        bool Update(NotificationBaseModel entityvm);

        List<NotificationViewModel> GetAll();


        NotificationViewModel GetById(int id);

        bool IsExisted(string title);

        Notification GetByTitle(string title);

        bool UpdateStatus(int id, string status);
    }
}
