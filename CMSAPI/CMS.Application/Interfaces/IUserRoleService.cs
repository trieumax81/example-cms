﻿using System;
using System.Collections.Generic;
using CMS.Utilities.ViewModels.System;
using CMS.Data.Entities;

namespace CMS.Application.Interfaces
{
    public interface IUserRoleService
    {
        List<UserRole> GetByUserId(Guid userId);

        List<UserRole> GetByRoleId(Guid roleId);

    }
}
