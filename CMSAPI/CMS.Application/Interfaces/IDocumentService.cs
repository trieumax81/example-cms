﻿using System.Collections.Generic;
using CMS.Utilities.ViewModels.System;
using CMS.Data.Entities;
using CMS.Utilities.ViewModels;

namespace CMS.Application.Interfaces
{
    public interface IDocumentService
    {
        Result<Document> Add(List<DocumentViewModel> request);

        Result<Document> DeleteMany(List<int> ids);

        Result<Document> Update(DocumentSaveModel request);

        Result<Document> GetById(int id);
    }
}
