﻿using System.Collections.Generic;
using CMS.Utilities.ViewModels.System;
using CMS.Data.Entities;
using CMS.Utilities.ViewModels;

namespace CMS.Application.Interfaces
{
    public interface IMenuService
    {
        Result<Menu> Add(MenuViewModel entityvm);

        Result<Menu> Delete(int id);

        Result<Menu> DeleteMany(List<int> ids);

        Result<Menu> Update(MenuViewModel entityvm);

        Result<Menu> GetById(int id);

        Menu GetByName(string Code); 
    }
}
