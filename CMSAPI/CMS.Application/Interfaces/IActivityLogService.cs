﻿using CMS.Data.Entities;
using CMS.Utilities.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Application.Interfaces
{
    public interface IActivityLogService
    {
        Task<Result<ActivityLog>> Create(Guid? UserId, string MenuCode, string Action, string Request = "", string Description = "");

    }
}
