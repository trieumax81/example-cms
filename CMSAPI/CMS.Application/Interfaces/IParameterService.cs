﻿using System.Collections.Generic;
using CMS.Utilities.ViewModels.Setting;
using CMS.Data.Entities;
using CMS.Utilities.ViewModels;

namespace CMS.Application.Interfaces
{
    public interface IParameterService
    {
        Result<Parameter> Add(ParameterViewModel entityvm);

        Result<Parameter> Delete(int id);

        Result<Parameter> DeleteMany(List<int> ids);

        Result<Parameter> Update(ParameterViewModel entityvm);

        //List<ParameterViewModel> GetAll();
        Result<List<ParameterViewModel>> GetAll();

        Result<ParameterViewModel> GetById(int id);

        bool IsExisted(string code, string type);

        Parameter GetByCode(string code);

        Result<Parameter> UpdateStatus(int id, string status);

        Result<List<ParameterViewModel>> GetByType(string ParameterType);

    }
}
