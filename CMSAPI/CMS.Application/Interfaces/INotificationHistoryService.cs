﻿using System.Collections.Generic;
using CMS.Utilities.ViewModels.System;
using CMS.Data.Entities;

namespace CMS.Application.Interfaces
{
    public interface INotificationHistoryService
    {
        bool Add(NotificationHistoryBaseModel entityvm);

        List<NotificationHistoryViewModel> GetAll();

        NotificationHistoryViewModel GetById(int id);

        bool UpdateStatus(int id, string status);
    }
}
