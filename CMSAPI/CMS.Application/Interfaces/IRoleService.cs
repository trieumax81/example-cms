﻿using CMS.Data.Entities;
using CMS.Utilities.ViewModels;
using CMS.Utilities.ViewModels.System;
using System;
using System.Collections.Generic;

namespace CMS.Application.Interfaces
{
    public interface IRoleService
    {
        Result<RoleViewModel> Add(RoleViewModel request);
        Result<RoleViewModel> Update(RoleViewModel request);

        bool Delete(Guid id);

        Result<bool> DeleteMany(List<Guid> Ids);

        IEnumerable<RoleViewModel> GetAll();


        RoleViewModel GetById(Guid id);

        List<RolePermissionViewModel> GetListFunctionWithRole(Guid roleId);

        bool UpdateStatus(Guid id, string status);

        Role GetByName(string name);

        bool IsExisted(string name);

        List<ClameViewModel> GetListFunctionWithRoles(List<Guid> roleIds);
    }
}
