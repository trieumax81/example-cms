﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CMS.Utilities.ViewModels.System;
using CMS.Utilities.Dtos;

namespace CMS.Application.Interfaces
{
    public interface IAuditTrailLogService
    {
        List<AuditTrailLogViewModel> GetAll();
        AuditTrailLogViewModel GetById(int id);
    }
}
