﻿using AutoMapper;
using CMS.Application.Interfaces;
using CMS.Data.EF;
using CMS.Data.EF.Repositories;
using CMS.Data.Entities;
using CMS.Utilities.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Application.Implementation
{
    public class ActivityLogService : BaseService, IActivityLogService
    {
        private IRepository<ActivityLog> _activityLogRepository;
        private IUnitOfWork _unitOfWork;
        public ActivityLogService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _activityLogRepository = unitOfWork.ActivityLogRepository;
        }

        public async Task<Result<ActivityLog>> Create(Guid? UserId, string MenuCode, string Action, string Request, string Description)
        {
            try
            {
                var entity = new ActivityLog();
                entity.MenuCode = MenuCode;
                entity.Action = Action;
                entity.Description = Description;
                entity.Request = Request;
                entity.RequestAt = DateTime.Now;
                entity.UserId = UserId;
                _activityLogRepository.Insert(entity);
                _unitOfWork.Commit();
                return new Result<ActivityLog>(true, entity);
            }
            catch (Exception ex)
            {
                return new Result<ActivityLog>(false, ex.Message);
            }
        }

       
    }
}
