﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using CMS.Application.Interfaces;
using CMS.Data.Entities;
using CMS.Data.EF.Repositories;
using CMS.Data.EF;

namespace CMS.Application.Implementation
{
    public class UserRoleService : BaseService, IUserRoleService
    {
        private IRepository<UserRole> _UserRoleRepository;
        private IUnitOfWork _unitOfWork;

        public UserRoleService(IUnitOfWork unitOfWork): base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _UserRoleRepository = unitOfWork.UserRoleRepository;
        }

        public List<UserRole> GetByUserId(Guid userId)
        {
            var result = _UserRoleRepository.GetMany(p => p.UserId == userId).ToList();
            return result;
        }

        public List<UserRole> GetByRoleId(Guid roleId)
        {
            var result = _UserRoleRepository.GetMany(p => p.RoleId == roleId).ToList();
            return result;
        }


    }
}