﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using CMS.Application.Interfaces;
using CMS.Utilities.ViewModels.System;
using CMS.Data.Entities;
using CMS.Data.EF.Repositories;
using CMS.Data.EF;

namespace CMS.Application.Implementation
{
    public class AuditTrailLogService : BaseService, IAuditTrailLogService
    {
        private IRepository<AuditTrailLog> _auditTrailLogRepository;
        private IUnitOfWork _unitOfWork;

        public AuditTrailLogService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _auditTrailLogRepository = unitOfWork.AuditTrailLogRepository;
        }

        public List<AuditTrailLogViewModel> GetAll()
        {
            var entities = _auditTrailLogRepository.GetAll().ToList();
            var results = Mapper.Map<List<AuditTrailLogViewModel>>(entities);
            return results;
        }

        public AuditTrailLogViewModel GetById(int id)
        {
            var entity = _auditTrailLogRepository.GetById(id);
            var result = Mapper.Map<AuditTrailLogViewModel>(entity);
            return result;
        }
    }
}