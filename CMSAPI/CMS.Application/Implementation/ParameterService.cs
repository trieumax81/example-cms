﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using CMS.Application.Interfaces;
using CMS.Data.Entities;
using CMS.Data.EF.Repositories;
using CMS.Data.EF;
using CMS.Utilities.Constants;
using CMS.Utilities.ViewModels;
using CMS.Utilities.ViewModels.Setting;

namespace CMS.Application.Implementation
{
    public class ParameterService : BaseService, IParameterService
    {
        private IRepository<Parameter> _parameterRepository;
        private IUnitOfWork _unitOfWork;

        public ParameterService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _parameterRepository = unitOfWork.ParameterRepository;
        }

        public Result<Parameter> Add(ParameterViewModel entityvm)
        {
            try
            {
                var entity = Mapper.Map<Parameter>(entityvm);
                entity.Status = CommonConstants.StatusActive;
                _parameterRepository.Insert(entity);
                _unitOfWork.Commit();
                return new Result<Parameter>(true, entity);
            }
            catch (Exception ex)
            {
                return new Result<Parameter>(false, ex.Message);
            }
        }

        public Result<Parameter> Delete(int id)
        {
            try
            {
                var entity = _parameterRepository.GetById(id);
                entity.Status = CommonConstants.StatusDeactivated;
                _parameterRepository.Update(entity);
                _unitOfWork.Commit();
                return new Result<Parameter>(true, entity);
            }
            catch (Exception ex)
            {
                return new Result<Parameter>(false, ex.Message);
            }
        }

        public Result<List<ParameterViewModel>> GetAll()
        {
            //var entities = _parameterRepository.GetAll().ToList();
            var entities = _parameterRepository.GetAll();

            var results = Mapper.Map<List<ParameterViewModel>>(entities);
            return new Result<List<ParameterViewModel>>(true,results,entities.Count());
        }

        public Result<ParameterViewModel> GetById(int id)
        {
            try {
                var entity = _parameterRepository.GetById(id);
                if (entity == null) {
                    return new Result<ParameterViewModel>(false, "Không tìm thấy dữ liệu!");
                } else {
                    var result = Mapper.Map<ParameterViewModel>(entity);
                    return new Result<ParameterViewModel>(true, result);
                }
            } catch (Exception ex) {
                return new Result<ParameterViewModel>(false, ex.Message);
            }
        }

        public Parameter GetByCode(string code)
        {
            var entity = string.IsNullOrEmpty(code.Trim())
                ? null
                : _parameterRepository.GetAll()
                        .FirstOrDefault(x => x.ParameterCode == code);
            return entity;
        }

        public Result<List<ParameterViewModel>> GetByType(string ParameterType)
        {
            try
            {
                var entities = _parameterRepository.FindAll(s => s.ParameterType.Contains(ParameterType) || s.ParameterCode.Contains(ParameterType)).ToList();
                var results = Mapper.Map<List<ParameterViewModel>>(entities);
                return new Result<List<ParameterViewModel>>(true, results,entities.Count());
            }
            catch(Exception e)
            {
                return new Result<List<ParameterViewModel>>(false,e.Message);
            }
        }

        public bool IsExisted(string code, string type)
        {
            return _parameterRepository.GetMany(p => p.ParameterCode.ToLower() == code.ToLower() && p.ParameterType.ToLower() == type.ToLower()).Any();
        }

        public Result<Parameter> Update(ParameterViewModel entityvm)
        {
            try
            {
                var entity = _parameterRepository.GetById(entityvm.Id);
                entity.ParameterCode = entityvm.ParameterCode;
                entity.ParameterType = entityvm.ParameterType;
                entity.Value = entityvm.Value;
                entity.Note = entityvm.Note;
                entity.Status = entityvm.Status;
                _parameterRepository.Update(entity);
                _unitOfWork.Commit();
                return new Result<Parameter>(true, entity);
            }
            catch (Exception ex)
            {
                return new Result<Parameter>(false, ex.Message);
            }
        }

        public Result<Parameter> UpdateStatus(int id, string status)
        {
            try
            {
                var entity = _parameterRepository.GetById(id);
                entity.Status = status;
                _parameterRepository.Update(entity);
                _unitOfWork.Commit();
                return new Result<Parameter>(true, entity);
            }
            catch (Exception ex)
            {
                return new Result<Parameter>(false, ex.Message);
            }
        }

        public Result<Parameter> DeleteMany(List<int> ids) {
            try {
                var entries = _parameterRepository.FindAll(s => ids.Contains(s.Id)).ToList();
                _parameterRepository.RemoveMultiple(entries);
                _unitOfWork.Commit();
            } catch (Exception ex) {
                return new Result<Parameter>(false, ex.Message);
            }
            return new Result<Parameter>(true);
        }
    }
}