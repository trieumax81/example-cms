﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using CMS.Application.Interfaces;
using CMS.Data.EF;
using CMS.Data.EF.Repositories;
using CMS.Data.Entities;
using CMS.Utilities.Constants;
using CMS.Utilities.ViewModels;
using CMS.Utilities.ViewModels.System;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CMS.Application.Implementation
{
    public class RoleService : BaseService, IRoleService
    {
        private RoleManager<Role> _roleManager;
        private IRepository<Menu> _menuRepository;
        private IRepository<UserRole> _userRoleRepository;
        private IRepository<Role> _roleRepository;
        private IRepository<RolePermission> _rolePermissionRepository;
        private IUnitOfWork _unitOfWork;

        public RoleService(RoleManager<Role> roleManager, IUnitOfWork unitOfWork
         ) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _roleManager = roleManager;
            _menuRepository = unitOfWork.MenuRepository;
            _userRoleRepository = unitOfWork.UserRoleRepository;
            _roleRepository = unitOfWork.RoleRepository;
            _rolePermissionRepository = unitOfWork.RolePermissionRepository;
        }

        public Result<RoleViewModel> Add(RoleViewModel request)
        {
            try
            {
                var role = new Role()
                {
                    Id = Guid.NewGuid(),
                    Name = request.Name,
                    Description = request.Description,
                    Status = CommonConstants.StatusActive
                };
                _roleRepository.Insert(role);
                _unitOfWork.Commit();

                //inser roles
                var rolePermisions = Mapper.Map<ICollection<RolePermission>>(request.RolePermissions);
                if (rolePermisions != null)
                {
                    rolePermisions.ToList().ForEach(p => p.RoleId = role.Id);
                    role.RolePermissions = rolePermisions;
                    _roleRepository.Insert(role);
                }
                //insert users to role
                var roleUsers = Mapper.Map<ICollection<UserRole>>(request.RoleUsers);
                foreach (var item in roleUsers)
                {
                    item.RoleId = role.Id;
                    _userRoleRepository.Insert(item);
                }

                _unitOfWork.Commit();

                return new Result<RoleViewModel>(true, Mapper.Map<RoleViewModel>(role));
            }
            catch (Exception e)
            {
                return new Result<RoleViewModel>(false, e.Message);
            }
        }
        public Result<RoleViewModel> Update(RoleViewModel request)
        {
            try
            {
                var role = _roleRepository.GetById(request.Id);
                role.Name = request.Name;
                role.Description = request.Description;
                role.Status = CommonConstants.StatusActive;
                //update  permissions
                if (request.RolePermissions != null)
                {
                    var MenuIds = new List<int>();
                    foreach (var item in request.RolePermissions)
                    {
                        MenuIds.Add(item.MenuId);
                    }
                    var deleteRolePermissions = _rolePermissionRepository.GetMany(p => p.RoleId == role.Id && MenuIds.Contains(p.MenuId)).ToList();
                    _rolePermissionRepository.RemoveMultiple(deleteRolePermissions);
                    var rolePermisions = Mapper.Map<ICollection<RolePermission>>(request.RolePermissions.Where(s => !s.IsDelete));
                    rolePermisions.ToList().ForEach(p => p.RoleId = role.Id);
                    role.RolePermissions = rolePermisions;
                    _roleRepository.Update(role);
                }
                //insert users to role
                if (request.RoleUsers != null)
                {
                    var UserIds = new List<Guid>();
                    foreach (var item in request.RoleUsers)
                    {
                        UserIds.Add(item.UserId);
                    }
                    var deleteRoleUsers = _userRoleRepository.GetMany(p => p.RoleId == role.Id && UserIds.Contains(p.UserId)).ToList();
                    _userRoleRepository.RemoveMultiple(deleteRoleUsers);
                    var roleUsers = Mapper.Map<ICollection<UserRole>>(request.RoleUsers.Where(s => !s.IsDelete));
                    foreach (var item in roleUsers) 
                    {
                        item.RoleId = role.Id;
                        _userRoleRepository.Insert(item);
                    }

                }
                _unitOfWork.Commit();
                return new Result<RoleViewModel>(true, request);
            }
            catch (Exception e)
            {
                return new Result<RoleViewModel>(false, e.Message);
            }
        }

        public bool Delete(Guid id)
        {
            try
            {
                var entity = _roleRepository.GetById(id);
                entity.Status = CommonConstants.StatusDeactivated;
                _roleRepository.Update(entity);
                _unitOfWork.Commit();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public Result<bool> DeleteMany(List<Guid> Ids)
        {
            try
            {
                var entries = _roleRepository.FindAll(s => Ids.Contains(s.Id)).ToList();
                _roleRepository.RemoveMultiple(entries);
                _unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                return new Result<bool>(false, ex.Message);
            }
            return new Result<bool>(true);
        }

        public IEnumerable<RoleViewModel> GetAll()
        {
            var entities = _roleRepository.GetAll().ToList().ToList();
            var results = Mapper.Map<List<RoleViewModel>>(entities);
            return results;
        } 
        public RoleViewModel GetById(Guid id)
        {
            var entity = _roleRepository.GetById(id);
            var result = Mapper.Map<RoleViewModel>(entity);
            return result;
        }

        public List<RolePermissionViewModel> GetListFunctionWithRole(Guid roleId)
        {
            var permissions = _rolePermissionRepository.GetMany(p => p.RoleId == roleId);
            var result = Mapper.Map<List<RolePermissionViewModel>>(permissions);
            return result;

        }

        public List<ClameViewModel> GetListFunctionWithRoles(List<Guid> roleIds)
        {
            var rolePermissions = new List<RolePermission>();
            var clames = new List<ClameViewModel>();
            foreach (var roleId in roleIds)
            {
                var permission = _rolePermissionRepository.GetMany(p => p.RoleId == roleId).ToList();
                rolePermissions.AddRange(permission);
            }

            var menuIds = rolePermissions.Select(p => p.MenuId).Distinct();
            var menus = _menuRepository.GetMany(p => menuIds.Contains(p.Id));
            foreach (var menu in menus)
            {
                var canCreateMenu = rolePermissions.Exists(p => p.MenuId == menu.Id && p.CanCreate == true);
                var canReadMenu = rolePermissions.Exists(p => p.MenuId == menu.Id && p.CanRead == true);
                var canUpdateMenu = rolePermissions.Exists(p => p.MenuId == menu.Id && p.CanUpdate == true);
                var canDeleteMenu = rolePermissions.Exists(p => p.MenuId == menu.Id && p.CanDelete == true);
                var canImportMenu = rolePermissions.Exists(p => p.MenuId == menu.Id && p.CanImport == true);
                var canExportMenu = rolePermissions.Exists(p => p.MenuId == menu.Id && p.CanExport == true);
                if (canCreateMenu)
                {
                    clames.Add(new ClameViewModel { ClameType = menu.Code, ClameValue = Action.CanCreate });
                }
                if (canReadMenu)
                {
                    clames.Add(new ClameViewModel { ClameType = menu.Code, ClameValue = Action.CanRead });
                }
                if (canUpdateMenu)
                {
                    clames.Add(new ClameViewModel { ClameType = menu.Code, ClameValue = Action.CanUpdate });
                }
                if (canDeleteMenu)
                {
                    clames.Add(new ClameViewModel { ClameType = menu.Code, ClameValue = Action.CanDelete });
                }
                if (canImportMenu)
                {
                    clames.Add(new ClameViewModel { ClameType = menu.Code, ClameValue = Action.CanImport });
                }
                if (canExportMenu)
                {
                    clames.Add(new ClameViewModel { ClameType = menu.Code, ClameValue = Action.CanExport });
                }

            }
            var result = clames.Distinct();
            return result.ToList();
        }


        public Role GetByName(string name)
        {
            var entity = string.IsNullOrEmpty(name.Trim())
                ? null
                : _roleRepository.GetAll()
                        .FirstOrDefault(x => x.Name.Equals(name, StringComparison.OrdinalIgnoreCase));
            return entity;
        }

        public bool IsExisted(string name)
        {
            return _roleRepository.GetMany(p => p.Name.ToLower() == name.ToLower()).Any();
        }

        public bool UpdateStatus(Guid id, string status)
        {
            try
            {
                var entity = _roleRepository.GetById(id);
                entity.Status = status;
                _roleRepository.Update(entity);
                _unitOfWork.Commit();
                return true;
            }
            catch
            {
                return false;
            }
        }

    }
}