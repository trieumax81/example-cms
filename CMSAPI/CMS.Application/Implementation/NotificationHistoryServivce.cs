﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CMS.Application.Interfaces;
using CMS.Utilities.ViewModels.System;
using CMS.Data.Entities;
using CMS.Utilities.Dtos;
using CMS.Data.EF.Repositories;
using CMS.Data.EF;
using CMS.Utilities.Constants;

namespace CMS.Application.Implementation
{
    public class NotificationHistoryService : BaseService, INotificationHistoryService
    {
        private IRepository<NotificationHistory> _notificationHistoryRepository;
        private IUnitOfWork _unitOfWork;

        public NotificationHistoryService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _notificationHistoryRepository = unitOfWork.NotificationHistoryRepository;
        }

        public bool Add(NotificationHistoryBaseModel entityvm)
        {
            try
            {
                var entity = Mapper.Map<NotificationHistory>(entityvm);
                entity.Status = CommonConstants.StatusActive;
                _notificationHistoryRepository.Insert(entity);
                _unitOfWork.Commit();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<NotificationHistoryViewModel> GetAll()
        {
            var entities = _notificationHistoryRepository.GetAll().ToList();
            var results = Mapper.Map<List<NotificationHistoryViewModel>>(entities);
            return results;
        }

        public NotificationHistoryViewModel GetById(int id)
        {
            var entity = _notificationHistoryRepository.GetById(id);
            var result = Mapper.Map<NotificationHistoryViewModel>(entity);
            return result;
        }

        public bool UpdateStatus(int id, string status)
        {
            try
            {
                var entity = _notificationHistoryRepository.GetById(id);
                entity.Status = status;
                _notificationHistoryRepository.Update(entity);
                _unitOfWork.Commit();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}