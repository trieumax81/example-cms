﻿using CMS.Application.Interfaces;
using CMS.Data.EF;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Application.Implementation
{
    public class BaseService : IBaseService
    {
        protected BaseService(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }

        protected IUnitOfWork UnitOfWork { get; set; }
    }
}
