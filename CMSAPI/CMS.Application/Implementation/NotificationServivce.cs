﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CMS.Application.Interfaces;
using CMS.Utilities.ViewModels.System;
using CMS.Data.Entities;
using CMS.Utilities.Dtos;
using CMS.Data.EF.Repositories;
using CMS.Data.EF;
using CMS.Utilities.Constants;

namespace CMS.Application.Implementation
{
    public class NotificationService : BaseService, INotificationService
    {
        private IRepository<Notification> _notificationRepository;
        private IUnitOfWork _unitOfWork;

        public NotificationService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _notificationRepository = unitOfWork.NotificationRepository;
        }

        public bool Add(NotificationBaseModel entityvm)
        {
            try
            {
                var entity = Mapper.Map<Notification>(entityvm);
                entity.Status = CommonConstants.StatusActive;
                _notificationRepository.Insert(entity);
                _unitOfWork.Commit();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                var entity = _notificationRepository.GetById(id);
                entity.Status = CommonConstants.StatusDeactivated;
                _notificationRepository.Update(entity);
                _unitOfWork.Commit();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<NotificationViewModel> GetAll()
        {
            var entities = _notificationRepository.GetAll().ToList();
            var results = Mapper.Map<List<NotificationViewModel>>(entities);
            return results;
        }
         

        public NotificationViewModel GetById(int id)
        {
            var entity = _notificationRepository.GetById(id);
            var result = Mapper.Map<NotificationViewModel>(entity);
            return result;
        }

        public Notification GetByTitle(string title)
        {
            var entity = string.IsNullOrEmpty(title.Trim())
              ? null
              : _notificationRepository.GetAll()
                      .FirstOrDefault(x => x.Title.Equals(title, StringComparison.OrdinalIgnoreCase));
            return entity;
        }

        public bool IsExisted(string title)
        {
            return _notificationRepository.GetMany(p => p.Title.ToLower() == title.ToLower()).Any();
        }

        public bool Update(NotificationBaseModel entityvm)
        {
            try
            {
                var entity = _notificationRepository.GetById(entityvm.Id);
                entity.Content = entityvm.Content;
                entity.Note = entityvm.Note;
                entity.Title = entityvm.Title;
                entity.Status = entityvm.Status;
                _notificationRepository.Update(entity);
                _unitOfWork.Commit();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateStatus(int id, string status)
        {
            try
            {
                var entity = _notificationRepository.GetById(id);
                entity.Status = status;
                _notificationRepository.Update(entity);
                _unitOfWork.Commit();
                return true;
            }
            catch
            {
                return false;
            }
        }

    }
}