﻿using AutoMapper;
using CMS.Application.Interfaces;
using CMS.Data.EF;
using CMS.Data.EF.Repositories;
using CMS.Data.Entities;
using CMS.Utilities.Constants;
using CMS.Utilities.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CMS.Application.Implementation
{
    public class DocumentService : BaseService, IDocumentService
    {
        private IRepository<Document> _DocumentRepository;
        private IUnitOfWork _unitOfWork;
        private IRepository<Role> _roleRepository;
        private IRepository<RolePermission> _rolePermissionRepository;

        public DocumentService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _DocumentRepository = unitOfWork.DocumentRepository;
            _roleRepository = _unitOfWork.RoleRepository;
            _rolePermissionRepository = _unitOfWork.RolePermissionRepository;
        }

        public Result<Document> Add(List<DocumentViewModel> list)
        {
            try
            {
                foreach (var item in list)
                {
                    var entity = Mapper.Map<Document>(item);
                    entity.Status = CommonConstants.StatusActive;
                    _DocumentRepository.Insert(entity);
                }
                _unitOfWork.Commit();
                return new Result<Document>(true);
            }
            catch (Exception e)
            {
                return new Result<Document>(false, e.Message);
            }
        }

        public Result<Document> Update(DocumentSaveModel request)
        {
            try
            {
                var entity = _DocumentRepository.GetById(request.Id);
                entity.Name = request.Name;
                entity.Status = request.Status;
                _DocumentRepository.Update(entity);
                _unitOfWork.Commit();
                return new Result<Document>(true, entity);
            }
            catch (Exception e)
            {
                return new Result<Document>(false, e.Message);
            }
        }

        public Result<Document> DeleteMany(List<int> ids)
        {
            try
            {
                var entries = _DocumentRepository.FindAll(s => ids.Contains(s.Id)).ToList();
                _DocumentRepository.RemoveMultiple(entries);
                _unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                return new Result<Document>(false, ex.Message);
            }
            return new Result<Document>(true);
        }

        public Result<Document> GetById(int id)
        {
            try
            {
                var entity = _DocumentRepository.GetById(id);
                return new Result<Document>(true, entity);
            }
            catch (Exception e)
            {
                return new Result<Document>(false, e.Message);
            }
        }
    }
}