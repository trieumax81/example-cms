﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using CMS.Application.Interfaces;
using CMS.Application.Providers;
using CMS.Data.EF;
using CMS.Data.EF.Repositories;
using CMS.Data.Entities;
using CMS.Utilities.Constants;
using CMS.Utilities.Enums;
using CMS.Utilities.Helpers;
using CMS.Utilities.ViewModels;
using CMS.Utilities.ViewModels.System;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CMS.Application.Implementation
{
    public class UserService : BaseService, IUserService
    {
        private IRepository<User> _userRepository;
        private IRepository<Role> _roleRepository;
        private IRepository<UserRole> _userRoleRepository;
        private UserManager<User> _userManager;
        private IUnitOfWork _unitOfWork;
        public UserService(IUnitOfWork unitOfWork, UserManager<User> userManager) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _userRepository = _unitOfWork.UserRepository;
            _userManager = userManager;
            _roleRepository = _unitOfWork.RoleRepository;
            _userRoleRepository = _unitOfWork.UserRoleRepository;
        }

        public Result<User> Create(RegisterUserViewModel request)
        {
            try
            {
                var user = new User();
                user.Id = Guid.NewGuid();
                user.UserName = request.UserName.ToLower();
                user.NormalizedUserName = request.UserName.ToUpper();
                user.FullName = request.FullName;
                user.Address = request.Address;
                user.Description = request.Description;
                user.Email = request.Email.ToLower();
                user.NormalizedEmail = request.Email.ToUpper();
                user.Status = CommonConstants.StatusActive;
                //user.PasswordHash = _userManager.PasswordHasher.HashPassword(user , registerUserDto.Password);
                user.PasswordHash = _userManager.PasswordHasher.HashPassword(user, CommonConstants.DefaultPassword);
                user.SecurityStamp = Guid.NewGuid().ToString();

                if(request.RoleIds != null)
                {
                    var roles = _roleRepository.GetMany(r => request.RoleIds.Contains(r.Name)).ToList();

                    if (request.RoleIds != null)
                    {
                        foreach (var roleId in request.RoleIds)
                        {
                            var userRole = new UserRole { UserId = user.Id, RoleId = new Guid(roleId) };
                            _userRoleRepository.Insert(userRole);
                        }
                    }
                } 
                _userRepository.Insert(user);

                _unitOfWork.Commit();
                return new Result<User>(true, user);
            }
            catch (Exception e)
            {
                return new Result<User>(false, e.Message);
            }

        }

        public Result<User> Update(RegisterUserViewModel request)
        {
            try
            {
                var user = _userRepository.GetById(request.Id);

                //Update user detail
                user.UserName = request.UserName.ToLower();
                user.NormalizedUserName = request.UserName.ToUpper();
                user.FullName = request.FullName;
                user.Address = request.Address;
                user.Description = request.Description;
                user.Email = request.Email.ToLower();
                user.NormalizedEmail = request.Email.ToUpper();

                //var userRoleOld = _userManager.GetRolesAsync(user);
                var userRoleOld = _userRoleRepository.FindAll(p => p.UserId.ToString() == user.Id.ToString()).ToList();
                _userRoleRepository.RemoveMultiple(userRoleOld);

                if (request.RoleIds != null)
                {
                    var roles = _roleRepository.GetMany(r => request.RoleIds.Contains(r.Id.ToString())).ToList();
                    foreach (var role in roles)
                    {
                        var userRole = new UserRole { UserId = user.Id, RoleId = role.Id };
                        _userRoleRepository.Insert(userRole);
                    }
                }
                _userRepository.Update(user);
                _unitOfWork.Commit();
                return new Result<User>(true, user);
            }
            catch (Exception e)
            {
                return new Result<User>(false, e.Message);
            }

        }

        public Result<string> Delete(string userId)
        {
            try
            {
                var user = _userRepository.GetById(userId);
                user.Status = CommonConstants.UserStatusDeleted;
                _userRepository.Update(user);
                return new Result<string>(true);
            }
            catch (Exception e)
            {
                return new Result<string>(false, e.Message);
            }
        }

        public Result<string> DeleteMany(List<Guid> ids)
        {
            try
            {
                var entries = _userRepository.FindAll(s => ids.Contains(s.Id)).ToList();
                foreach (var item in entries)
                {
                    item.Status = CommonConstants.StatusDeleted;
                    _userRepository.Update(item);
                }
                _unitOfWork.Commit();
                return new Result<string>(true);
            }
            catch (Exception ex)
            {
                return new Result<string>(false, ex.Message);
            }
        }

        public Result<UserViewModel> GetById(string id)
        {
            try
            {
                var user = _userManager.FindByIdAsync(id).Result;
                var result = Mapper.Map<UserViewModel>(user);
                return new Result<UserViewModel>(true, result);
            }
            catch (Exception e)
            {
                return new Result<UserViewModel>(false, e.Message);
            }

        }

        public bool IsExistedEmail(string email)
        {
            return _userRepository.GetMany(p => p.Email.Trim().ToLower() == email.Trim().ToLower()).Any();
        }

        public bool IsExistedEmail(string id, string email)
        {
            return
                _userRepository.GetMany(p => p.Email.Trim().ToLower() == email.Trim().ToLower() && p.Id.ToString() != id)
                    .Any();
        }

        public bool IsUserNameExisted(string userName)
        {
            return _userRepository.GetMany(p => p.UserName.Trim().ToLower() == userName.Trim().ToLower()).Any();
        }

        public bool IsExisted(string id, string userName)
        {
            return
                _userRepository.GetMany(p => p.UserName.Trim().ToLower() == userName.Trim().ToLower() && p.Id.ToString() != id)
                    .Any();
        }

        public string ResetPassword(string userId)
        {
            var newPassword = string.Empty;
            var user = _unitOfWork.UserRepository.GetById(userId);
            if (user != null)
            {
                newPassword = StringUtil.Generate(8, true, true, false);
                user.PasswordHash = _userManager.PasswordHasher.HashPassword(user, CommonConstants.DefaultPassword);
                _unitOfWork.UserRepository.Update(user);
                _unitOfWork.Commit();
            }

            return newPassword;
        }

        public bool UpdateProfile(UserProfileViewModel userProfile)
        {

            var user = _userRepository.GetById(new Guid(userProfile.Id));
            if (user != null)
            {
                user.FullName = userProfile.FullName;
                user.Email = userProfile.Email;
                if (userProfile.IsUploadAvatar && userProfile.Base64Avatar != null)
                {
                    var fileStorageProvider = new FileStorageProvider();
                    user.Avatar = fileStorageProvider.ProcessAndSaveBase64Img(userProfile.Base64Avatar, CommonConstants.DocumentType.Avatar, user.Id.ToString());
                }
                _userRepository.Update(user);
                _unitOfWork.Commit();
                return true;
            }
            return false;
        }

        public UserViewModel UpdateStatus(string userId, int status, byte[] version)
        {
            throw new NotImplementedException();
        }

        public User GetByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public User GetByUserName(string userName)
        {
            throw new NotImplementedException();
        }

        public string ResetPasswordAndStatus(string userId, DatabaseEnum.UserStatus status)
        {
            throw new NotImplementedException();
        }

    }
}
