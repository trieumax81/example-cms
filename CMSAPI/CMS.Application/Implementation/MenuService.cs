﻿using AutoMapper;
using CMS.Application.Interfaces;
using CMS.Data.EF;
using CMS.Data.EF.Repositories;
using CMS.Data.Entities;
using CMS.Utilities.Constants;
using CMS.Utilities.ViewModels;
using CMS.Utilities.ViewModels.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CMS.Application.Implementation
{
    public class MenuService : BaseService, IMenuService
    {
        private IRepository<Menu> _menuRepository;
        private IUnitOfWork _unitOfWork;
        private IRepository<Role> _roleRepository;
        private IRepository<RolePermission> _rolePermissionRepository;

        public MenuService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _menuRepository = unitOfWork.MenuRepository;
            _roleRepository = _unitOfWork.RoleRepository;
            _rolePermissionRepository = _unitOfWork.RolePermissionRepository;
        }

        public Result<Menu> Add(MenuViewModel request)
        {
            try
            {
                var parentMenu = _menuRepository.GetById(request.ParentId);
                if(parentMenu== null)
                {
                    request.Level = 0;
                }
                else
                {
                    request.Level = parentMenu.Level + 1;
                }

                var entity = Mapper.Map<Menu>(request);
                entity.Status = CommonConstants.StatusActive;
                _menuRepository.Insert(entity);
                _unitOfWork.Commit();

                //inser roles 
                InsertRolePermission(entity.Id);

                return new Result<Menu>(true, entity);
            }
            catch (Exception e)
            {
                return new Result<Menu>(false, e.Message);
            }
        }

        async Task<string> InsertRolePermission(int MenuId)
        {
            var rolePermisions = new RolePermission
            {
                MenuId = MenuId,
                RoleId = new Guid("08d6148a-359d-c447-3557-dace3f3c53e2"),
                CanRead = true,
                CanCreate = true,
                CanUpdate = true,
                CanDelete = true,
                CanImport = true,
                CanExport = true,
            };
            _rolePermissionRepository.Insert(rolePermisions);
            return "";
        }

        public Result<Menu> Update(MenuViewModel request)
        {
            try
            {
                var parentMenu = _menuRepository.GetById(request.ParentId);
                if (parentMenu == null)
                {
                    request.Level = 0;
                }
                else
                {
                    request.Level = parentMenu.Level + 1;
                }

                var entity = _menuRepository.GetById(request.Id);
                entity.ParentId = request.ParentId;
                entity.Level = request.Level;
                entity.NumberOrder = request.NumberOrder;
                entity.Code = request.Code;
                entity.Name = request.Name;
                entity.Link = request.Link;
                entity.Icon = request.Icon;
                entity.Color = request.Color;
                entity.BackgroundColor = request.BackgroundColor;
                entity.IsVisible = request.IsVisible;
                entity.Status = request.Status;
                _menuRepository.Update(entity);
                _unitOfWork.Commit();
                return new Result<Menu>(true, entity);
            }
            catch (Exception e)
            {
                return new Result<Menu>(false, e.Message);
            }
        }
        public Result<Menu> Delete(int id)
        {
            try
            {
                var entity = _menuRepository.GetById(id);
                entity.Status = CommonConstants.StatusDeactivated;
                _menuRepository.Update(entity);
                _unitOfWork.Commit();
                return new Result<Menu>(true);
            }
            catch (Exception e)
            {
                return new Result<Menu>(false, e.Message);
            }
        }

        public Result<Menu> DeleteMany(List<int> ids)
        {
            try
            {
                var entries = _menuRepository.FindAll(s => ids.Contains(s.Id)).ToList();
                _menuRepository.RemoveMultiple(entries);
                _unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                return new Result<Menu>(false, ex.Message);
            }
            return new Result<Menu>(true);
        }

        public Result<Menu> GetById(int id)
        {
            try
            {
                var entity = _menuRepository.GetById(id);
                return new Result<Menu>(true, entity);
            }
            catch (Exception e)
            {
                return new Result<Menu>(false, e.Message);
            }
        }


        public Menu GetByName(string Name)
        {
            try
            {
                return _menuRepository.FindSingle(s => s.Name == Name);
            }
            catch (Exception e)
            {
                return null;
            }
        }


        public Result<Menu> UpdateStatus(int id, string status)
        {
            try
            {
                var entity = _menuRepository.GetById(id);
                entity.Status = status;
                _menuRepository.Update(entity);
                _unitOfWork.Commit();
                return new Result<Menu>(true, entity);
            }
            catch (Exception e)
            {
                return new Result<Menu>(false, e.Message);
            }
        }
    }
}