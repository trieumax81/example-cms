﻿using AutoMapper;
using CMS.Utilities.ViewModels.System;
using CMS.Data.Entities;
using CMS.Utilities.ViewModels.Setting;
using CMS.Utilities.ViewModels;

namespace CMS.Application.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {
            CreateMap<UserViewModel, User>();
            CreateMap<RoleViewModel, Role>();
            CreateMap<MenuViewModel, Menu>();
            CreateMap<RolePermissionViewModel, RolePermission>();
            CreateMap<UserRoleViewModel, UserRole>();
            CreateMap<NotificationViewModel, Notification>();
            CreateMap<NotificationHistoryViewModel, NotificationHistory>();
            CreateMap<ParameterViewModel, Parameter>();
            CreateMap<DocumentViewModel, Document>();
            CreateMap<DocumentSaveModel, Document>();

        }
    }
}
