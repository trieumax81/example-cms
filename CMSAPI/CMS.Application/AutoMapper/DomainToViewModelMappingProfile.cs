﻿using AutoMapper;
using CMS.Utilities.ViewModels.System;
using CMS.Data.Entities;
using CMS.Utilities.ViewModels.Setting;
using CMS.Utilities.ViewModels;

namespace CMS.Application.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<User, UserViewModel>();
            CreateMap<Role, RoleViewModel>();
            CreateMap<Role, RoleBaseModel>();
            CreateMap<Menu, MenuViewModel>();
            CreateMap<RolePermission, RolePermissionViewModel>();
            CreateMap<UserRole, UserRoleViewModel>();
            CreateMap<Notification, NotificationViewModel>();
            CreateMap<NotificationHistory, NotificationHistoryViewModel>();
            CreateMap<Parameter, ParameterViewModel>();
            CreateMap<AuditTrailLog, AuditTrailLogViewModel>();
            CreateMap<Document, DocumentViewModel>();
        }
    }
}
