﻿using AutoMapper;
using CMS.Utilities.ViewModels.System;
using CMS.Data.Entities;

namespace CMS.Application.AutoMapper
{
    public class BaseModelToDomainMappingProfile : Profile
    {
        public BaseModelToDomainMappingProfile()
        {
            CreateMap<UserBaseModel, User>();
            CreateMap<NotificationBaseModel, Notification>();
            CreateMap<NotificationHistoryBaseModel, NotificationHistory>();
            CreateMap<RolePermissionBaseModel, RolePermission>();
        }
    }
}
