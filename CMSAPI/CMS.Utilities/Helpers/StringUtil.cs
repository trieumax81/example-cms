﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace CMS.Utilities.Helpers
{
    public static class StringUtil
    {
        public static string GetHash(string input)
        {
            using (var hashAlgorithm = new SHA256CryptoServiceProvider())
            {
                var byteValue = Encoding.UTF8.GetBytes(input);

                var byteHash = hashAlgorithm.ComputeHash(byteValue);

                return Convert.ToBase64String(byteHash);
            }
        }

        public static int ConvertToInt(string stringValue)
        {
            int value;
            return int.TryParse(stringValue, out value) ? value : 0;
        }

        public static string Generate(int length)
        {
            return Generate(length, false, false, false);
        }

        public static string Generate(int length, bool hasNumber, bool hasUppercaseChar, bool hasSpecialChar)
        {
            var random = new Random();
            var seed = random.Next(1, int.MaxValue);

            const string allowedChars = "abcdefghijkmnopqrstuvwxyz";
            const string upperChars = "ABCDEFGHJKLMNOPQRSTUVWXYZ";
            const string numberChars = "0123456789";
            const string specialChars = @"!#$%&'()*+,-./:;<=>?@[\]_";

            var chars = new char[length];
            var rd = new Random(seed);

            for (var i = 0; i < length; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }

            var randomIndexs = new List<int>();
            var randomIndex = -1;

            if (hasNumber)
            {
                do
                {
                    randomIndex = rd.Next(0, length);
                    if (!randomIndexs.Contains(randomIndex))
                    {
                        randomIndexs.Add(randomIndex);
                        break;
                    }
                } while (randomIndexs.Contains(randomIndex));

                chars[randomIndex] = numberChars[rd.Next(0, numberChars.Length)];
            }

            if (hasUppercaseChar)
            {
                do
                {
                    randomIndex = rd.Next(0, length);
                    if (!randomIndexs.Contains(randomIndex))
                    {
                        randomIndexs.Add(randomIndex);
                        break;
                    }
                } while (randomIndexs.Contains(randomIndex));

                chars[randomIndex] = upperChars[rd.Next(0, upperChars.Length)];
            }

            if (hasSpecialChar)
            {
                do
                {
                    randomIndex = rd.Next(0, length);
                    if (!randomIndexs.Contains(randomIndex))
                    {
                        randomIndexs.Add(randomIndex);
                        break;
                    }
                } while (randomIndexs.Contains(randomIndex));

                chars[randomIndex] = specialChars[rd.Next(0, specialChars.Length)];
            }

            return new string(chars);
        }

        public static string ReplaceTokens(string template, Dictionary<string, string> replacements)
        {
            var rex = new Regex(@"\{([^}]+)}");
            return rex.Replace(
                template,
                delegate (Match m)
                {
                    var key = m.Groups[1].Value;
                    var rep = replacements.ContainsKey(key) ? replacements[key] : m.Value;
                    return rep;
                });
        }

        public static bool IsBase64String(string str)
        {
            try
            {
                Convert.FromBase64String(str);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool IsNumber(string stringValue)
        {
            try
            {
                Convert.ToInt32(stringValue, CultureInfo.CurrentCulture);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static string MakeFullName(string firstName, string lastName)
        {
            return string.IsNullOrWhiteSpace(firstName) && string.IsNullOrWhiteSpace(lastName)
                ? string.Empty
                : string.Format(CultureInfo.CurrentCulture, "{0}, {1}", lastName, firstName);
        }

        public static string MakeNewFileName(string oldName, int number, string fileExt)
        {
            return string.Format(CultureInfo.CurrentCulture, "{0} ({1}){2}", oldName, number, fileExt);
        }

        public static string WildCardForKeyword(string keyword)
        {
            var kw = keyword?.Replace("%", "[%]").Replace("_", "[_]").Replace("*", "%");
            return !string.IsNullOrWhiteSpace(kw) && !kw.StartsWith("%", StringComparison.CurrentCulture) &&
                   !kw.EndsWith("%", StringComparison.CurrentCulture)
                ? string.Format(CultureInfo.CurrentCulture, "%{0}%", kw)
                : kw;
        }

        public static string BindSqlQueryLike(params string[] fields)
        {
            var sql = string.Empty;
            Array.ForEach(fields, fieldName =>
            {
                if (!string.IsNullOrWhiteSpace(fieldName))
                {
                    sql += string.IsNullOrWhiteSpace(sql)
                        ? string.Format(CultureInfo.CurrentCulture, " {0} like @p0 ", fieldName)
                        : string.Format(CultureInfo.CurrentCulture, " or {0} like @p0 ", fieldName);
                }
            });

            return string.Format(CultureInfo.CurrentCulture, " where ({0})", sql);
        }
    }
}