﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Utilities.Constants
{
    public static class CommonConstants
    {
        public const string DefaultPassword = "1";

        public const string AdministratorRoleId = "51bd39ff-6679-454f-a1ec-9fef07d25f80";
        public class AppRole
        {
            public const string AdminRole = "Admin";
        }
        public class UserClaims
        {
            public const string Roles = "Roles";
        }
        public const string StatusNew = "New";
        public const string StatusCancelled = "Cancelled";
        public const string StatusDeleted = "Deleted";
        public const string UserStatusNew = "New";
        public const string UserStatusDeleted = "Deleted";
        public const string UserStatusLocked = "Locked";
        public const string StatusActive = "Active";
        public const string StatusDeactivated = "Inactive";


        public enum DocumentType
        {
            Product,
            Avatar
        }

    }
}

public static class MenuItem
{
    public const string User = "User";
    public const string Role = "Role";
    public const string Menu = "Menu";
    public const string Notification = "Notification";
    public const string NotificationHistory = "NotificationHistory";
    public const string Parameter = "Parameter";
    public const string ActivityLog = "ActivityLog";
    public const string Document = "Document";

}


public static class Action
{
    public const string CanRead = "CanRead";
    public const string CanCreate = "CanCreate";
    public const string CanUpdate = "CanUpdate";
    public const string CanDelete = "CanDelete";
    public const string CanImport = "CanImport";
    public const string CanExport = "CanExport";
}
 