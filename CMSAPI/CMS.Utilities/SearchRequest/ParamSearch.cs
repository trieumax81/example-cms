﻿namespace CMS.Utilities.SearchRequest
{
    public class ParamSearch
    {
        private int _pageIndex;

        public ParamSearch()
        {
            PageIndex = 1;
            PageSize = 30;
            Keyword = "";
            Sort = "";
            Status = "Active";
        }
        public string Keyword { get; set; }

        public string Status { get; set; }

        public int PageIndex
        {
            get { return _pageIndex; }
            set { _pageIndex = value > 0 ? value : 1; }
        }

        public int PageSize { get; set; }
        public string Sort { get; set; }
    }
}