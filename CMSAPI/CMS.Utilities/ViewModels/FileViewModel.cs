﻿
namespace CMS.Utilities.ViewModels
{
    public class FileViewModel
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public string Type { get; set; }
        public string Extension { get; set; }
        public double Size { get; set; }
    }
}
