﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMS.Utilities.ViewModels.System
{
    public class UserRoleViewModel  
    {
        public UserViewModel User { get; set; }

        public RoleViewModel Role { get; set; }

    }
    public class UserRoleBaseModel  
    {
        public Guid UserId { get; set; }
        public Guid? RoleId { get; set; }
        public bool IsDelete { get; set; }
    }
}
