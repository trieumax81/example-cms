﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CMS.Utilities.ViewModels.System
{
    public class UserBaseModel
    {
        public string Id { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public string FullName { get; set; }

        [Required]
        public int DepartmentRefId { get; set; }

        public Guid ManagerId { get; set; }

        public string Address { get; set; }

        public string Phone { get; set; }

        [Required]
        public string Email { get; set; }

        public string Description { get; set; }

        public string Avatar { get; set; }

        public string Setting { get; set; }

        public string Status { set; get; }

        public string CreatedBy { set; get; }

        public DateTime CreatedDate { set; get; }

        public string UpdatedBy { set; get; }

        public DateTime UpdatedDate { set; get; }

    }
}
