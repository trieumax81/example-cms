﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CMS.Utilities.ViewModels.System
{
    public class UserDropdownModel
    {
        public string Id { get; set; }

        public string UserName { get; set; }

        public string FullName { get; set; }
    }
}
