﻿namespace CMS.Utilities.ViewModels.System
{
    public class UserInGroupViewModel
    {
        public string UserId { get; set; }

        public int GroupId { get; set; }
        
        public virtual UserViewModel User { get; set; }

        public virtual GroupViewModel Group { get; set; }
    }
}
