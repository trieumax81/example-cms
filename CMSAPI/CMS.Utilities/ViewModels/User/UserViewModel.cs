﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CMS.Utilities.ViewModels.System
{
    public class UserViewModel
    {
        public string Id { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public string FullName { get; set; }

        public string Address { get; set; }

        public string PhoneNumber { get; set; }

        [Required]
        public string Email { get; set; }

        public string Description { get; set; }

        public string Avatar { get; set; }

        public string Setting { get; set; }

        public string Status { set; get; }

        public string CreatedBy { set; get; }

        public DateTime CreatedDate { set; get; }

        public string UpdatedBy { set; get; }

        public DateTime UpdatedDate { set; get; }

        public virtual ICollection<UserInGroupViewModel> UserInGroups { get; set; }

        public virtual ICollection<UserRoleViewModel> UserRoles { get; set; }

        public virtual ICollection<NotificationHistoryViewModel> NotificationHistories { get; set; }



    }
}
