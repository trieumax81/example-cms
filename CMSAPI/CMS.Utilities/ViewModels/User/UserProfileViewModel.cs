﻿using CMS.Utilities.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CMS.Utilities.ViewModels.System
{
    public class UserProfileViewModel
    {
        [Required]
        public string Id { get; set; }

        [Required]
        public string FullName { get; set; }

        [Required]
        [RegularExpression(RegularExpressionConstant.EmailRules, ErrorMessage = "Invalid email pattern")]
        public string Email { get; set; }

        public string Description { get; set; }

        public string BccEmail { get; set; }

        public string Avatar { get; set; }

        public string Base64Avatar { get; set; }

        public bool IsUploadAvatar { get; set; }

    }
}