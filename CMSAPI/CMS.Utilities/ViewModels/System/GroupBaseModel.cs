﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CMS.Utilities.ViewModels.System
{
    public class GroupBaseModel
    {
        public int Id { set; get; }

        [Required]
        public string Name { set; get; }

        public string Description { set; get; }

        public string Status { get; set; }

    }
}
