﻿namespace CMS.Utilities.ViewModels.System
{
    public class ConcurrencyCheckViewModel
    {
        public byte[] RowVersion { get; set; }
    }
}