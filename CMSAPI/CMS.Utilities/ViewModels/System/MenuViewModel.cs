﻿using System.ComponentModel.DataAnnotations;

namespace CMS.Utilities.ViewModels.System
{
    public class MenuViewModel
    {
        public int Id { get; set; }

        [StringLength(50)]
        public string Code { set; get;}

        [Required]
        public string Name { get; set; }

        public string Link { get; set; }

        public int ParentId { get; set; }

        [Required]
        public int Level { get; set; }

        [Required]
        public int NumberOrder { get; set; }

        [StringLength(50)]
        public string Icon { get; set; }

        [StringLength(50)]
        public string Color { get; set; }

        [StringLength(50)]
        public string BackgroundColor { get; set; }

        public bool IsVisible { get; set; }

        public string Status { get; set; }
    }
}
