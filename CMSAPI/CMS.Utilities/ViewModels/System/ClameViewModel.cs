﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Utilities.ViewModels.System
{
    public class ClameViewModel
    {
        public string ClameType { get; set; }
        public string ClameValue { get; set; }
    }
}
