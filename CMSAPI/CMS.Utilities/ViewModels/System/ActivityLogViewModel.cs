﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CMS.Utilities.ViewModels.System
{
    public class ActivityLogViewModel
    {
        public int Id { get; set; }

        public Guid UserId { get; set; }

        [StringLength(50)]
        public string MenuCode { get; set; }

        [StringLength(50)]
        public string Action { get; set; }

        public string Request { get; set; }

        [StringLength(512)]
        public string Description { get; set; }

        public DateTime RequestAt { get; set; }

    }
}
