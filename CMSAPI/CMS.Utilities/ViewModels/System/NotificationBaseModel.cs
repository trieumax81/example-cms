﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CMS.Utilities.ViewModels.System
{
    public class NotificationBaseModel
    {
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Content { get; set; }

        public string Note { get; set; }

        public string Status { set; get; }

    }
}