﻿using CMS.Utilities.Constants;
using System;
using System.ComponentModel.DataAnnotations;

namespace CMS.Utilities.ViewModels.System
{
    public class RegisterUserViewModel : ConcurrencyCheckViewModel
    {
        public Guid Id { get; set; }

        [Required]
        public string FullName { get; set; }

        [Required]
        public string UserName { get; set; }

        public string Password { get; set; }

        [Required]
        [RegularExpression(RegularExpressionConstant.EmailRules, ErrorMessage = "Invalid email pattern")]
        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string Address { get; set; }

        public string[] RoleIds { get; set; }

        public string[] GroupIds { get; set; }

        public string Description { get; set; }



    }
}