﻿using System;

namespace CMS.Utilities.ViewModels.System
{
    public class AuditTrailLogViewModel
    {
        public Guid Id { get; set; }

        public string Action { get; set; }

        public string TableName { get; set; }

        public string RecordId { get; set; }

        public string OldValue { get; set; }

        public string NewValue { set; get; }

        public string UserId { set; get; }

        public DateTime ChangeTime{ get; set; }
    }
}
