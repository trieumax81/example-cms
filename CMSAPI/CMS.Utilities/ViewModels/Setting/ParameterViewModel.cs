﻿using System.ComponentModel.DataAnnotations;

namespace CMS.Utilities.ViewModels.Setting
{
    public class ParameterViewModel
    {
        public int Id { get; set; }

        [Required]
        public string ParameterCode { get; set; }

        [Required]
        public string ParameterType { get; set; }

        [Required]
        public string Value { get; set; }

        public string Note { get; set; }

        public string Status { set; get; }
    }
}
