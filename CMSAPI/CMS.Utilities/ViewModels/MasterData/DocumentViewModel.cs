﻿
using System.ComponentModel.DataAnnotations;

namespace CMS.Utilities.ViewModels
{
    public class DocumentViewModel
    {
        public int Id { get; set; }

        [StringLength(128)]
        public string Name { get; set; }

        [StringLength(512)]
        public string Url { get; set; }

        [StringLength(50)]
        public string Type { get; set; }

        [StringLength(50)]
        public string Extension { get; set; }

        public float Size { get; set; }

        [StringLength(50)]
        public string Status { get; set; }
    }

    public class DocumentSaveModel
    {
        public int Id { get; set; }

        [StringLength(128)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Status { get; set; }
    }
}
