﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CMS.Utilities.ViewModels
{
    public class Result<T>
    {
        public Result()
        {
        }

        public Result(bool success)
        {
            IsSuccess = success;
        }

        public Result(bool IsSuccess, List<string> Messages)
        {
            this.IsSuccess = IsSuccess;
            this.Messages = Messages;
        }
        public Result(bool IsSuccess, string Message)
        {
            this.IsSuccess = IsSuccess;
            this.Messages = new List<string> { Message };
        }
        public Result(bool IsSuccess, T Data)
        {
            this.IsSuccess = IsSuccess;
            this.Data = Data;
        }
        public Result(bool IsSuccess, T Data, int TotalItems)
        {
            this.IsSuccess = IsSuccess;
            this.Data = Data;
            this.TotalItems = TotalItems;
        }

        public bool IsSuccess { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public T Data { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? TotalItems { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<string> Messages { get; set; }
    }
}
