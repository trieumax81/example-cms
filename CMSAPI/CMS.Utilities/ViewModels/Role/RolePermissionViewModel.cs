﻿using System.ComponentModel.DataAnnotations;

namespace CMS.Utilities.ViewModels.System
{
    public class RolePermissionViewModel
    {
        public string RoleId { get; set; }

        public int MenuId { get; set; }

        public bool CanRead { set; get; }

        public bool CanCreate { set; get; }

        public bool CanUpdate { set; get; }

        public bool CanDelete { set; get; }

        public bool CanImport { set; get; }

        public bool CanExport { set; get; }

        public virtual RoleViewModel Role { get; set; }

        public virtual MenuViewModel Menu { get; set; }
    }

    public class RolePermissionBaseModel
    {
        public int MenuId { get; set; }

        public bool CanRead { set; get; }

        public bool CanCreate { set; get; }

        public bool CanUpdate { set; get; }

        public bool CanDelete { set; get; }

        public bool CanImport { set; get; }

        public bool CanExport { set; get; }

        public bool IsDelete { get; set; }

    }
}
