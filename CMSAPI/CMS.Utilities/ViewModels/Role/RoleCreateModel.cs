﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CMS.Utilities.ViewModels.System
{
    public class RoleCreateModel
    {
        [Required]
        public string Name { set; get; }

        public string Description { set; get; }

        public List<RolePermissionBaseModel> RolePermissionViewModels { get; set; }
    }
}
