﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CMS.Utilities.ViewModels.System
{ 
    public class RoleViewModel
    {
        public Guid Id { set; get; }

        [Required]
        public string Name { set; get; }

        public string Description { set; get; }

        public string Status { get; set; }

        public List<RolePermissionBaseModel> RolePermissions { get; set; }

        public List<UserRoleBaseModel> RoleUsers { get; set; }
    }
    public class RoleBaseModel
    {
        public string Id { set; get; }
        public string Name { set; get; }
    }
}
