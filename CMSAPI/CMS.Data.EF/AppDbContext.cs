﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using CMS.Data.Entities;
using Microsoft.EntityFrameworkCore;
using CMS.Data.EF.Extensions;
using System.Linq;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using CMS.Data.Interfaces;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;
using CMS.Data.EF.Seed;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using CMS.Utilities.Constants;
using CMS.Utilities.Configurations;

namespace CMS.Data.EF
{
    public class AppDbContext : DbContext
    {
        private string _currentUser;
        public AppDbContext(DbContextOptions options, UserResolverService userService) : base(options)
        {
            _currentUser = userService.GetUser();
        }

        public AppDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<User> Users { set; get; }
        public DbSet<Role> Roles { get; set; } 
        public DbSet<Menu> Menus { get; set; }
        public DbSet<RolePermission> RolePermissions { set; get; }
        public DbSet<Notification> Notifications { set; get; }
        public DbSet<NotificationHistory> NotificationHistories { set; get; }
        public DbSet<Parameter> Parameters { set; get; }
        public DbSet<AuditTrailLog> AuditTrailLogs { get; set; } 
        public DbSet<ActivityLog> ActivityLogs { get; set; }
        public DbSet<Icon> Icons { get; set; }
        public DbSet<Document> Documents { get; set; }

        protected override void OnModelCreating(ModelBuilder builder) 
        {
            #region Identity Config

            builder.Entity<IdentityUserClaim<Guid>>().ToTable("AppUserClaims").HasKey(x => x.Id);

            builder.Entity<IdentityRoleClaim<Guid>>().ToTable("AppRoleClaims").HasKey(x => x.Id);

            builder.Entity<IdentityUserLogin<Guid>>().ToTable("AppUserLogins").HasKey(x => x.UserId);

            builder.Entity<IdentityUserToken<Guid>>().ToTable("AppUserTokens").HasKey(x => new { x.UserId });

            builder.Entity<UserRole>(p => {
                p.HasKey(x => new { x.RoleId, x.UserId });
                //p.HasOne(r => r.Role)
                //  .WithMany(u => u.UserRoles)
                //  .HasForeignKey(ur => ur.RoleId)
                //  .IsRequired();
                //p.HasOne(ur => ur.User)
                //    .WithMany(r => r.UserRoles)
                //    .HasForeignKey(ur => ur.UserId)
                //    .IsRequired();
            });
            //builder.Entity<IdentityUserRole<Guid>>().ToTable("UserRoles");

            #endregion Identity Config

            builder.Entity<RolePermission>()
                        .HasKey(c => new { c.RoleId, c.MenuId });

            builder.Entity<RolePermission>()
                      .HasOne(e => e.Role)
                      .WithMany(e => e.RolePermissions)
                      .HasForeignKey(e => e.RoleId); 

            base.OnModelCreating(builder);
        }
        public override int SaveChanges()
        {
            try
            {
                var auditEntries = OnBeforeSaveChanges();
                var modified = ChangeTracker.Entries().Where(e => e.State == EntityState.Modified || e.State == EntityState.Added || e.State == EntityState.Deleted);
                foreach (EntityEntry item in modified)
                {
                    var changedOrAddedItem = item.Entity as IDateTracking;
                    if (changedOrAddedItem != null)
                    {
                        if (item.State == EntityState.Added)
                        {
                            changedOrAddedItem.CreatedDate = DateTime.Now;
                            changedOrAddedItem.CreatedBy = _currentUser;
                        }
                        changedOrAddedItem.UpdatedDate = DateTime.Now;
                        changedOrAddedItem.UpdatedBy = _currentUser;
                    }
                }
                base.SaveChanges();

                return OnAfterSaveChanges(auditEntries);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        private int OnAfterSaveChanges(List<AuditEntry> auditEntries)
        {
            if (auditEntries == null || auditEntries.Count == 0)
                return 1;

            foreach (var auditEntry in auditEntries)
            {
                // Get the final value of the temporary properties
                foreach (var prop in auditEntry.TemporaryProperties)
                {
                    if (prop.Metadata.IsPrimaryKey())
                    {
                        auditEntry.KeyValues[prop.Metadata.Name] = prop.CurrentValue;
                    }
                    else
                    {
                        auditEntry.NewValues[prop.Metadata.Name] = prop.CurrentValue;
                    }
                }

                // Save the Audit entry
                AuditTrailLogs.Add(auditEntry.ToAudit());
            }

            return SaveChanges();
        }
        private List<AuditEntry> OnBeforeSaveChanges()
        {
            ChangeTracker.DetectChanges();
            var auditEntries = new List<AuditEntry>();
            foreach (var entry in ChangeTracker.Entries())
            {
                if (entry.Entity is AuditTrailLog || entry.State == EntityState.Detached || entry.State == EntityState.Unchanged)
                    continue;

                var auditEntry = new AuditEntry(entry);
                auditEntry.TableName = entry.Metadata.Relational().TableName;
                auditEntries.Add(auditEntry);
                var isDeleted = false;
                var count = 0;
                foreach (var property in entry.Properties)
                {
                    if (property.IsTemporary)
                    {
                        // value will be generated by the database, get the value after saving
                        auditEntry.TemporaryProperties.Add(property);
                        continue;
                    }

                    string propertyName = property.Metadata.Name;
                    if (propertyName == "CreatedDate" || propertyName == "CreatedBy" ||
                        propertyName == "UpdatedDate" || propertyName == "UpdatedBy" || propertyName == "PasswordHash")
                    {
                        continue;
                    }
                    if (property.Metadata.IsPrimaryKey())
                    {
                        auditEntry.KeyValues[propertyName] = property.CurrentValue;
                        continue;
                    }
                    auditEntry.UserId = _currentUser;
                    switch (entry.State)
                    {
                        case EntityState.Added:
                            auditEntry.Action = "Add";
                            auditEntry.NewValues[propertyName] = property.CurrentValue;
                            break;

                        case EntityState.Deleted:
                            auditEntry.Action = "Delete";
                            auditEntry.OldValues[propertyName] = property.OriginalValue;
                            break;

                        case EntityState.Modified:
                            if (property.IsModified)
                            {
                                if(property.OriginalValue == null && property.CurrentValue == null)
                                {

                                }
                                var originalValue = property.OriginalValue == null ? "" : property.OriginalValue.ToString();
                                var currentValue = property.CurrentValue == null ? "" : property.CurrentValue.ToString();
                                if (originalValue != currentValue)
                                {
                                    if (propertyName == "Status" && property.CurrentValue.ToString() == CommonConstants.StatusDeactivated)
                                    {
                                        isDeleted = true;
                                        count++;

                                    }
                                    auditEntry.Action = "Update";
                                    if (isDeleted && count == 1)
                                    {
                                        auditEntry.Action = "Delete";
                                    }
                                    auditEntry.OldValues[propertyName] = property.OriginalValue;
                                    auditEntry.NewValues[propertyName] = property.CurrentValue;
                                }
                            }
                            break;
                    }
                }
            }

            // Save audit entities that have all the modifications
            foreach (var auditEntry in auditEntries.Where(_ => !_.HasTemporaryProperties))
            {
                AuditTrailLogs.Add(auditEntry.ToAudit());
            }

            // keep a list of entries where the value of some properties are unknown at this step
            return auditEntries.Where(_ => _.HasTemporaryProperties).ToList();
        }
    }

    public class AuditEntry
    {
        public AuditEntry(EntityEntry entry)
        {
            Entry = entry;
        }

        public EntityEntry Entry { get; }
        public string TableName { get; set; }
        public string UserId { get; set; }
        public string Action { get; set; }
        public Dictionary<string, object> KeyValues { get; } = new Dictionary<string, object>();
        public Dictionary<string, object> OldValues { get; } = new Dictionary<string, object>();
        public Dictionary<string, object> NewValues { get; } = new Dictionary<string, object>();
        public List<PropertyEntry> TemporaryProperties { get; } = new List<PropertyEntry>();

        public bool HasTemporaryProperties => TemporaryProperties.Any();

        public AuditTrailLog ToAudit()
        {
            var audit = new AuditTrailLog();
            audit.UserId = UserId;
            audit.TableName = TableName;
            audit.Action = Action;
            audit.ChangeTime = DateTime.UtcNow;
            audit.RecordId = JsonConvert.SerializeObject(KeyValues);
            audit.OldValue = OldValues.Count == 0 ? null : JsonConvert.SerializeObject(OldValues);
            audit.NewValue = NewValues.Count == 0 ? null : JsonConvert.SerializeObject(NewValues);
            return audit;
        }
    }
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<AppDbContext>
    {
        public AppDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<AppDbContext>();
            var connectionString = AppSettings.ConnectString;
            builder.UseSqlServer(connectionString);
            return new AppDbContext(builder.Options);
        }
    }

    public class UserResolverService
    {
        private readonly IHttpContextAccessor _context;
        public UserResolverService(IHttpContextAccessor context)
        {
            _context = context;
        }

        public string GetUser()
        {
            return _context.HttpContext != null ? _context.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier) : "";
        }
    }

}
