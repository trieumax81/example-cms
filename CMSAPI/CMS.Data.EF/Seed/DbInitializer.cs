﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS.Data.Entities;
using CMS.Data.Enums;
using CMS.Utilities.Constants;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;

namespace CMS.Data.EF.Seed
{
    public class DbInitializer
    {
        private AppDbContext _context;
        private UserManager<User> _userManager;
        private RoleManager<Role> _roleManager;
        public DbInitializer(AppDbContext context, UserManager<User> userManager, RoleManager<Role> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public async Task Seed()
        {
            try
            {
 
                if (!_context.Menus.Any())
                {
                    _context.Menus.AddRange(
                          new Menu() { Code = "Home", Name = "Home", Status = CommonConstants.StatusActive },
                          new Menu() { Code = "User", Name = "User", Status = CommonConstants.StatusActive },
                          new Menu() { Code = "Role", Name = "Role", Status = CommonConstants.StatusActive },
                          new Menu() { Code = "Menu", Name = "Menu", Status = CommonConstants.StatusActive },
                          new Menu() { Code = "Custommer", Name = "Custommer", Status = CommonConstants.StatusActive },
                          new Menu() { Code = "Department", Name = "Department", Status = CommonConstants.StatusActive },
                          new Menu() { Code = "Group", Name = "Group", Status = CommonConstants.StatusActive },
                          new Menu() { Code = "Notification", Name = "Notification", Status = CommonConstants.StatusActive },
                          new Menu() { Code = "NotificationHistory", Name = "NotificationHistory", Status = CommonConstants.StatusActive },
                          new Menu() { Code = "Parameter", Name = "Parameter", Status = CommonConstants.StatusActive },
                          new Menu() { Code = "Partner", Name = "Partner", Status = CommonConstants.StatusActive }
                        );
                }

                if (!_roleManager.Roles.Any())
                {
                    await _roleManager.CreateAsync(new Role()
                    {
                        Id = new Guid("08d6148a-359d-c447-3557-dace3f3c53e2"),
                        Name = "Administrator",
                        NormalizedName = "Administrator",
                        Description = "Adminitrator - Highest Permission"
                    });

                    await _roleManager.CreateAsync(new Role()
                    {
                        Id = new Guid("08d6148a-35ed-1cc7-447c-4f828fa386b7"),
                        Name = "User",
                        NormalizedName = "User",
                        Description = "User - User Permission"
                    });

                    await _roleManager.CreateAsync(new Role()
                    {
                        Id = Guid.NewGuid(),
                        Name = "Guest",
                        NormalizedName = "Guest",
                        Description = "Guest - Guest Permission"
                    });
                }
                if (!_userManager.Users.Any())
                {
                    await _userManager.CreateAsync(new User()
                    {
                        Id = new Guid("caa91831-8650-4d68-b00c-a2e4874042b3"),
                        UserName = "sa",
                        FullName = "Super Administrator",
                        Email = "sa@SNG-core.com",
                        Description = "Supper Adminitrator - Highest User",
                        Status = "SA",
                    }, "Admin@123");
                    var user = await _userManager.FindByNameAsync("sa");
                    await _userManager.AddToRoleAsync(user, "Administrator");

                }

                if (!_context.RolePermissions.Any())
                {
                    _context.RolePermissions.AddRange(
                        new RolePermission { RoleId = new Guid("08d6148a-359d-c447-3557-dace3f3c53e2"), MenuId = 1, CanCreate = true, CanUpdate = true, CanExport = true, CanImport = true, CanRead = true },
                        new RolePermission { RoleId = new Guid("08d6148a-359d-c447-3557-dace3f3c53e2"), MenuId = 2, CanCreate = true, CanUpdate = true, CanExport = true, CanImport = true, CanRead = true },
                        new RolePermission { RoleId = new Guid("08d6148a-359d-c447-3557-dace3f3c53e2"), MenuId = 3, CanCreate = true, CanUpdate = true, CanExport = true, CanImport = true, CanRead = true },
                        new RolePermission { RoleId = new Guid("08d6148a-359d-c447-3557-dace3f3c53e2"), MenuId = 4, CanCreate = true, CanUpdate = true, CanExport = true, CanImport = true, CanRead = true },
                        new RolePermission { RoleId = new Guid("08d6148a-359d-c447-3557-dace3f3c53e2"), MenuId = 5, CanCreate = true, CanUpdate = true, CanExport = true, CanImport = true, CanRead = true },
                        new RolePermission { RoleId = new Guid("08d6148a-359d-c447-3557-dace3f3c53e2"), MenuId = 6, CanCreate = true, CanUpdate = true, CanExport = true, CanImport = true, CanRead = true },
                        new RolePermission { RoleId = new Guid("08d6148a-359d-c447-3557-dace3f3c53e2"), MenuId = 7, CanCreate = true, CanUpdate = true, CanExport = true, CanImport = true, CanRead = true },
                        new RolePermission { RoleId = new Guid("08d6148a-359d-c447-3557-dace3f3c53e2"), MenuId = 8, CanCreate = true, CanUpdate = true, CanExport = true, CanImport = true, CanRead = true },
                        new RolePermission { RoleId = new Guid("08d6148a-359d-c447-3557-dace3f3c53e2"), MenuId = 9, CanCreate = true, CanUpdate = true, CanExport = true, CanImport = true, CanRead = true },
                        new RolePermission { RoleId = new Guid("08d6148a-359d-c447-3557-dace3f3c53e2"), MenuId = 10, CanCreate = true, CanUpdate = true, CanExport = true, CanImport = true, CanRead = true },
                        new RolePermission { RoleId = new Guid("08d6148a-359d-c447-3557-dace3f3c53e2"), MenuId = 11, CanCreate = true, CanUpdate = true, CanExport = true, CanImport = true, CanRead = true }
                        );
                }
                _context.SaveChanges();
            }
            catch(Exception ex)
            {

            }

        }
    }
}
