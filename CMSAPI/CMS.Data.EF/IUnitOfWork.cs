﻿using CMS.Data.EF.Repositories;
using CMS.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Data.EF
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<User> UserRepository { get; }
        IRepository<Role> RoleRepository { get; }
        IRepository<RolePermission> RolePermissionRepository { get; }
        IRepository<Menu> MenuRepository { get; }
        IRepository<Notification> NotificationRepository { get; }
        IRepository<NotificationHistory> NotificationHistoryRepository { get; }
        IRepository<Parameter> ParameterRepository { get; }
        IRepository<UserRole> UserRoleRepository { get; }
        IRepository<AuditTrailLog> AuditTrailLogRepository { get; } 
        IRepository<ActivityLog> ActivityLogRepository { get; }
        IRepository<Document> DocumentRepository { get; }

        /// <summary>
        /// Call save change from db context
        /// </summary>
        void Commit();
    }
}
