﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CMS.Data.EF.Migrations
{
    public partial class editDocument : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Extension",
                table: "Documents",
                maxLength: 50,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Extension",
                table: "Documents");
        }
    }
}
