﻿using Microsoft.EntityFrameworkCore;
using CMS.Data.EF.Repositories;
using CMS.Data.Entities;
using System;

namespace CMS.Data.EF
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _context;
        public UnitOfWork(AppDbContext context)
        {
            _context = context;
        }

        #region private Variables;
        private IRepository<User> _userRepository;
        private IRepository<Role> _roleRepository;
        private IRepository<RolePermission> _rolePermissionRepository;
        private IRepository<Menu> _menuRepository;
        private IRepository<Notification> _notificationRepository;
        private IRepository<NotificationHistory> _notificationHistoryRepository;
        private IRepository<Parameter> _parameterRepository;
        private IRepository<UserRole> _userRoleRepository;
        private IRepository<AuditTrailLog> _auditTrailLogRepository; 
        private IRepository<Icon> _iconRepository; 
        private IRepository<ActivityLog> _activityLogRepository;
        private IRepository<Document> _documentRepository;

        #endregion
        public IRepository<User> UserRepository
        {
            get { return _userRepository ?? (_userRepository = new Repository<User>(_context)); }
        }
        public IRepository<Role> RoleRepository
        {
            get { return _roleRepository ?? (_roleRepository = new Repository<Role>(_context)); }
        }
        public IRepository<RolePermission> RolePermissionRepository
        {
            get { return _rolePermissionRepository ?? (_rolePermissionRepository = new Repository<RolePermission>(_context)); }
        } 
        public IRepository<Menu> MenuRepository
        {
            get { return _menuRepository ?? (_menuRepository = new Repository<Menu>(_context)); }
        }
        public IRepository<Notification> NotificationRepository
        {
            get { return _notificationRepository ?? (_notificationRepository = new Repository<Notification>(_context)); }
        }
        public IRepository<NotificationHistory> NotificationHistoryRepository
        {
            get { return _notificationHistoryRepository ?? (_notificationHistoryRepository = new Repository<NotificationHistory>(_context)); }
        }
        public IRepository<Parameter> ParameterRepository
        {
            get { return _parameterRepository ?? (_parameterRepository = new Repository<Parameter>(_context)); }
        } 
        public IRepository<UserRole> UserRoleRepository
        {
            get { return _userRoleRepository ?? (_userRoleRepository = new Repository<UserRole>(_context)); }
        }
        public IRepository<AuditTrailLog> AuditTrailLogRepository
        {
            get { return _auditTrailLogRepository ?? (_auditTrailLogRepository = new Repository<AuditTrailLog>(_context)); }
        } 
        public IRepository<Icon> IconRepository
        {
            get { return _iconRepository ?? (_iconRepository = new Repository<Icon>(_context)); }
        } 

        public IRepository<ActivityLog> ActivityLogRepository
        {
            get { return _activityLogRepository ?? (_activityLogRepository = new Repository<ActivityLog>(_context)); }
        }

        public IRepository<Document> DocumentRepository
        {
            get { return _documentRepository ?? (_documentRepository = new Repository<Document>(_context)); }
        }

        public void Commit()
        {
            var result = 0;
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    result = _context.SaveChanges();
                    transaction.Commit();
                }
                catch (DbUpdateConcurrencyException concurrencyEx)
                {
                    transaction.Rollback();
                    throw new Exception();
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    throw new Exception();
                }
            }
        }
        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
