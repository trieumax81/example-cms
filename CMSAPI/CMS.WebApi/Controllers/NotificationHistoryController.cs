﻿using Microsoft.AspNetCore.Mvc;
using CMS.Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using CMS.Utilities.ViewModels.System;
using CMS.Utilities.Resources;
using CMS.WebApi.Filters;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CMS.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]/[action]")]
    public class NotificationHistoryController : BaseApiController
    {
        INotificationHistoryService _notificationHistoryService;
        public NotificationHistoryController(INotificationHistoryService NotificationHistoryService)
        {
            _notificationHistoryService = NotificationHistoryService;
        }
        //// GET: api/values
        [HttpGet]
        [ClaimRequirement(MenuItem.NotificationHistory, Action.CanRead)]
        public IActionResult GetAll()
        {
            var result = _notificationHistoryService.GetAll();
            return new OkObjectResult(result);
        }

        [HttpGet]
        [ClaimRequirement(MenuItem.NotificationHistory, Action.CanRead)]
        public IActionResult GetById(int id)
        {
            return new OkObjectResult(_notificationHistoryService.GetById(id));
        }

        [HttpPost]
        [ClaimRequirement(MenuItem.NotificationHistory, Action.CanCreate)]
        public IActionResult Create(NotificationHistoryBaseModel entity)
        {
            var validateMessage = ValidateData(entity);
            if (!string.IsNullOrEmpty(validateMessage))
            {
                return ResponseError(validateMessage);
            }
            var result = _notificationHistoryService.Add(entity);
            return new OkObjectResult(result);
        }

        [HttpPut]
        [ClaimRequirement(MenuItem.NotificationHistory, Action.CanUpdate)]
        public IActionResult UpdateStatus(int id,  string status)
        {
            var result = _notificationHistoryService.UpdateStatus(id, status);
            return new OkObjectResult(result);
        }

        private string ValidateData(NotificationHistoryBaseModel entity)
        {
            var invalidMessage = string.Empty;
            if (entity == null)
            {
                return Language.MSG_INVALID_REQUEST;
            }
            return invalidMessage;
        }

    }
}
