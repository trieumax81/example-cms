﻿using Microsoft.AspNetCore.Mvc;
using CMS.Application.Interfaces;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CMS.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]/[action]")]
    public class AudtiTrailLogController : BaseApiController
    {
        IAuditTrailLogService _auditTrailLogService;
        public AudtiTrailLogController(IAuditTrailLogService auditTrailLogService)
        {
            _auditTrailLogService = auditTrailLogService;
        }
        //// GET: api/values
        [HttpGet]
        public IActionResult GetAll()
        {
            var result = _auditTrailLogService.GetAll();
            return new OkObjectResult(result);
        }

        [HttpGet]
        public IActionResult GetById(int id)
        {
            return new OkObjectResult(_auditTrailLogService.GetById(id));
        }

    }
}
