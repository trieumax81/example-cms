﻿using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CMS.WebApi.Controllers
{
    [Produces("application/json")]
    public class BaseApiController : Controller
    {
        protected string _currentUserId
        {
            get
            {
                var userId = HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
                return userId;
            }
        }

        protected string GetDomainWithProtocol()
        {
            return $"{Request.Scheme}://{Request.Host}";
        }

        protected ActionResult ResponseError(string message = "") {
            return Ok(new Utilities.ViewModels.Result<string> { IsSuccess = false, Messages = new System.Collections.Generic.List<string> { message } });
        }
    }
}
