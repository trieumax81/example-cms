﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using CMS.Application.Dapper.Interfaces;
using CMS.Application.Interfaces;
using CMS.Utilities.Resources;
using CMS.Utilities.SearchRequest;
using CMS.Utilities.ViewModels.System;
using CMS.WebApi.Filters;
using System;
using System.Collections.Generic;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CMS.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]/[action]")]
    public class RoleController : BaseApiController
    {
        IRoleService _roleService;
        IRoleDapper _roleDapper;
        public RoleController(IRoleService RoleService, IRoleDapper roleDapper)
        {
            _roleService = RoleService;
            _roleDapper = roleDapper;
        }
        //// GET: api/values
        [HttpGet]
        [ClaimRequirement(MenuItem.Role, Action.CanRead)]
        public IActionResult GetRoles(ParamSearch param)
        {
            var result = _roleDapper.GetRoles(param,_currentUserId);
            return new OkObjectResult(result);
        }

        [HttpGet]
        [ClaimRequirement(MenuItem.Role, Action.CanRead)]
        public IActionResult GetById(Guid id)
        {
            return new OkObjectResult(_roleService.GetById(id));
        }

        [HttpPost]
        [ValidateModel]
        [ClaimRequirement(MenuItem.Role, Action.CanCreate)]
        public IActionResult Create([FromBody] RoleViewModel entity)
        {
            var validateMessage = ValidateData(entity);
            if (!string.IsNullOrEmpty(validateMessage))
            {
                return ResponseError(validateMessage);
            }
            var result = _roleService.Add(entity);
            return Ok(result);
        }

        [HttpPut]
        [ValidateModel]
        [ClaimRequirement(MenuItem.Role, Action.CanUpdate)]
        public IActionResult Update([FromBody] RoleViewModel entity)
        {
            var validateMessage = ValidateDataForUpdate(entity);
            if (!string.IsNullOrEmpty(validateMessage))
            {
                return ResponseError(validateMessage);
            }
            var result = _roleService.Update(entity);
            return Ok(result);
        }

        [HttpPut]
        [ClaimRequirement(MenuItem.Role, Action.CanUpdate)]
        public IActionResult UpdateStatus(Guid id, string status)
        {
            var result = _roleService.UpdateStatus(id, status);
            return new OkObjectResult(result);
        }

        [HttpPost]
        [ClaimRequirement(MenuItem.Role, Action.CanDelete)]
        public IActionResult DeleteMany([FromBody]List<Guid> Ids)
        {
            var result = _roleService.DeleteMany(Ids);
            return new OkObjectResult(result);
        }

        [HttpGet]
        [ClaimRequirement(MenuItem.Role, Action.CanRead)]
        public IActionResult GetMenusByRoleId(Guid RoleId)
        {
            var result = _roleDapper.GetMenusByRoleId(RoleId.ToString(), _currentUserId);
            return new OkObjectResult(result);
        }

        [HttpGet]
        [ClaimRequirement(MenuItem.Role, Action.CanRead)]
        public IActionResult GetUsersByRoleId(Guid RoleId)
        {
            var result = _roleDapper.GetUsersByRoleId(RoleId.ToString(), _currentUserId);
            return new OkObjectResult(result);
        }

        private string ValidateDataForUpdate(RoleViewModel entity)
        {
            // Check role instance was deleted by another user while editing.
            var invalidMessage = string.Empty;
            var existedRole = _roleService.GetById(entity.Id);
            if (existedRole == null)
            {
                invalidMessage = Language.MSG_ERROR_MODIFY_CONCURRENCY;
            }
            else
            {
                // Get instance have the same name with edited role.
                var existedNameRole = _roleService.GetByName(entity.Name);
                if (existedNameRole != null &&
                    !existedNameRole.Id.Equals(entity.Id))
                {
                    invalidMessage = string.Format(Language.MSG_IS_EXISTED, Language.LBL_ROLE);
                }
            }
            return invalidMessage;
        }
        private string ValidateData(RoleViewModel entity)
        {
            var invalidMessage = string.Empty;
            if (entity == null)
            {
                return Language.MSG_INVALID_REQUEST;
            }
            if (_roleService.IsExisted(entity.Name))
            {
                invalidMessage = String.Format(Language.MSG_IS_EXISTED, Language.LBL_ROLE);
            }
            return invalidMessage;
        }

    }
}
