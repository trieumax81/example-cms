﻿using CMS.Application.Dapper.Interfaces;
using CMS.Application.Interfaces;
using CMS.Utilities.Resources;
using CMS.Utilities.SearchRequest;
using CMS.Utilities.ViewModels;
using CMS.WebApi.Filters;
using CMS.WebApi.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CMS.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]/[action]")]
    public class DocumentController : BaseApiController
    {
        IDocumentService _documentService;
        IDocumentDapper _DocumentDapper;
        IActivityLogService _activityLogService;
        public DocumentController(IDocumentService DocumentService, IDocumentDapper DocumentDapper, IActivityLogService activityLogService)
        {
            _documentService = DocumentService;
            _DocumentDapper = DocumentDapper;
            _activityLogService = activityLogService;
        }

        //// GET: api/values
        [HttpGet]
        [ClaimRequirement(MenuItem.Document, Action.CanRead)]
        public IActionResult GetDocuments(ParamSearch param)
        {
            var result = _DocumentDapper.GetDocuments(param, _currentUserId);
            return new OkObjectResult(result);
        }

        [HttpGet]
        [ClaimRequirement(MenuItem.Document, Action.CanRead)]
        public IActionResult GetById(int id)
        {
            return new OkObjectResult(_documentService.GetById(id));
        }

        [HttpPost]
        [ClaimRequirement(MenuItem.Document, Action.CanCreate)]
        [ValidateModel]
        public IActionResult Create([FromForm]List<IFormFile> files)
        {
            if (files == null || files.Count == 0)
            {
                return ResponseError("Vui lòng chọn tệp trước khi lưu!");
            }

            try
            {
                var list = UploadFile(files);
                var result = _documentService.Add(list);
                _activityLogService.Create(new Guid(_currentUserId), MenuItem.Document, "Create", list.ToString());
                return new OkObjectResult(result);
            }catch(Exception e)
            {
                return ResponseError(e.Message);
            }
        }

        [HttpPut]
        [ValidateModel]
        [ClaimRequirement(MenuItem.Document, Action.CanUpdate)]
        public IActionResult Update([FromBody] DocumentSaveModel entity)
        {
            var validateMessage = ValidateDataForUpdate(entity);
            if (!string.IsNullOrEmpty(validateMessage))
            {
                return ResponseError(validateMessage);
            }
            var result = _documentService.Update(entity);
            _activityLogService.Create(new Guid(_currentUserId), MenuItem.Document, "Update", entity.ToString());
            return new OkObjectResult(result);
        }

        [HttpPost]
        [ClaimRequirement(MenuItem.Document, Action.CanDelete)]
        public IActionResult DeleteMany([FromBody] List<int> ids)
        {
            var result = _documentService.DeleteMany(ids);
            return Ok(result);
        }


        private string ValidateDataForUpdate(DocumentSaveModel entity)
        {
            // Check role instance was deleted by another user while editing.
            var invalidMessage = string.Empty;
            var existedDocument = _documentService.GetById(entity.Id);
            if (existedDocument == null)
            {
                invalidMessage = "Không tìm thấy tệp. Vui lòng kiểm tra lại!";
            }
            else
            {
                // Get instance have the same name with edited role.

            }
            return invalidMessage;
        }
        private string ValidateData(DocumentSaveModel entity)
        {
            var invalidMessage = string.Empty;
            if (entity == null)
            {
                return Language.MSG_INVALID_REQUEST;
            }

            return invalidMessage;
        }

        private List<DocumentViewModel> UploadFile(IList<IFormFile> files)
        {
            var listFiles = new List<DocumentViewModel>();
            foreach (var file in files)
            {
                var uploadFile = new UploadFile();
                var prefix = @"Documents";
                var result = uploadFile.Upload(prefix, file, new List<string> { "pdf" });
                listFiles.Add(new DocumentViewModel { Type = result.Type, Url = result.Url, Name = result.Name, Size = result.Size, Extension = result.Extension });
            }
            return listFiles;
        }
    }
}
