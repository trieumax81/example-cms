﻿using System;
using Microsoft.AspNetCore.Mvc;
using CMS.Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using CMS.Utilities.ViewModels.System;
using CMS.Utilities.Resources;
using CMS.WebApi.Filters;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CMS.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]/[action]")]
    public class NotificationController : BaseApiController
    {
        INotificationService _notificationService;
        public NotificationController(INotificationService NotificationService)
        {
            _notificationService = NotificationService;
        }
        //// GET: api/values
        [HttpGet]
        public IActionResult GetAll()
        {
            var result = _notificationService.GetAll();
            return new OkObjectResult(result);
        }

        [HttpGet]
        public IActionResult GetById(int id)
        {
            return new OkObjectResult(_notificationService.GetById(id));
        }

        [HttpPost]
        public IActionResult Create(NotificationBaseModel entity)
        {
            var validateMessage = ValidateData(entity);
            if (!string.IsNullOrEmpty(validateMessage))
            {
                return ResponseError(validateMessage);
            }
            var result = _notificationService.Add(entity);
            return new OkObjectResult(result);
        }

        [HttpPut]
        public IActionResult Update(NotificationBaseModel entity)
        {
            var validateMessage = ValidateDataForUpdate(entity);
            if (!string.IsNullOrEmpty(validateMessage))
            {
                return ResponseError(validateMessage);
            }
            var result = _notificationService.Update(entity);
            return new OkObjectResult(result);
        }

        [HttpPut]
        public IActionResult UpdateStatus(int id,  string status)
        {
            var result = _notificationService.UpdateStatus(id, status);
            return new OkObjectResult(result);
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            var NotificationEntity = _notificationService.GetById(id);
            if (NotificationEntity == null)
            {
                return ResponseError(Language.MSG_ERROR_MODIFY_CONCURRENCY);
            }
            var result = _notificationService.Delete(id);
            return new OkObjectResult(result);
        }
        private string ValidateDataForUpdate(NotificationBaseModel entity)
        {
            // Check role instance was deleted by another user while editing.
            var invalidMessage = string.Empty;
            var existedNotification = _notificationService.GetById(entity.Id);
            if (existedNotification == null)
            {
                invalidMessage = Language.MSG_ERROR_MODIFY_CONCURRENCY;
            }
            else
            {
                // Get instance have the same name with edited role.
                var existedNameNotification = _notificationService.GetByTitle(entity.Title);
                if (existedNameNotification != null &&
                    !existedNameNotification.Id.Equals(entity.Id))
                {
                    invalidMessage = string.Format(Language.MSG_IS_EXISTED, Language.LBL_NOTIFICATION);
                }
            }
            return invalidMessage;
        }
        private string ValidateData(NotificationBaseModel entity)
        {
            var invalidMessage = string.Empty;
            if (entity == null)
            {
                return Language.MSG_INVALID_REQUEST;
            }
            if (_notificationService.IsExisted(entity.Title))
            {
                invalidMessage = String.Format(Language.MSG_IS_EXISTED, Language.LBL_NOTIFICATION);
            }
            return invalidMessage;
        }

    }
}
