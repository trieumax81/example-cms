﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CMS.Application.Dapper.Interfaces;
using CMS.Application.Interfaces;
using CMS.Utilities.Resources;
using CMS.Utilities.SearchRequest;
using CMS.WebApi.Filters;

namespace CMS.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]/[action]")]
    public class ActivityLogController : BaseApiController
    {
        IActivityLogService _activityLogService;
        IActivityLogDapper _activityLogDapper;

        public ActivityLogController(IActivityLogService activityLogService, IActivityLogDapper activityLogDapper)
        {
            _activityLogService = activityLogService;
            _activityLogDapper = activityLogDapper;
        }

        [HttpGet]
        [ClaimRequirement(MenuItem.ActivityLog, Action.CanRead)]
        public IActionResult GetActivityLogs(ParamSearch param)
        {
            var result = this._activityLogDapper.GetActivityLogs(param, _currentUserId);
            return Ok(result);
        }

    }
}