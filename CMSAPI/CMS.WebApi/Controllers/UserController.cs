﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using CMS.Application.Dapper.Interfaces;
using CMS.Application.Interfaces;
using CMS.Data.Entities;
using CMS.Utilities.Configurations;
using CMS.Utilities.Constants;
using CMS.Utilities.Enums;
using CMS.Utilities.Resources;
using CMS.Utilities.SearchRequest;
using CMS.Utilities.ViewModels;
using CMS.Utilities.ViewModels.System;
using CMS.WebApi.Filters;
using CMS.WebApi.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CMS.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]/")]
    public class UserController : BaseApiController
    {
        private readonly IMemoryCache _memoryCache;
        private readonly UserManager<User> _userManager;
        IUserDapper _userDapper;
        IRoleDapper _roleDapper;
        public UserController(IUserService userService
            , IRoleService roleService
            , IAuditTrailLogService auditTrailLog
            , IMemoryCache memoryCache
            , UserManager<User> userManager
            , IUserDapper userDapper
            , IRoleDapper roleDapper
            )
        {
            _userService = userService;
            _roleService = roleService;
            _auditTrailLog = auditTrailLog;
            _memoryCache = memoryCache;
            _userManager = userManager;
            _userDapper = userDapper;
            _roleDapper = roleDapper;
        }

        [HttpPost]
        [Route("Create")]
        [ClaimRequirement(MenuItem.User, Action.CanCreate)]
        public async Task<IActionResult> Create([FromBody]RegisterUserViewModel register)
        {
            try
            {
                if (_userService.IsUserNameExisted(register.UserName))
                {
                    return ResponseError(Language.MSG_USER_NAME_IS_EXISTED);
                }

                var newUser = _userService.Create(register);
                if (newUser.IsSuccess && newUser.Data != null)
                {
                    var user = newUser.Data;
                    var expiredDate = DateTime.Now.AddDays(AppSettings.RegistrationExpireDays);
                    await SendRegistrationEmail(user.Id.ToString(), user.Email, user.FullName, register.UserName, register.Password, expiredDate);
                }
                return Ok(newUser);
            }
            catch (Exception e)
            {
                return ResponseError(e.Message);
            }
        }

        //[ValidateModel]
        [HttpPut, Route("Update")]
        [ClaimRequirement(MenuItem.User, Action.CanUpdate)]
        public async Task<IActionResult> Update([FromBody]RegisterUserViewModel register)
        {
            var oldUser = _userService.GetById(register.Id.ToString());

            if (oldUser == null)
            {
                return ResponseError(Language.MSG_ERROR_MODIFY_CONCURRENCY);
            } 

            if (_userService.IsExisted(register.Id.ToString(), register.UserName))
            {
                return ResponseError(Language.MSG_USER_NAME_IS_EXISTED);
            }

            if (_userService.IsExistedEmail(register.Id.ToString(), register.Email))
            {
                return ResponseError(Language.MSG_EMAIL_IS_EXISTED);
            }

            try
            {
                var newUser = _userService.Update(register);
                return Ok(newUser);
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return ResponseError(Language.MSG_ERROR_MODIFY_CONCURRENCY);
            }
            catch (Exception)
            {
                return ResponseError(Language.MSG_ERROR_MODIFY_CONCURRENCY);
            }
        }

        //[ValidateModel]
        [AllowAnonymous]
        [HttpPost]
        [Route("ChangePassword")]
        public IActionResult ChangePassword(ChangePasswordViewModel model)
        {
            var aspNetuser = _userService.GetByUserName(model.UserName);

            if (aspNetuser == null)
            {
                return ResponseError(string.Format(Language.MSG_NOT_EXISTED, Language.LBL_USER));
            }
            if (aspNetuser.Status == CommonConstants.StatusDeactivated)
            {
                return ResponseError(string.Format(Language.MSG_NOT_ACTIVATED, Language.LBL_USER));
            }

            var user = _userManager.FindByIdAsync(aspNetuser.Id.ToString()).Result;

            if (!_userManager.CheckPasswordAsync(user, model.CurrentPassword).Result)
            {
                return ResponseError(Language.ACCOUNT_MSG_PASSWORD_IS_INCORRECT);
            }

            if (_userManager.CheckPasswordAsync(user, model.NewPassword).Result)
            {
                return ResponseError(Language.ACCOUNT_MSG_NEW_PASSWORD_SAME_CURRENT);
            }

            var result = _userManager.ChangePasswordAsync(user, model.CurrentPassword,
                model.NewPassword);

            if (!result.Result.Succeeded)
            {
                return ResponseError(Language.ACCOUNT_MSG_PASSWORD_CHANGE_FAILURE);
            }

            return Ok(Language.ACCOUNT_MSG_PASSWORD_CHANGE_SUCCESS);
        }


        [HttpDelete, Route("")]
        [ClaimRequirement(MenuItem.User, Action.CanDelete)]
        public IActionResult DeleteUser(string id)
        {
            try
            {
                var aspNetuser = _userService.GetById(id);
                if (aspNetuser == null)
                {
                    return ResponseError($"{Language.LBL_USER} {Language.MSG_IS_NOT_FOUND}");
                }
                _userService.Delete(id);
                return Ok();
            }
            catch (DbUpdateConcurrencyException)
            {
                return ResponseError(Language.MSG_ERROR_MODIFY_CONCURRENCY);
            }
            catch (Exception)
            {
                return ResponseError(Language.USER_MANAGEMENT_MSG_DELETE_FAIL);
            }
        }

        [HttpPost, Route("DeleteMany")]
        [ClaimRequirement(MenuItem.User, Action.CanDelete)]
        public IActionResult DeleteMany([FromBody]List<Guid> ids)
        {
            var result = _userService.DeleteMany(ids);
            return Ok(result);
        }

        [HttpPut]
        [Route("Status")]
        [ClaimRequirement(MenuItem.User, Action.CanUpdate)]
        public IActionResult UpdateStatus(string userId, int status, string version)
        {
            try
            {
                var resultUser = _userService.GetById(userId);
                if (resultUser.IsSuccess && resultUser.Data == null)
                {
                    return ResponseError(string.Format(Language.MSG_IS_NOT_EXISTED, Language.LBL_USER_NAME));
                }
                var user = resultUser.Data;

                var versionByte = Convert.FromBase64String(version);
                var userDto = _userService.UpdateStatus(userId, status, versionByte);
                if (userDto == null)
                {
                    return ResponseError(string.Format(Language.MSG_IS_NOT_EXISTED, Language.LBL_USER_NAME));
                }

                var content = string.Empty;

                var fullName = userDto.FullName == null
                    ? userDto.Email
                    : userDto.FullName;

                var currentStatus = user.Status;

                if (currentStatus == DatabaseEnum.UserStatus.Locked.ToString()
                    && status == (int)DatabaseEnum.UserStatus.Activated)
                {

                    content = Language.MSG_UNLOCK_USER_SUCCESS;
                    //_sendGridEmailService.SendMailUnlock(userDto.Email, fullName, "");
                }
                else if (currentStatus == DatabaseEnum.UserStatus.DeActivated.ToString()
                         && status == (int)DatabaseEnum.UserStatus.Activated)
                {
                    content = Language.ACCOUNT_MSG_REACTIVE_USER_SUCCESS;
                    //_sendGridEmailService.SendMailReactive(userDto.Email, fullName, "");
                }
                return Ok(content);
            }
            catch (DbUpdateConcurrencyException ex)
            {
                //TODO: Write log this exception to log file.
                return ResponseError(Language.MSG_ERROR_MODIFY_CONCURRENCY);
            }
            catch
            {
                return Ok(Language.MSG_SEND_EMAIL_FAILURE);
            }
        }


        [HttpGet]
        [ClaimRequirement(MenuItem.User, Action.CanRead)]
        public IActionResult GetById(string userId)
        {
            try
            {
                var searchUsers = _userService.GetById(userId);
                return Ok(searchUsers);
            }
            catch (Exception ex)
            {
                return Ok();
            }
        }

        [HttpGet, Route("GetUsers")]
        [ClaimRequirement(MenuItem.User, Action.CanRead)]
        public IActionResult GetUsers(ParamSearch param)
        {
            var result = _userDapper.GetUsers(param, _currentUserId);
            return Ok(result);
        }

        [HttpPut]
        [Route("ResetPassword")]
        public IActionResult ResetPassword(string userId)
        {
            var user = _userService.GetById(userId);
            if (user == null)
            {
                return ResponseError(string.Format(Language.MSG_IS_NOT_EXISTED, Language.LBL_USER_NAME));
            }

            var newPassword = _userService.ResetPassword(userId);

            if (string.IsNullOrEmpty(newPassword))
            {
                return ResponseError(Language.ACCOUNT_MSG_PASSWORD_RESET_FAILURE);
            }

            return Ok(Language.ACCOUNT_MSG_RESET_PASSWORD_SUCCESS);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("ForgotPassword")]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel forgotPasswordModel)
        {
            var user = _userService.GetByEmail(forgotPasswordModel.Email);
            if (user == null)
            {
                return ResponseError(string.Format(Language.MSG_IS_NOT_EXISTED, Language.LBL_USER_NAME));
            }

            if (user.Status == CommonConstants.StatusDeactivated)
            {
                return ResponseError(Language.ACCOUNT_MSG_RESET_PASSWORD_NOT_ALLOW);
            }

            try
            {
                // when pending users use forgot password function, we'll send the registration email again
                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                var newPassword = _userService.ResetPassword(user.Id.ToString());
                if (string.IsNullOrEmpty(newPassword))
                {
                    return ResponseError(Language.ACCOUNT_MSG_PASSWORD_RESET_FAILURE);
                }

                var appUser = await _userManager.FindByIdAsync(user.Id.ToString());
                //await _sendGridEmailService.SendMailForgotPassword(forgotPasswordModel.Email,
                //                   user.FullName, HttpUtility.UrlEncode(code),
                //                   GetDomainWithProtocol());

                return Ok(Language.ACCOUNT_MSG_RESET_PASSWORD_SUCCESS);
            }
            catch (Exception e)
            {
                return ResponseError(Language.MSG_SEND_EMAIL_FAILURE);
            }
        }

        [HttpGet]
        [Route("Profile")]
        public IActionResult GetCurrentUserProfile()
        {
            if (!string.IsNullOrEmpty(_currentUserId))
            {
                var userDto = _userService.GetById(_currentUserId);
                if (userDto == null)
                {
                    return ResponseError();
                }
                return Ok(userDto);
            }

            return ResponseError();
        }

        [HttpPut, Route("UpdateProfile")]
        public IActionResult UpdateProfile([FromBody] UserProfileViewModel profile)
        {
            try
            {
                if (_userService.IsExistedEmail(profile.Id, profile.Email))
                {
                    return ResponseError(Language.MSG_EMAIL_IS_EXISTED);
                }

                var updatedUser = _userService.UpdateProfile(profile);

                return Ok(updatedUser);
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return ResponseError(Language.MSG_ERROR_MODIFY_CONCURRENCY);
            }
            catch (Exception ex)
            {
                return ResponseError(Language.MSG_ERROR_MODIFY_CONCURRENCY);
            }
        }

        [HttpGet,Route("GetRoles")]
        [ClaimRequirement(MenuItem.User, Action.CanRead)]
        public IActionResult GetRoles(Guid UserId)
        {
            var data = _userDapper.GetRolesByUser(UserId.ToString());
            return Ok(data);

        }
        #region Private Methods

        private async Task SendRegistrationEmail(string userId, string email, string fullName, string userName, string password, DateTime expiredDate)
        {
            var appUser = _userManager.FindByIdAsync(userId).Result;
            try
            {
                //await _sendGridEmailService.SendMailRegister(email, fullName, userName, password, expiredDate, "");
            }
            catch (Exception ex)
            {
                //Logger.Log(new Error(ex));
            }
        }

        #endregion

        #region Private Variables

        private readonly IUserService _userService;
        private readonly IRoleService _roleService;
        private readonly IAuditTrailLogService _auditTrailLog;

        #endregion
    }
}