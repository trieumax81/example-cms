﻿using System;
using Microsoft.AspNetCore.Mvc;
using CMS.Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using CMS.Utilities.Resources;
using CMS.Utilities.ViewModels.Setting;
using CMS.WebApi.Filters;
using CMS.Application.Dapper.Interfaces;
using CMS.Utilities.ViewModels;
using System.Collections.Generic;
using System.Data;
using CMS.Utilities.SearchRequest;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CMS.WebApi.Controllers {
    [Authorize]
    [Route("api/[controller]/[action]")]
    public class ParameterController : BaseApiController {
        IParameterService _parameterService;
        IParameterDapper _parameterDapper;

        public ParameterController(IParameterService ParameterService, IParameterDapper parameterDapper) {
            _parameterService = ParameterService;
            _parameterDapper = parameterDapper;
        }
        //// GET: api/values
        [HttpGet]
        [ClaimRequirement(MenuItem.Parameter, Action.CanRead)]
        public IActionResult GetAll() {
            var result = _parameterService.GetAll();
            return new OkObjectResult(result);
        }

        //// GET: api/values
        [HttpGet]
        [ClaimRequirement(MenuItem.Parameter, Action.CanRead)]
        public IActionResult GetAllByParams(ParamSearch param) {
            var result = this._parameterDapper.GetParameters(param, _currentUserId);
            return Ok(result);
        }

        [HttpGet]
        [ClaimRequirement(MenuItem.Parameter, Action.CanRead)]
        public IActionResult GetById(int id) {
            var result = this._parameterService.GetById(id);
            return Ok(result);
        }

        [HttpPost]
        [ClaimRequirement(MenuItem.Parameter, Action.CanCreate)]
        public IActionResult Create([FromBody]ParameterViewModel entity) {
            var validateMessage = ValidateData(entity);
            if (!string.IsNullOrEmpty(validateMessage)) {
                return ResponseError(validateMessage);
            }
            var result = _parameterService.Add(entity);
            return Ok(result);
        }

        [HttpPut]
        [ClaimRequirement(MenuItem.Parameter, Action.CanUpdate)]
        public IActionResult Update([FromBody]ParameterViewModel entity) {
            var validateMessage = ValidateDataForUpdate(entity);
            if (!string.IsNullOrEmpty(validateMessage)) {
                return ResponseError(validateMessage);
            }
            var result = _parameterService.Update(entity);
            return Ok(result);
        }

        [HttpPut]
        [ClaimRequirement(MenuItem.Parameter, Action.CanUpdate)]
        public IActionResult UpdateStatus(int id, string status) {
            var result = _parameterService.UpdateStatus(id, status);
            return new OkObjectResult(result);
        }

        [HttpDelete]
        [ClaimRequirement(MenuItem.Parameter, Action.CanDelete)]
        public IActionResult DeleteMany(List<int> ids) {
            var result = _parameterService.DeleteMany(ids);
            return Ok(result);
        }

        [HttpDelete]
        [ClaimRequirement(MenuItem.Parameter, Action.CanDelete)]
        public IActionResult Delete(int id) {
            var ParameterEntity = _parameterService.GetById(id);
            if (ParameterEntity == null) {
                return ResponseError(Language.MSG_ERROR_MODIFY_CONCURRENCY);
            }
            var result = _parameterService.Delete(id);
            return new OkObjectResult(result);
        }

        private string ValidateDataForUpdate(ParameterViewModel entity) {
            // Check role instance was deleted by another user while editing.
            var invalidMessage = string.Empty;
            var existedParameter = _parameterService.GetById(entity.Id);
            if (existedParameter == null) {
                invalidMessage = Language.MSG_ERROR_MODIFY_CONCURRENCY;
            } else {
                // Get instance have the same name with edited role.
                var existedNameParameter = _parameterService.GetByCode(entity.ParameterCode);
                if (existedNameParameter != null &&
                    !existedNameParameter.Id.Equals(entity.Id)) {
                    invalidMessage = string.Format(Language.MSG_IS_EXISTED, Language.LBL_PARAMETER);
                }
            }
            return invalidMessage;
        }

        private string ValidateData(ParameterViewModel entity) {
            var invalidMessage = string.Empty;
            if (entity == null) {
                return Language.MSG_INVALID_REQUEST;
            }
            if (_parameterService.IsExisted(entity.ParameterCode, entity.ParameterType)) {
                invalidMessage = String.Format(Language.MSG_IS_EXISTED, Language.LBL_PARAMETER);
            }
            return invalidMessage;
        }

    }
}
