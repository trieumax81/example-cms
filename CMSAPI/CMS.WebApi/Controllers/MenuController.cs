﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using CMS.Application.Dapper.Interfaces;
using CMS.Application.Interfaces;
using CMS.Utilities.Resources;
using CMS.Utilities.SearchRequest;
using CMS.Utilities.ViewModels.System;
using CMS.WebApi.Filters;
using System;
using System.Collections.Generic;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CMS.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]/[action]")]
    public class MenuController : BaseApiController
    {
        IMenuService _menuService;
        IMenuDapper _menuDapper;
        IActivityLogService _activityLogService;
        IRoleDapper _roleDapper;
        public MenuController(IMenuService menuService, IMenuDapper menuDapper, IActivityLogService activityLogService, IRoleDapper roleDapper)
        {
            _menuService = menuService;
            _menuDapper = menuDapper;
            _activityLogService = activityLogService;
            _roleDapper = roleDapper;
        }
        //// GET: api/values
        [HttpGet]
        public IActionResult GetMenusByUser()
        {
            var result = _menuDapper.GetMenusByUser(_currentUserId);
            return new OkObjectResult(result);
        }

        //// GET: api/values
        [HttpGet]
        [ClaimRequirement(MenuItem.Menu, Action.CanRead)]
        public IActionResult GetMenus(ParamSearch param)
        {
            var result = _menuDapper.GetMenus(param, _currentUserId);
            return new OkObjectResult(result);
        }

        [HttpGet]
        [ClaimRequirement(MenuItem.Menu, Action.CanRead)]
        public IActionResult GetMenuParents()
        {
            var result = _menuDapper.GetMenuParents(_currentUserId);
            return new OkObjectResult(result);
        }

        [HttpGet]
        [ClaimRequirement(MenuItem.Menu, Action.CanRead)]
        public IActionResult GetById(int id)
        {
            return new OkObjectResult(_menuService.GetById(id));
        }

        [HttpPost]
        [ClaimRequirement(MenuItem.Menu, Action.CanCreate)]
        [ValidateModel]
        public IActionResult Create([FromBody]MenuViewModel entity)
        {
            var validateMessage = ValidateData(entity);
            if (!string.IsNullOrEmpty(validateMessage))
            {
                return ResponseError(validateMessage);
            }
            var result = _menuService.Add(entity);
            _activityLogService.Create(new Guid(_currentUserId), MenuItem.Menu, "Create", entity.ToString()); 
            return new OkObjectResult(result);
        }

        [HttpPut]
        [ValidateModel]
        [ClaimRequirement(MenuItem.Menu, Action.CanUpdate)]
        public IActionResult Update([FromBody] MenuViewModel entity)
        {
            var validateMessage = ValidateDataForUpdate(entity);
            if (!string.IsNullOrEmpty(validateMessage))
            {
                return ResponseError(validateMessage);
            }
            var result = _menuService.Update(entity);
            _activityLogService.Create(new Guid(_currentUserId), MenuItem.Menu, "Update", entity.ToString()); 
            return new OkObjectResult(result);
        }   
         
        [HttpDelete]
        [ClaimRequirement(MenuItem.Menu, Action.CanDelete)]
        public IActionResult Delete(int id)
        {
            var MenuEntity = _menuService.GetById(id);
            if (MenuEntity == null)
            {
                return ResponseError(Language.MSG_ERROR_MODIFY_CONCURRENCY);
            }
            var result = _menuService.Delete(id);
            return new OkObjectResult(result);
        }

        [HttpPost]
        [ClaimRequirement(MenuItem.Menu, Action.CanDelete)]
        public IActionResult DeleteMany([FromBody] List<int> ids)
        {
            var result = _menuService.DeleteMany(ids);
            return Ok(result);
        }


        private string ValidateDataForUpdate(MenuViewModel entity)
        {
            // Check role instance was deleted by another user while editing.
            var invalidMessage = string.Empty;
            var existedMenu = _menuService.GetById(entity.Id); 
            if (existedMenu == null)
            {
                invalidMessage = Language.MSG_ERROR_MODIFY_CONCURRENCY;
            }
            else
            {
                // Get instance have the same name with edited role.

            }
            return invalidMessage;
        }
        private string ValidateData(MenuViewModel entity)
        {
            var invalidMessage = string.Empty; 
            if (entity == null)
            {
                return Language.MSG_INVALID_REQUEST;
            }

            return invalidMessage;
        }

        [HttpGet]
        [ClaimRequirement(MenuItem.Menu, Action.CanRead)]
        public IActionResult GetRoles(int MenuId)
        {
            var data = _menuDapper.GetRolesByMenuId(MenuId);
            return Ok(data);

        }

    }
}
