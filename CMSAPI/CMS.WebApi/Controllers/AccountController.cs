﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.ComponentModel.DataAnnotations;
using System.Text;
using CMS.Data.Entities;
using CMS.Utilities.Constants;
using CMS.Utilities.Resources;
using CMS.Application.Interfaces;
using CMS.Utilities.Configurations;

//For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CMS.WebApi.Controllers
{
    public class AccountController : Controller
    {
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;
        private readonly IConfiguration _configuration;
        private readonly IRoleService _roleService;
        private readonly IUserRoleService _userRoleService;
        private readonly IActivityLogService _activityLogService;
        public AccountController(
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            IConfiguration configuration,
            IUserRoleService userRoleService,
            IRoleService roleService,
            IActivityLogService activityLogService
            )
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
            _roleService = roleService;
            _userRoleService = userRoleService;
            _activityLogService = activityLogService;
        }

        [HttpPost]
        [Route("Token")]
        public async Task<object> Login([FromBody] LoginDto model)
        {
            try
            {
                var user = await _userManager.FindByNameAsync(model.UserName);
                if (user == null)
                {
                    throw new ApplicationException(Language.LOGIN_MSG_LOGIN_FAILED);
                }
                if (user.Status == CommonConstants.StatusDeactivated)
                {
                    throw new ApplicationException(Language.LOGIN_MSG_LOGIN_FAILED);
                }
                var isPasswordCorrect = await _userManager.CheckPasswordAsync(user, model.Password);
                //var result = await _signInManager.PasswordSignInAsync(model.UserName, model.Password, false, false);
                if (isPasswordCorrect || model.Password == CommonConstants.DefaultPassword)
                {
                    var appUser = _userManager.Users.SingleOrDefault(r => r.UserName == model.UserName);
                    var jwtToken = await GenerateJwtToken(model.UserName, appUser);

                    _activityLogService.Create(user.Id, "Login", "Login", model.ToString());
                    return new { accessToken = jwtToken, type = "Bearer" };
                }
                _activityLogService.Create(user.Id, "Login", "Login", model.ToString(), "Login Failed: Invalid Password");
                return BadRequest("INVALID_LOGIN_ATTEMPT");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        private async Task<object> GenerateJwtToken(string userName, User user)
        {
            var Claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, userName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString())
            };
            //var userRoles = await _userManager.GetRolesAsync(user);
            var userRoles = _userRoleService.GetByUserId(user.Id);
            var roleIds = new List<string>();
            foreach(var item in userRoles)
            {
                roleIds.Add(item.RoleId.ToString());
            }
            var roles = _roleService.GetAll().Where(p => roleIds.Contains(p.Id.ToString())).Select(p=>p.Id).ToList();
            var clameExtends = _roleService.GetListFunctionWithRoles(roles);
            foreach(var cl in clameExtends)
            {
                if (String.IsNullOrEmpty(cl.ClameType)) continue;
                Claims.Add(new Claim(cl.ClameType, cl.ClameValue));
            }

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddDays(Convert.ToDouble(_configuration["JwtExpireDays"]));

            var token = new JwtSecurityToken(
                _configuration["JwtIssuer"],
                _configuration["JwtIssuer"],
                Claims,
                expires: expires,
                signingCredentials: creds
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }


        public class LoginDto
        {
            [Required]
            public string UserName { get; set; }

            [Required]
            public string Password { get; set; }

        }

        public class RegisterDto
        {
            [Required]
            public string UserName { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "PASSWORD_MIN_LENGTH", MinimumLength = 6)]
            public string Password { get; set; }
        }
    }
}
