﻿using System;
using Microsoft.AspNetCore.Mvc;
using CMS.Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using CMS.Utilities.ViewModels.System;
using CMS.Utilities.Resources;
using CMS.WebApi.Filters;
using CMS.Application.Dapper.Interfaces;
using CMS.Utilities.SearchRequest;
using System.Collections.Generic;
using CMS.Utilities.Constants;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CMS.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]/[action]")]
    public class CommonController : BaseApiController
    {
        ICommonDapper commonDapper;
        public CommonController(ICommonDapper commonDapper)
        {
            this.commonDapper = commonDapper;
        }

        [HttpGet]
        public IActionResult GetGenders()
        {
            var data = commonDapper.GetByType("Gender",true);
            return Ok(data);
        }
        [HttpGet]
        public IActionResult GetUnits()
        {
            var result = commonDapper.GetByType("Unit", false);
            return Ok(result);
        }

        [HttpGet]
        public IActionResult GetIcons(ParamSearch param) {
            var data = commonDapper.GetIcons(param, _currentUserId);
            return Ok(data);
        }
    }
}
