﻿//using Kendo.Mvc;
//using Kendo.Mvc.UI;
//using System;
//using System.Collections.Generic;
//using System.Linq;

//namespace CMSApp.WebApi.Helpers
//{
//    public class KendoHelpers
//    {
//        public string GetSortStringFormRequest(DataSourceRequest request)
//        {
//            string result = "", sortCont = "";
//            if (request.Sorts.Count > 0)
//            {
//                foreach (SortDescriptor itemSort in request.Sorts)
//                {
//                    if (itemSort.SortDirection == ListSortDirection.Ascending)
//                    {
//                        sortCont += itemSort.Member + " ASC, ";
//                    }
//                    else
//                    {
//                        sortCont += itemSort.Member + " DESC, ";
//                    }
//                }
//                sortCont = sortCont.Substring(0, sortCont.Length - 2);
//                result = " ORDER BY " + sortCont;
//            }
//            return result;
//        }
//        public static string ApplyFilter(IFilterDescriptor filter)
//        {
//            return ApplyFilter(filter, "");
//        }

//        public static string ApplyFilter(IFilterDescriptor filter, string id)
//        {
//            var filters = string.Empty;
//            if (filter is CompositeFilterDescriptor)
//            {
//                filters += "(";
//                var compositeFilterDescriptor = (CompositeFilterDescriptor)filter;
//                foreach (IFilterDescriptor childFilter in compositeFilterDescriptor.FilterDescriptors)
//                {
//                    filters += ApplyFilter(childFilter, id);
//                    filters += string.Format(" {0} ", compositeFilterDescriptor.LogicalOperator.ToString());
//                }
//            }
//            else
//            {
//                string filterDescriptor = String.Empty;
//                var descriptor = (FilterDescriptor)filter;
//                var filterMember = descriptor.Member;
//                var filterValue = descriptor.Value.ToString().Replace("'", "''");

//                DateTime temp;

//                switch (descriptor.Operator)
//                {
//                    case FilterOperator.IsEqualTo:
//                        if (filterMember.Contains('.'))
//                            filterDescriptor += String.Format("{0} = N'{1}'", id + filterMember, filterValue);
//                        else
//                            filterDescriptor += String.Format("{0} = N'{1}'", id + "[" + filterMember + "]", filterValue);
//                        break;
//                    case FilterOperator.IsNotEqualTo:
//                        if (filterMember.Contains('.'))
//                            filterDescriptor += String.Format("{0} <> N'{1}'", id + filterMember, filterValue);
//                        else
//                            filterDescriptor += String.Format("{0} <> N'{1}'", id + "[" + filterMember + "]", filterValue);
//                        break;
//                    case FilterOperator.StartsWith:
//                        if (filterMember.Contains('.'))
//                            filterDescriptor += String.Format("{0} COLLATE Latin1_General_CI_AI LIKE N'{1}%'", id + filterMember, filterValue);
//                        else
//                            filterDescriptor += String.Format("{0} COLLATE Latin1_General_CI_AI LIKE N'{1}%'", id + "[" + filterMember + "]", filterValue);

//                        break;
//                    case FilterOperator.Contains:
//                        if (filterMember.Contains('.'))
//                            filterDescriptor += String.Format("{0} COLLATE Latin1_General_CI_AI LIKE N'%{1}%'", id + filterMember, filterValue);
//                        else
//                            filterDescriptor += String.Format("{0} COLLATE Latin1_General_CI_AI LIKE N'%{1}%'", id + "[" + filterMember + "]", filterValue);

//                        break;
//                    case FilterOperator.EndsWith:
//                        if (filterMember.Contains('.'))
//                            filterDescriptor += String.Format("{0} COLLATE Latin1_General_CI_AI LIKE N'%{1}'", id + filterMember, filterValue);
//                        else
//                            filterDescriptor += String.Format("{0} COLLATE Latin1_General_CI_AI LIKE N'%{1}'", id + "[" + filterMember + "]", filterValue);
//                        break;
//                    case FilterOperator.IsLessThanOrEqualTo:
//                        if (DateTime.TryParse(filterValue.ToString(), out temp))
//                        {
//                            filterDescriptor += String.Format("{0} <='{1}'", id + "[" + filterMember + "]", filterValue);
//                        }
//                        else
//                        {
//                            filterDescriptor += String.Format("{0} <={1}", id + "[" + filterMember + "]", filterValue);
//                        }
//                        break;
//                    case FilterOperator.IsLessThan:
//                        if (DateTime.TryParse(filterValue.ToString(), out temp))
//                        {
//                            filterDescriptor += String.Format("{0}<'{1}'", id + "[" + filterMember + "]", filterValue);
//                        }
//                        else
//                        {
//                            filterDescriptor += String.Format("{0}<{1}", id + "[" + filterMember + "]", filterValue);
//                        }


//                        break;
//                    case FilterOperator.IsGreaterThanOrEqualTo:
//                        if (DateTime.TryParse(filterValue.ToString(), out temp))
//                        {
//                            filterDescriptor += String.Format("{0}>='{1}'", id + "[" + filterMember + "]", filterValue);
//                        }
//                        else
//                        {
//                            filterDescriptor += String.Format("{0}>={1}", id + "[" + filterMember + "]", filterValue);
//                        }

//                        break;
//                    case FilterOperator.IsGreaterThan:
//                        if (DateTime.TryParse(filterValue.ToString(), out temp))
//                        {
//                            filterDescriptor += String.Format("{0}>'{1}'", id + "[" + filterMember + "]", filterValue);
//                        }
//                        else
//                        {
//                            filterDescriptor += String.Format("{0}>{1}", id + "[" + filterMember + "]", filterValue);
//                        }

//                        break;
//                }



//                filters = filterDescriptor;
//            }

//            filters = filters.EndsWith("And ") == true ? string.Format("{0})", filters.Substring(0, filters.Length - 4)) : filters;
//            filters = filters.EndsWith("Or ") == true ? string.Format("{0})", filters.Substring(0, filters.Length - 4)) : filters;

//            return filters;
//        }

//        public static string ApplyFreeTextFilter(IFilterDescriptor filter, string id)
//        {
//            var filters = string.Empty;
//            if (filter is CompositeFilterDescriptor)
//            {
//                filters += "(";
//                var compositeFilterDescriptor = (CompositeFilterDescriptor)filter;
//                foreach (IFilterDescriptor childFilter in compositeFilterDescriptor.FilterDescriptors)
//                {
//                    filters += ApplyFilter(childFilter, id);
//                    filters += string.Format(" {0} ", compositeFilterDescriptor.LogicalOperator.ToString());
//                }
//            }
//            else
//            {
//                string filterDescriptor = String.Empty;
//                var descriptor = (FilterDescriptor)filter;
//                var filterMember = descriptor.Member;
//                var filterValue = descriptor.Value.ToString().Replace("'", "''");

//                DateTime temp;

//                filterDescriptor = String.Format(@" CONTAINS((Meta, MetaOCM),'""*{0}*""')", filterValue);
//                filters = filterDescriptor;
//            }

//            filters = filters.EndsWith("And ") == true ? string.Format("{0})", filters.Substring(0, filters.Length - 4)) : filters;
//            filters = filters.EndsWith("Or ") == true ? string.Format("{0})", filters.Substring(0, filters.Length - 4)) : filters;

//            return filters;
//        }


//        public static string ApplyFilterExclude(IFilterDescriptor filter, string id, List<String> exclude)
//        {
//            var filters = string.Empty;
//            if (filter is CompositeFilterDescriptor)
//            {
//                filters += "(";
//                var compositeFilterDescriptor = (CompositeFilterDescriptor)filter;
//                foreach (IFilterDescriptor childFilter in compositeFilterDescriptor.FilterDescriptors)
//                {
//                    filters += ApplyFilterExclude(childFilter, id, exclude);
//                    filters += string.Format(" {0} ", compositeFilterDescriptor.LogicalOperator.ToString());
//                }
//            }
//            else
//            {

//                string filterDescriptor = String.Empty;
//                var descriptor = (FilterDescriptor)filter;
//                var filterMember = descriptor.Member;
//                var filterValue = descriptor.Value;
//                if (!exclude.Contains(descriptor.Member))
//                {
//                    DateTime temp;

//                    switch (descriptor.Operator)
//                    {
//                        case FilterOperator.IsEqualTo:
//                            filterDescriptor += String.Format("{0} = N'{1}'", id + "[" + filterMember + "]", filterValue);
//                            break;
//                        case FilterOperator.IsNotEqualTo:
//                            filterDescriptor += String.Format("{0} <> N'{1}'", id + "[" + filterMember + "]", filterValue);
//                            break;
//                        case FilterOperator.StartsWith:
//                            filterDescriptor += String.Format("{0} like N'{1}%'", id + "[" + filterMember + "]", filterValue);
//                            break;
//                        case FilterOperator.Contains:
//                            filterDescriptor += String.Format("{0} like N'%{1}%'", id + "[" + filterMember + "]", filterValue);
//                            break;
//                        case FilterOperator.EndsWith:
//                            filterDescriptor += String.Format("{0} like N'%{1}'", id + "[" + filterMember + "]", filterValue);
//                            break;
//                        case FilterOperator.IsLessThanOrEqualTo:
//                            if (DateTime.TryParse(filterValue.ToString(), out temp))
//                            {
//                                filterDescriptor += String.Format("{0} <='{1}'", id + "[" + filterMember + "]", filterValue);
//                            }
//                            else
//                            {
//                                filterDescriptor += String.Format("{0} <={1}", id + "[" + filterMember + "]", filterValue);
//                            }


//                            break;
//                        case FilterOperator.IsLessThan:
//                            if (DateTime.TryParse(filterValue.ToString(), out temp))
//                            {
//                                filterDescriptor += String.Format("{0}<'{1}'", id + "[" + filterMember + "]", filterValue);
//                            }
//                            else
//                            {
//                                filterDescriptor += String.Format("{0}<{1}", id + "[" + filterMember + "]", filterValue);
//                            }


//                            break;
//                        case FilterOperator.IsGreaterThanOrEqualTo:
//                            if (DateTime.TryParse(filterValue.ToString(), out temp))
//                            {
//                                filterDescriptor += String.Format("{0}>='{1}'", id + "[" + filterMember + "]", filterValue);
//                            }
//                            else
//                            {
//                                filterDescriptor += String.Format("{0}>={1}", id + "[" + filterMember + "]", filterValue);
//                            }

//                            break;
//                        case FilterOperator.IsGreaterThan:
//                            if (DateTime.TryParse(filterValue.ToString(), out temp))
//                            {
//                                filterDescriptor += String.Format("{0}>'{1}'", id + "[" + filterMember + "]", filterValue);
//                            }
//                            else
//                            {
//                                filterDescriptor += String.Format("{0}>{1}", id + "[" + filterMember + "]", filterValue);
//                            }

//                            break;
//                    }
//                }
//                else
//                {
//                    filterDescriptor += "1=1";
//                }

//                filters = filterDescriptor;
//            }

//            filters = filters.EndsWith("And ") == true ? string.Format("{0})", filters.Substring(0, filters.Length - 4)) : filters;
//            filters = filters.EndsWith("Or ") == true ? string.Format("{0})", filters.Substring(0, filters.Length - 4)) : filters;

//            return filters;
//        }

//        public static string ApplyFilterNotN(IFilterDescriptor filter, string id)
//        {
//            var filters = string.Empty;
//            if (filter is CompositeFilterDescriptor)
//            {
//                filters += "(";
//                var compositeFilterDescriptor = (CompositeFilterDescriptor)filter;
//                foreach (IFilterDescriptor childFilter in compositeFilterDescriptor.FilterDescriptors)
//                {
//                    filters += ApplyFilterNotN(childFilter, id);
//                    filters += string.Format(" {0} ", compositeFilterDescriptor.LogicalOperator.ToString());
//                }
//            }
//            else
//            {
//                string filterDescriptor = String.Empty;
//                var descriptor = (FilterDescriptor)filter;
//                var filterMember = descriptor.Member;
//                var filterValue = descriptor.Value;

//                DateTime temp;

//                switch (descriptor.Operator)
//                {
//                    case FilterOperator.IsEqualTo:
//                        filterDescriptor += String.Format("{0} = '{1}'", id + "[" + filterMember + "]", filterValue);
//                        break;
//                    case FilterOperator.IsNotEqualTo:
//                        filterDescriptor += String.Format("{0} <> '{1}'", id + "[" + filterMember + "]", filterValue);
//                        break;
//                    case FilterOperator.StartsWith:
//                        filterDescriptor += String.Format("{0} like '{1}%'", id + "[" + filterMember + "]", filterValue);
//                        break;
//                    case FilterOperator.Contains:
//                        filterDescriptor += String.Format("{0} like '%{1}%'", id + "[" + filterMember + "]", filterValue);
//                        break;
//                    case FilterOperator.EndsWith:
//                        filterDescriptor += String.Format("{0} like '%{1}'", id + "[" + filterMember + "]", filterValue);
//                        break;
//                    case FilterOperator.IsLessThanOrEqualTo:
//                        if (DateTime.TryParse(filterValue.ToString(), out temp))
//                        {
//                            filterDescriptor += String.Format("{0} <='{1}'", id + "[" + filterMember + "]", filterValue);
//                        }
//                        else
//                        {
//                            filterDescriptor += String.Format("{0} <={1}", id + "[" + filterMember + "]", filterValue);
//                        }
//                        break;
//                    case FilterOperator.IsLessThan:
//                        if (DateTime.TryParse(filterValue.ToString(), out temp))
//                        {
//                            filterDescriptor += String.Format("{0}<'{1}'", id + "[" + filterMember + "]", filterValue);
//                        }
//                        else
//                        {
//                            filterDescriptor += String.Format("{0}<{1}", id + "[" + filterMember + "]", filterValue);
//                        }
//                        break;
//                    case FilterOperator.IsGreaterThanOrEqualTo:
//                        if (DateTime.TryParse(filterValue.ToString(), out temp))
//                        {
//                            filterDescriptor += String.Format("{0}>='{1}'", id + "[" + filterMember + "]", filterValue);
//                        }
//                        else
//                        {
//                            filterDescriptor += String.Format("{0}>={1}", id + "[" + filterMember + "]", filterValue);
//                        }
//                        break;
//                    case FilterOperator.IsGreaterThan:
//                        if (DateTime.TryParse(filterValue.ToString(), out temp))
//                        {
//                            filterDescriptor += String.Format("{0}>'{1}'", id + "[" + filterMember + "]", filterValue);
//                        }
//                        else
//                        {
//                            filterDescriptor += String.Format("{0}>{1}", id + "[" + filterMember + "]", filterValue);
//                        }
//                        break;
//                }

//                filters = filterDescriptor;
//            }

//            filters = filters.EndsWith("And ") == true ? string.Format("{0})", filters.Substring(0, filters.Length - 4)) : filters;
//            filters = filters.EndsWith("Or ") == true ? string.Format("{0})", filters.Substring(0, filters.Length - 4)) : filters;

//            return filters;
//        } 
//    }
//}
