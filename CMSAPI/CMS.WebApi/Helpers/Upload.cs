﻿using Microsoft.AspNetCore.Http;
using CMS.Utilities.Configurations;
using CMS.Utilities.ViewModels;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Collections.Generic;

namespace CMS.WebApi.Helpers
{
    public class UploadFile
    {
        private string rootFolder;
        private string UrlBase;
        public UploadFile()
        {
            rootFolder = Directory.GetCurrentDirectory() + "\\wwwroot";
            UrlBase = AppSettings.Configuration["UrlBase"];

            UrlBase = UrlBase != null ? UrlBase : "http://localhost";
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="domain"></param>
        /// <param name="prefix"></param>
        /// <param name="file"></param>
        /// <param name="acceptFile">image,video,music,all</param>
        /// <returns></returns>
        public DocumentViewModel Upload(string prefix, IFormFile file, List<string> acceptFile = null)
        {
            var pFile = new DocumentViewModel();
            var filename = ContentDispositionHeaderValue
                                        .Parse(file.ContentDisposition)
                                        .FileName
                                        .Trim('"');
            pFile.Name = filename;
            var extension = filename.Substring(filename.LastIndexOf('.') + 1).ToLower();

            string[] aImage = { "jpg", "png", "gif", "jpeg" };
            string[] aVideo = { "mp4", "avi" };
            string[] aMusic = { "mp3", "m4a", "flac", "m4b", "wav", "wma" };

            pFile.Extension = extension;

            if (aImage.Any(extension.Contains))
            {
                pFile.Type = "image";
            }
            else if (aVideo.Any(extension.Contains))
            {
                pFile.Type = "video";
            }
            else if (aMusic.Any(extension.Contains))
            {
                pFile.Type = "audio";
            }
            else
            {
                pFile.Type = "file";
            }

            if (!acceptFile.Any(t => t.Contains(pFile.Extension)) && (acceptFile != null || acceptFile.Count == 0))
            { 
                throw new Exception("Hệ thống chỉ chấp nhận các định dạng tệp: " + String.Join(",", acceptFile));
            }

                
            filename = DateTime.Now.ToString("yyMMddHHmmssfff") + "." + extension;
            var imageFolder = $@"\Upload\" + prefix;

            string folder = rootFolder + imageFolder;

            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            string filePath = Path.Combine(folder, filename);
            using (FileStream fs = System.IO.File.Create(filePath))
            {
                file.CopyTo(fs);
                fs.Flush();
            }
            pFile.Url = UrlBase + "/Upload/" + prefix.Replace(@"\", @"/") + "/" + filename;

            return pFile;
        }
        public bool DeleteFile(string fileUrl)
        {
            try
            {
                fileUrl = fileUrl.Replace(UrlBase, "");
                var fileInfo = new FileInfo(rootFolder + fileUrl);
                if (fileInfo.Exists)
                {
                    fileInfo.Delete();
                }
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public DocumentViewModel SaveBase64ToImage(string base64String, string prefix = "")
        {
            var pFile = new DocumentViewModel();
            var imageFolder = $@"\Upload\" + prefix;
            string folder = rootFolder + imageFolder;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            var dataType = base64String.Substring(0, 5);
        

            string filename = DateTime.Now.ToString("yyyyMMddHHmmss") + ".png";
            string filePath = Path.Combine(folder, filename);

            byte[] bytes = Convert.FromBase64String(base64String);
          
            using (var imageFile = new FileStream(filePath, FileMode.Create))
            {
                imageFile.Write(bytes, 0, bytes.Length);
                imageFile.Flush();
            }

            pFile.Url = UrlBase + "/Upload/" + prefix.Replace(@"\", @"/") + "/" + filename;
            return pFile;
        }

    }
}
