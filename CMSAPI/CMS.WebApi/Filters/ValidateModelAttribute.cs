﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using CMS.Utilities.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace CMS.WebApi.Filters
{
    public class ValidateModelAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {
            if (actionContext.ModelState.IsValid == false)
            {
                var result = new Result<string>(false);
                result.Messages = new List<string>();
                foreach (var key in actionContext.ModelState.Keys.ToList())
                {
                    if (key != null)
                    {
                        var value = actionContext.ModelState[key];
                        var valueExcept = "";
                        foreach (var er in actionContext.ModelState[key].Errors)
                        {
                            valueExcept = er.ErrorMessage;
                        }
                        result.Messages.Add(key + " " + valueExcept);
                        actionContext.ModelState.Remove(key);
                    }
                }

                actionContext.Result = new BadRequestObjectResult(result);
            }
        }
    } 
}
