﻿//using System.Diagnostics;
//using System.Net;
//using System.Net.Http;
//using System.Text;
//using System.Web;
//using System.Web.Http.Filters;
//using Elmah;
//using Newtonsoft.Json;
//using Westnetz.BrainStore.Shared.Resources;

//namespace CMS.WebApi.Filters
//{
//    public class UnhandledExceptionAttribute : ExceptionFilterAttribute
//    {
//        public override void OnException(HttpActionExecutedContext context)
//        {
//            Trace.TraceError(context.Exception.ToString());
//            ErrorLog.GetDefault(HttpContext.Current).Log(new Error(context.Exception));

//            var errorMsgJson = JsonConvert.SerializeObject(
//                new
//                {
//                    message = Language.MSG_UNHANDLED_EXCEPTION
//                },
//                Formatting.Indented);

//            context.Response = new HttpResponseMessage
//            {
//                Content = new StringContent(errorMsgJson, Encoding.UTF8, "application/json"),
//                StatusCode = HttpStatusCode.InternalServerError
//            };
//        }
//    }
//}