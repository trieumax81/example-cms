﻿using AutoMapper;
using CMS.Application.AutoMapper;
using CMS.Application.Dapper.Implementation;
using CMS.Application.Dapper.Interfaces;
using CMS.Application.Implementation;
using CMS.Application.Interfaces;
using CMS.Data.EF;
using CMS.Data.EF.Seed;
using CMS.Data.Entities;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Text;

namespace CMS.WebApi
{
    public class Startup
    {
        private string CorsPolicy = "SNGCorsPolicy";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<UserResolverService>();
            services.AddDbContext<AppDbContext>(options =>
                  options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")
                  , o => o.MigrationsAssembly("CMS.Data.EF")));
            services.AddCors(o => o.AddPolicy(CorsPolicy, builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials()
                    .SetIsOriginAllowedToAllowWildcardSubdomains()
                    ;
            }));
            services.AddAutoMapper();
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<DomainToViewModelMappingProfile>();
                cfg.AddProfile<ViewModelToDomainMappingProfile>();
                cfg.AddProfile<BaseModelToDomainMappingProfile>();
                cfg.CreateMissingTypeMaps = true;
                cfg.ValidateInlineMaps = false;
            });

            services.AddScoped<IMapper>(sp => new Mapper(sp.GetRequiredService<AutoMapper.IConfigurationProvider>(), sp.GetService));
            services.AddTransient<DbInitializer>();
            services.AddTransient(typeof(IUnitOfWork), typeof(UnitOfWork));
            // Add Database Initializer

            services.AddIdentity<User, Role>()
                .AddEntityFrameworkStores<AppDbContext>()
                .AddDefaultTokenProviders();

            //Service
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IRoleService, RoleService>();
            services.AddTransient<IUserRoleService, UserRoleService>();
            services.AddTransient<IAuditTrailLogService, AuditTrailLogService>();
            services.AddTransient<IMenuService, MenuService>();
            services.AddTransient<INotificationHistoryService, NotificationHistoryService>();
            services.AddTransient<INotificationService, NotificationService>();
            services.AddTransient<IParameterService, ParameterService>();
            services.AddTransient<IActivityLogService, ActivityLogService>();
            services.AddTransient<IDocumentService, DocumentService>();

            //Dapper
            services.AddTransient<IUserDapper, UserDapper>();
            services.AddTransient<IParameterDapper, ParameterDapper>();
            services.AddTransient<IMenuDapper, MenuDapper>();
            services.AddTransient<IRoleDapper, RoleDapper>();
            services.AddTransient<ICommonDapper, CommonDapper>();
            services.AddTransient<IActivityLogDapper, ActivityLogDapper>();
            services.AddTransient<IDocumentDapper, DocumentDapper>();

            services.AddMvc().
               AddJsonOptions(options =>
               options.SerializerSettings.ContractResolver = new DefaultContractResolver());
            // ===== Add Jwt Authentication ========
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear(); // => remove default Claims
            services
                .AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

                })
                .AddJwtBearer(cfg =>
                {
                    cfg.RequireHttpsMetadata = false;
                    cfg.SaveToken = true;
                    cfg.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = Configuration["JwtIssuer"],
                        ValidAudience = Configuration["JwtIssuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JwtKey"])),
                        ClockSkew = TimeSpan.Zero // remove delay of token when expire
                    };
                });


            services.AddSwaggerGen(s =>
            {
                s.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "SNG ERP Project",
                    Description = "SNG ERP API Swagger surface",
                });
                s.IgnoreObsoleteActions();
                s.DescribeAllEnumsAsStrings();
                s.OperationFilter<AddAuthTokenHeaderParameter>();
                s.OperationFilter<FileOperationFilter>();
            });
            services.AddMemoryCache();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseStatusCodePages();
            app.UseStaticFiles(new StaticFileOptions {
                OnPrepareResponse = context =>  
                {
                    context.Context.Request.Headers["Access-Control-Allow-Origin"] = "*";
                    context.Context.Response.Headers["Access-Control-Allow-Origin"] = "*";
                }
            });
            app.UseCors(CorsPolicy);

            app.UseSwagger();
            app.UseSwaggerUI(s =>
            {
                s.SwaggerEndpoint("../swagger/v1/swagger.json", "MySite");
            });

            app.UseAuthentication();
            //Generate EF Core Seed Data
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            }); 
        }
        public class AddAuthTokenHeaderParameter : IOperationFilter
        {
            public void Apply(Operation operation, OperationFilterContext context)
            {
                if (operation.Parameters == null)
                    operation.Parameters = new List<IParameter>();

                operation.Parameters.Add(new HeaderParameter
                {
                    Name = "Authorization",
                    In = "header",
                    Type = "string",
                    Required = false
                });
            }
            class HeaderParameter : NonBodyParameter
            {
            }
        }

        public class FileOperationFilter : IOperationFilter
        {
            public void Apply(Operation operation, OperationFilterContext context)
            {
                if (context.ApiDescription.ParameterDescriptions.Any(x => x.ModelMetadata.ContainerType == typeof(IFormFile)))
                {
                    operation.Parameters.Clear();
                    operation.Parameters.Add(new NonBodyParameter
                    {
                        Name = "FilePayload", // must match parameter name from controller method
                        In = "formData",
                        Description = "Upload file.",
                        Required = true,
                        Type = "file"
                    });
                    operation.Consumes.Add("application/form-data");
                }
            }
        }

    }
}