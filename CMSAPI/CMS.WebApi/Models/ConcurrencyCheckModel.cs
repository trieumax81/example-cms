﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CMS.WebApi.Models
{
    public class ConcurrencyCheckModel
    {
        public byte[] RowVersion { get; set; }
    }
}
