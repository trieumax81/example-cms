﻿using CMS.Utilities.Constants;
using System.ComponentModel.DataAnnotations;

namespace CMS.WebApi.Models
{
    public class RegisterModel
    {
        public string Id { get; set; }

        [Required]
        public string FullName { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        [RegularExpression(RegularExpressionConstant.EmailRules, ErrorMessage = "Invalid email pattern")]
        public string Email { get; set; }

        public string Phone { get; set; }

        public string Address { get; set; }

        public string[] RoleIds { get; set; }

        public string[] GroupIds { get; set; }

        public string Description { get; set; }

        public int DepartmentRefId { get; set; }
    }

    public class RegisterConfirmViewModel
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Code { get; set; }
    }
}