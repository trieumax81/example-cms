﻿using CMS.Utilities.Constants;
using System.ComponentModel.DataAnnotations;

namespace CMS.WebApi.Models
{
    public class ChangePasswordViewModel
    {
        public string UserName { get; set; }

        [Required]
        public string CurrentPassword { get; set; }

        [Required]
        [RegularExpression(RegularExpressionConstant.PasswordRules, ErrorMessage = "Invalid password pattern")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [Required]
        public string NewPasswordConfirm { get; set; }
    }
}