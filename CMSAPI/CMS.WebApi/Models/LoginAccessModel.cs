﻿namespace CMS.WebApi.Models
{
    public class LoginAccessModel
    {
        public string UserName { get; set; }

        public string AccessToken { get; set; }
    }
}