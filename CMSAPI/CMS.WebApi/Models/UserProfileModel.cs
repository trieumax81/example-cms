﻿using System;
using System.Collections.Generic;

namespace CMS.WebApi.Models
{
    public class UserProfileModel: ConcurrencyCheckModel
    {
        public string Id { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public string ChangePasswordToken { get; set; }

        public string UpdatedBy { get; set; }

        public string Description { get; set; }

        public string BccEmail { get; set; }

        public string Avatar { get; set; }

        public string Base64Avatar { get; set; }

        public bool IsUploadAvatar { get; set; }

    }
}