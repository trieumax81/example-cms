﻿using CMS.Utilities.Constants;
using System.ComponentModel.DataAnnotations;

namespace CMS.WebApi.Models
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [RegularExpression(RegularExpressionConstant.EmailRules, ErrorMessage = "Invalid email pattern")]
        public string Email { get; set; }
    }
}