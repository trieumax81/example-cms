﻿using CMS.Utilities.Constants;
using System.ComponentModel.DataAnnotations;

namespace CMS.WebApi.Models
{
    public class ResetPasswordViewModel
    {
        [Required]
        [RegularExpression(RegularExpressionConstant.EmailRules, ErrorMessage = "Invalid email pattern")]
        public string Email { get; set; }

        [Required]
        [RegularExpression(RegularExpressionConstant.PasswordRules, ErrorMessage = "Invalid password pattern")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string Password { get; set; }

        public string PasswordConfirm { get; set; }

        [Required]
        public string Code { get; set; }
    }
}